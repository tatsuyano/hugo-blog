#!/bin/bash

# Build the project.
hugo # if using a theme, replace by `hugo -t <yourtheme>`

# Copy public directory to apache directory
cp -a public/* /var/www/html/hugo/

cd /var/www/html/hugo/

# Change url for vagrant private ip
find ./ -name '*.html' -print | xargs sed -i "s|http://tatsuyano.github.io|http://192.168.33.10/hugo|g"
