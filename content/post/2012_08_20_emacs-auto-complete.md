+++
date = "2012-08-20T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "auto-complete.elをインストールする"
slug = "emacs-auto-complete"
+++

## はじめに

すでにauto-intall.elがインストールされている前提で話をすすめます。  
auto-installのインストール手順は[こちら](/2012/08/20/emacs-auto-install/)をご覧ください。  
  
またauto-installを使わずに、手動でインストール、コンパイルしたい場合は  
下記サイト(auto-complete.elの開発者さんが説明してくれています)をご参照ください。  
  
[「Emacsのトラノマキ」連載第09回「auto-completeを使おう」](http://dev.ariel-networks.com/wp/documents/aritcles/emacs/part9")  

## インストール手順

emacsを起動し、  
  
```
M-x auto-install-batch <RET>
Extension name: auto-complete development version <RET>
```
  
auto-isntall-batchを実行、次にインストールしたいパッケージ名(auto-complete development version)を入力します。  
すると自動で複数ソースのダウンロードが始まります。  
  
ダウンロードし終わると「モード行」に  
  
```
Type C-c C-c to continue; Type C-c C-d for view diff; Type C-c C-q to quit.
```

というメッセージが出てくるので、「C-c C-c」と入力し、ダウンロードしたソースを順番にコンパイルしていきます。  
すべてコンパイルし終えると、以下のようなメッセージが表示され、インストールが完了します。  

```
Installation is completed.
```

##. emacsの設定

``` scheme
;; auto-complete
(require 'auto-complete)
(require 'auto-complete-config) ; 必須ではないですが一応
(global-auto-complete-mode t)
```

## 実行画面

![](https://dl.dropboxusercontent.com/u/159938/blog_images/auto-complete001.png)

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

* [「Emacsのトラノマキ」連載第09回「auto-completeを使おう」](http://dev.ariel-networks.com/wp/documents/aritcles/emacs/part9")  
