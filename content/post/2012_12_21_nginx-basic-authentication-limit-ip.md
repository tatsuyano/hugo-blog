+++
date = "2012-12-21T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["nginx"]
title = "nginxにBASIC認証とIP制限を入れる"
slug = "nginx-basic-authentication-limit-ip"
+++

## Basic認証
自分のホーム以下に.htpasswdファイルを配置するとうまく認証できませんでした。  
なのであまりよくありませんが面倒なので、/etc/nginx配下で、かつパーミッションは777で。  

```
cd /etc/nginx
htpasswd -c /etc/nginx/.htpasswd hoge
chmod 777 .htpasswd
```

``` scheme
emacs conf.d/vh_redmine.conf
   location / {
     ....
     auth_basic "Secret Area";
     auth_basic_user_file "/etc/nginx/.htpasswd";
   }
```

## IP制限
上から順に設定が反映されるらしく、allowより前にdenyの設定を入れてしまうとallowが反映されないので注意。  

``` scheme
  server {
    ....
    allow xxx.xxx.xxx.xxx;
    deny  all;
  }
```

## 再起動

```
/etc/init.d/nginx restart
```

## 参考サイト
以下のサイトを参考にさせていただきました。ありがとうございます！

- [nginxでBasic認証を設定](http://se-suganuma.blogspot.jp/2012/05/nginxbasic.html)
- [Nginx で IPアドレスによるアクセス制限](http://ecpplus.net/weblog/nginx%E3%81%A7ip%E3%82%A2%E3%83%89%E3%83%AC%E3%82%B9%E3%81%AB%E3%82%88%E3%82%8B%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B9%E5%88%B6%E9%99%90/)
