+++
date = "2015-07-25T18:15:01+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "blog"]
title = "ブログをGhostからHugoに移行した"
slug = "switch-hugo"
+++

今までaws上でブログ(Ghost)を運用していたが、無料期間が終わったら月2000円  
ちょっとかかったので、またGithub Pages上にブログを移行させた。

以前はGithub PagesでOctopressを使っていたが、最近はHugoというのが流行っているらしいので使ってみた。

記事が思いの外多かったので、エクスポート用のコードをrubyで書いた。  
コードを書くのに思ったより時間がかかったが、久々にrubyを使って楽しかった。

<script src="https://gist.github.com/tatsuyano/2e74910f360ad1887de2.js"></script>


## 参考サイト

* [HugoとCircleCIでGitHub PagesにBlogを公開してみたら超簡単だった](http://hori-ryota.com/blog/create-blog-with-hugo-and-circleci/)
* [Hugoを使ってみたときのメモ](http://machortz.github.io/posts/usinghugo/)
* [Hugo + Github Pages + Wercker CI = ¥0（無料）でコマンド 1 発](http://qiita.com/yoheimuta/items/8a619cac356bed89a4c9)
