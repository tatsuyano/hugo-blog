+++
date = "2017-09-17T14:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["fish","shell"]
title = "fishをインストールする"
slug = "install-fish"
+++

macを新調したので、shellもzshからfishに乗り換えてみた。

## Install fish
``` shell
$ brew install fish

$ fish -v
fish, version 2.6.0
```

## fishをログインシェルにせず、bashから起動する
ログインシェルをfishにするとfishに問題があったときにterminalが動かなくなってしまうので、bash_profileから`exec fish`で起動する。
またanyenvがfishに完全に対応していないみたいなので、bashでanyenvを動かしてからfishを動かす。
``` shell
## HOME/.bash_profile
if [ -d $HOME/.anyenv ]; then
   export PATH=$HOME/.anyenv/bin:$PATH
   eval "$(anyenv init -)"
fi
exec fish
```
* [ログインシェルはbashのままfishを利用する](http://qiita.com/tak-onda/items/a90b63d9618d6b15c18c)
* [fishシェルでanyenvを設定したときにcalled during startupエラーが出る](https://ja.stackoverflow.com/questions/33694/fish%E3%82%B7%E3%82%A7%E3%83%AB%E3%81%A7anyenv%E3%82%92%E8%A8%AD%E5%AE%9A%E3%81%97%E3%81%9F%E3%81%A8%E3%81%8D%E3%81%ABcalled-during-startup%E3%82%A8%E3%83%A9%E3%83%BC%E3%81%8C%E5%87%BA%E3%82%8B)

## Install fisherman
はじめはOhMyFishをインストールしたが、調べた結果fishermanのほうがよさそうだったのでプラグインマネージャーはfishermanに。コマンドは`fisher`
``` shell
$ curl -Lo ~/.config/fish/functions/fisher.fish --create-dirs https://git.io/fisher

$ fisher -v
fisherman version 2.13.1 ~/.config/fish/functions/fisher.fish
```
* [fisherman/fisherman](https://github.com/fisherman/fisherman)
* [oh-my-fish は古い！fisherman で置き換えられる](http://futurismo.biz/archives/6079)
* [fishのプラグインマネージャ比較](http://qiita.com/ryotako/items/2d9d6fb646d2f3644dc8)

テーマはOhMyFishのagnosterを選択。別のテーマに変更する場合は、再度installすると変更される。
``` shell
$ fisher install omf/theme-agnoster
```

pluginのインストール
``` shell
$ fisher install z 0rax/fish-bd edc/bass

$ fisher ls // themeの前には*が付く
* agnoster   await  bd  get   getopts   last_job_id   z   bass
```
* [fish ユーザなら入れておきたいプラグイン 13 選](http://futurismo.biz/archives/6087)


## alliasとfunctionを設定する
自分用のaliasとfunctionを`HOME/.config/fish/config.fish`に設定。


``` shell
## HOME/.config/fish/config.fish

# git alias
alias ga  "git add"
alias gd  "git diff"
alias gb  "git branch"
alias gco "git checkout"
alias gst "git status"

# colordiff or diff
set tmp (which colordiff) #実行結果を変数にsetすることで、標準出力に出力させないようにする
if test $status -eq 0
    alias diff "colordiff -u"
else
    alias diff "diff -u"
end

# tmux
function tmr
    tmux new -s $argv[1]; or tmux attach -d -t $argv[1]
end
```

## Ctrl+F のキーバインドを変える
`Ctrl+F`でカーソルが動かないと思ったら、デフォルトだと`Ctrl+F`はautosuggestionがbindされている。
``` fish
bind \cf 'forward-word'
```
* [bind キーバインドを設定・削除 使えるキーを一覧する](http://fish.rubikitch.com/bind/)
* [fish shell main page](https://fishshell.com/)
* [Autosuggestions](http://fishshell.com/docs/current/index.html)

``` shell
## Autosuggestionsより抜粋
To accept the autosuggestion (replacing the command line contents), press right arrow or Control+F.
To accept the first suggested word, press Alt+→ or Alt+F. 
If the autosuggestion is not what you want, just ignore it: it won't execute unless you accept it.
```

## peco & ghqを入れる
``` shell
$ brew install peco ghq
$ fisher install z decors/fish-ghq peco  yoshiori/fish-peco_select_ghq_repository
```
fishでhistory見るには`fish | history | peco`みたいなコマンドで実行する。下記のサイトを参考にfunctionにした。
``` shell
function peco_select_history
    if test (count $argv) = 0
        set peco_flags
    else
        set peco_flags --query "$argv"
    end
    history|peco $peco_flags|read foo
    if [ $foo ]
        commandline $foo
    else
        commandline ''
    end
end

# keybinding for peco
function fish_user_key_bindings
    bind \cs peco_select_ghq_repository
    bind \cr peco_select_history # Bind for peco select history to Ctrl+R
end
```
* [やっぱり良かったpeco + ghq](https://blog.craftz.dog/%E3%82%84%E3%81%A3%E3%81%B1%E3%82%8A%E8%89%AF%E3%81%8B%E3%81%A3%E3%81%9Fpeco-ghq-4eb57aeda33e)

## その他参考サイト
* [zsh から fish にした](https://www.hsbt.org/diary/20170421.html)
* [fish shell を使いたい人生だった](http://dev.classmethod.jp/etc/fish-shell-life/)
* [詳解 fishでモダンなシェル環境の構築](http://qiita.com/susieyy/items/ac2133e249f252dc9a34)
* [fish shellを使う](https://blog.nijohando.jp/post/starting-fishshell/)
