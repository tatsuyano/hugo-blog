+++
date = "2014-07-29T04:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "fuelphpのエラー文言を日本語化する"
slug = "fuelphp-validation"
+++

#### config.phpに設定を追加

app/config/config.php

```
//'language'           => 'en', // Default language
//'language_fallback'  => 'en', // Fallback language when file isn't available for default language
//'locale'             => 'en_US', // PHP set_locale() setting, null to not set
'language'           => 'ja',
'language_fallback'  => 'en',
'locale'             => 'ja_JP.UTF-8',
```

#### langディレクトリに日本語用のディレクトリを作成

    mkdir app/lang/ja
    touch app/lang/ja/validation.php

validation.phpの設定は core/lang/en/validation.php を参照
設定されているkey(メソッド)は、core/classes/validation.phpで設定されている

```
<?php
return array(
    'required'         => '「:label」は必須です',
    'min_length'       => '「:label」は:param:1文字以上で入力してください',
    'max_length'       => '「:label」は:param:1文字以内で入力してください',
    'exact_length'     => '「:label」は:param:1文字で入力してください',
    'match_value'      => '「:label」は「:param:1」と一致していません',
    'match_pattern'    => '「:label」はパターン「:param:1」と一致しません',
    'match_field'      => '「:label」は「:param:1」と一致していません',
    'valid_email'      => '「:label」はメールアドレスが不正です',
    'valid_emails'     => '「:label」に不正なメールアドレスが含まれてます',
    'valid_url'        => '「:label」は不正なURLです',
    'valid_ip'         => '「:label」は不正なIPアドレスです',
    'numeric_min'      => '「:label」は:param:1以上で入力してください',
    'numeric_max'      => '「:label」は:param:1以内で入力してください',
    'valid_string'     => '「:label」は:param:1で入力する必要があります',
);
```

### 自前のvalidation(helperクラス)を作成する

    mkdir app/classes/helper
    touch app/classes/helper/myvalidation.php

app/classes/helper/myvalidation.php
```
<?php
class Helper_MyValidation
{
    /**
     * 数字をチェックするバリデーション
     *
     * @param  string val
     * @return bool   numeric or blank is 'true'.
     */
    public static function _validation_numeric_only($val)
    {
        mb_regex_encoding("UTF-8");
        return preg_match("/^[\d]*$/", $val) === 1;
    }
}
```

#### 自作したメソッドのアラートを登録する
```
cat fuel/app/lang/ja/validation.php | grep numeric_only
'numeric_only'     => '「:label」は数字のみで入力してください',
```

#### controllerで自作のバリデーションの呼び出し
app/classes/controller/hoge.php
```
public function action_create()
{
  $val = Validation::forge();
  $val->add_callable('Helper_MyValidation');
  $val->add_field('name','名前','required|min_length[2]|max_length[30]');
  $val->add_field('email','Email','required|valid_email');
  $val->add_field('tel','電話番号','numeric_only');
```

### どこでバリデーションを呼ぶべきか
modelかcontroller どちらに実装すべきかは迷いましたが、今回はcontrollerに実装するようにしました。
理由はfieldsetを使わないからです。

fieldsetは便利ですが、どうしても細かいところに手が届かないのと、デザイナーさんと分業するには、少し勝手が悪いかなあと思いました。

### 参考サイト
* [Validation クラス](http://fuelphp.jp/docs/1.7/classes/validation/validation.html)
* [バリデーションのルールを追加する](http://raining.bear-life.com/fuelphp/%E3%83%90%E3%83%AA%E3%83%87%E3%83%BC%E3%82%B7%E3%83%A7%E3\%83%B3%E3%81%AE%E3%83%AB%E3%83%BC%E3%83%AB%E3%82%92%E8%BF%BD%E5%8A%A0%E3%81%99%E3%82%8B)

### 関連する記事
* [fuelphpでAgentClassを設定する](http://blog.10rane.com/2014/07/29/fuelphp-use-agent-class/)
* [fuelphpでredisを使う](http://blog.10rane.com/2014/07/28/use-the-redis-in-fuelphp/)
