+++
date = "2014-07-21T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["vagrant"]
title = "VagrantとVirtualBoxのインストール"
slug = "install-vagrant-and-virtualbox"
+++

今回はVagrantが1.5.4、VirtualBoxが4.3.10をインストール

* [DOWNLOAD VAGRANT](http://www.vagrantup.com/downloads.html)
* [Download VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## BOXのインストール
新しくできた[VAGRANT CLOUD](https://vagrantcloud.com/discover/featured)にあるBOXをインストール

```
$ vagrant add chef/centos-6.5
1) virtualbox
2) hyperv
Enter your choice: 1
```

## 仮想マシンを作る

### まずは設定ファイルを置くためのディレクトリを掘る
```
$ mkdir -p ~/Vagrant/CentOS65
$ cd /Vagrant/CentOS65
```

### インストールされているBOXの確認
```
$ vagrant box list
$ chef/centos-6.5 (virtualbox, 1.0.0)
```

### 初期化と起動
```
$ vagrant init chef/centos-6.5
$ vagrant up
```

### 仮想マシンへの接続
```
$ vagrant ssh
```

### 仮想マシンのIPを固定にする
Vagrantfileのconfig.vm.networkをコメントインする
```
$ grep -ir "private_network" ~/Vagrant/Centos65/Vagrantfile
/$HOME/Vagrant/Centos65/Vagrantfile:  # config.vm.network "private_network", ip: "192.168.33.10"
$ vagrant reload
```

### SSHの設定を保存し、ホスト名でログインできるようにする
```
$ cd ~/Vagrant/Centos65
$ vagrant ssh-config >> ~/.ssh/config
ssh default <- 追加した状態だとhost名が'default'なので、後で変える
```

### それ以外のコマンド

仮想マシンの削除
```
$ vagrant box remove chef/centos-6.5
```

ステータスの確認
```
$ vagrant status
```

## 参考サイト
以下のサイトを参考にさせていただきました。ありがとうございます！

* [Vagrant セットアップ (Mac)](http://qiita.com/inouet/items/b36638adc2b5772db457)
* [Vagrant のコマンドをメモしておく](http://shindolog.hatenablog.com/entry/2013/04/15/235731)

#### 関連する記事
* [aws はじめました。](http://blog.10rane.com/2014/07/21/start-aws/)
* [VagrantとVirtualBoxのインストール](http://blog.10rane.com/2014/07/21/install-vagrant-and-virtualbox/)
