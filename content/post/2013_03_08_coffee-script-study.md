+++
date = "2013-03-08T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["coffeescript"]
title = "CoffeeScriptの勉強 その２(構文)"
slug = "coffee-script-study"
+++

#### Hello World

$ emacs test.coffee
<pre><code class="language-bash">hello = ->
        console.log("Hello World!")

hello()
</code></pre>

コンパイル coffee -> js。 test.jsというJsファイルが生成される

<pre><code class="language-bash">$ coffee -c test.coffee
</code></pre>

生成したJsの実行

<pre><code class="language-bash">$ node test.js
Hello World!
</code></pre>

生成されたJsの中身

<pre><code class="language-bash">(function() {
    var hello;
    hello = function() {
    return console.log("Hello World!");
 };
 hello();
}).call(this);
</code></pre>

コンパイルせずに直接実行

<pre><code class="language-bash">$ coffee test.coffee
Hello World!
</code></pre>

coffee <ファイル名> と入力するのが面倒なので、zshのaliasを設定する

emacs $HOME/.zshrc
<pre><code class="language-bash">alias -s coffee=coffee

source $HOME/.zshrc
</code></pre>

今後は、ファイル名を入力するだけで実行される

<pre><code class="language-bash">$ ./test.coffee
Hello World!
</code></pre>

#### オプション
* -c (--compile) .coffee から .js にコンパイルします。
* -b (--bare)    外側の (function(){ ... }).call(this); を取り除きます。
* -w (--watch)   ソースファイルを監視して、ファイルが変更されるたびにコンパイルを実行します。

#### 構文
* var での変数宣言が不要
* 関数の定義(function)の代わりに -> を使う
* 式の終わりや行末にセミコロンを必ずしも置かなくてもよい
* インデントでブロックを表現する
* 引数を取る関数の場合、() を省略して書くこともできる

#### コメントアウト
<pre><code class="language-bash"># 1行コメント
# コンパイル時には消される

###
複数行コメント
コンパイル時にも残る
###
</code></pre>

#### 関数
-> を使って定義します。引数を取る場合は -> を (var1, var2) -> のようにして引数を並べます。  
関数内の最後の値が自動的にreturnされます。

<pre><code class="language-bash">square = (x) -> x * x
</code></pre>

コンパイル結果 ↓

<pre><code class="language-bash">square = function(x) {
  return x * x;
};
</code></pre>

例) 引数がないので、()を省略
<pre><code class="language-bash">hello = -> "Hello World!"
hello()
</code></pre>

#### if文
* 複数行の場合
<pre><code class="language-bash">fib = (n) ->
  if n < 2
     n
  else
     fib(n-1) + fib(n-2)
</code></pre>

* 一行の場合
<pre><code class="language-bash">fib = (n) -> if n < 2 then n else fib(n-1) + fib(n-2)
</code></pre>

#### 変数展開
" " でくくった文字列の中では #{変数名} で変数の値を埋め込むことができます。
<pre><code class="language-bash">name = "Nao"
console.log "My name is #{name}!"
</code></pre>

#### 演算子
<pre><code class="language-bash">|Coffee | Js |
|:------|:---|
| is    | ===|
| isnt  | !==|
| not   | !  |
| and   | && |
| or    | || |
</code></pre>

#### 参考サイト
以下のサイトを参考にさせていただきました。ありがとうございます！

* [今日から始めるCoffeeScript](http://tech.kayac.com/archive/coffeescript-tutorial.html)
* [The Little Book on CoffeeScript](http://minghai.github.com/library/coffeescript/index.html)
