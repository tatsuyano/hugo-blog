+++
date = "2017-09-19T10:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs","flycheck","tidy"]
title = "Syntax checker html-tidy reported too many errors"
slug = "installed-tidy-html5-for-flycheck"
+++

macのemacsでHTMLを開くとflycheck(html-tidy)で警告がでるようになった。
``` shell
Warning (flycheck): Syntax checker html-tidy reported too many errors (960) and is disabled.
```

`tidy`という名前のパッケージがなかったので調べたら、最近は`tidy-html5`という名前らしい。
``` shell
$ brew install tidy-html5
```
