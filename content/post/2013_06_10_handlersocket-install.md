+++
date = "2013-06-10T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "mysql", "middleware"]
title = "HandlerSocketをインストールする"
slug = "handlersocket-install"
+++

## インストールする内容

* rpm版 MySQL 5.1.68
* HandlerSocket-Plugin-for-MySQL 最新版(2013/06/05)
* php-handlersocket 0.3.1

## 作業の流れ

1. すでにyumでインストールしてあるMySQLの削除
2. MySQL(rpm版)のインストール
3. Handler Socketのインストール
4. php-handlersocketのインストール

## すでにyumでインストールしてあるMySQLの削除

前にテスト導入で成功したバージョンにあわせるため、yumでインストールしたMySQLを削除

### インストール済みのMySQLを確認

```
yum list installed | grep mysql
mysql.x86_64            5.1.69-1.el6_4  @updates
mysql-libs.x86_64       5.1.69-1.el6_4  @updates
mysql-server.x86_64     5.1.69-1.el6_4  @updates
php-mysql.x86_64        5.3.3-22.el6    @base
```

### chkconfigからmysqldをoff

```
chkconfig --list mysqld
mysqld          0:off   1:off   2:on    3:on    4:on    5:on    6:off

chkconfig mysqld off
```

### yumでインストールしたMySQLを削除

```
yum -y remove mysql mysql-server mysql-libs
```

## MySQL(rpm版)のインストール

過去にインストールしたmysqlのバージョンは5.1.66 だったが見つからなかったので、  
近いバージョンの5.1.68をインストールすることにした。

### インストール先のディレクトリ作成

```
mkdir -p /usr/src/redhat/RPMS/x86_64/
cd /usr/src/redhat/
mkdir BUILD SOURCES SPECS SRPMS
```

### rpmファイルのダウンロード

```
cd /usr/src/redhat/RPMS/x86_64/
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/MySQL-client-community-5.1.68-1.rhel5.x86_64.rpm
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/MySQL-server-community-5.1.68-1.rhel5.x86_64.rpm
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/MySQL-devel-community-5.1.68-1.rhel5.x86_64.rpm
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/MySQL-shared-community-5.1.68-1.rhel5.x86_64.rpm
```

### インストール

```
rpm -ivh MySQL*.rpm
```

### インストールの確認

```
/etc/init.d/mysql start
/usr/bin/mysql -uroot
```

## Handler Socketのインストール
Handler Socketをインストールするには、MySQLのソース版とgitが必要。

### 起動中のMySQLを停止させる(念のため)
```
/etc/init.d/mysql stop
```

### MySQLのソース版をダウンロード
ダウンロードするソースは、先ほどインストールしたMySQL 5.1.68と合わせること。

```
cd /usr/src/redhat/SRPMS/
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/MySQL-community-5.1.68-1.rhel5.src.rpm
rpm -ivh MySQL-community-5.1.68-1.rhel5.src.rpm
```
```
cd /usr/src/redhat/SOURCES/
wget http://download.softagency.net/MySQL/Downloads/MySQL-5.1/mysql-5.1.68.tar.gz
tar xvzf mysql-5.1.68.tar.gz
```

### 必要なライブラリなどをyumでインストール

```
yum -y install libtool gcc gcc-c++ openssl-devel
```

### HandlerSocket-Plugin-for-MySQLをダウンロード(git clone)

```
cd /tmp/
git clone git://github.com/ahiguti/HandlerSocket-Plugin-for-MySQL.git
```

### configure and make install

```
cd /tmp/HandlerSocket-Plugin-for-MySQL/

./autogen.sh

./configure \
--with-mysql-source=/usr/src/redhat/SOURCES/mysql-5.1.68 \
--with-mysql-bindir=/usr/bin \
--with-mysql-plugindir=/usr/lib64/mysql/plugin
make
make install
```

* with-mysql-source..... MySQLのソースコードのトップディレクトリを指定します。
* with-mysql-bindir..... インストール済みのMySQLのmysql_configコマンドが有るディレクトリを指定します。"which mysql_config"で調べます。
* with-mysql-plugindir.. MySQLのpluginディレクトリを指定します。以下のコマンドで確認できます。
```
cd /usr/src/redhat/RPMS/x86_64/
rpm -ql MySQL-server-community-5.1.68-1.rhel5 | grep plugin
/usr/lib64/mysql/plugin/ha_innodb_plugin.so
/usr/lib64/mysql/plugin/ha_innodb_plugin.so.0
/usr/lib64/mysql/plugin/ha_innodb_plugin.so.0.0.0
```

### configure 時の注意

自分は --with-mysql-plugindirのパスを間違えて、  
--with-mysql-plugindir=/usr/lib64/mysql/plugin ではなく、  
--with-mysql-plugindir=/usr/lib/mysql/plugin で指定したために、  
「libtool: link: only absolute run-paths are allowed」とうエラーが出てしまいうまくいかなった。  
うまくいかないときは、一度 make clean して再度 configure のパラメータを確認すること。

### handlersocketのpluginが入っているか(インストールがうまくいっているかの)確認

```
ll /usr/lib64/mysql/plugin/handlersocket*
-rw-r--r-- 1 root root  3519732  6月  7 10:16 2013 handlersocket.a
-rwxr-xr-x 1 root root      972  6月  7 10:16 2013 handlersocket.la
lrwxrwxrwx 1 root root       22  6月  7 10:16 2013 handlersocket.so -> handlersocket.so.0.0.0
lrwxrwxrwx 1 root root       22  6月  7 10:16 2013 handlersocket.so.0 -> handlersocket.so.0.0.0
-rwxr-xr-x 1 root root  1708138  6月  7 10:16 2013 handlersocket.so.0.0.0
```

### pluginのインストール

```
/etc/init.d/mysql start
mysql -uroot;

mysql> install plugin handlersocket soname 'handlersocket.so';
mysql> show plugin;
+---------------+----------+----------------+------------------+---------+
| Name          | Status   | Type           | Library          | License |
+---------------+----------+----------------+------------------+---------+
| binlog        | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| partition     | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| ARCHIVE       | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| BLACKHOLE     | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| CSV           | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| FEDERATED     | DISABLED | STORAGE ENGINE | NULL             | GPL     |
| MEMORY        | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| InnoDB        | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| MyISAM        | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| MRG_MYISAM    | ACTIVE   | STORAGE ENGINE | NULL             | GPL     |
| handlersocket | ACTIVE   | DAEMON         | handlersocket.so | BSD     |
+---------------+----------+----------------+------------------+---------+
11 rows in set, 1 warning (0.00 sec)
```

### my.cnf の設定

```
/etc/init.d/mysql stop
cat > /etc/my.cnf
[mysqld]
handlersocket_port =    9998 # handlersocketが接続を受け付けるポート(参照系リクエスト用)
handlersocket_port_wr = 9999 # handlersocketが接続を受け付けるポート(更新系リクエスト用)
handlersocket_address =      # handlersocketがバインドするアドレス(空のままでOK)
handlersocket_verbose = 0    # デバッグ用
handlersocket_timeout = 300  # 通信タイムアウト(秒)
handlersocket_threads = 16   # handlersocketのワーカースレッド数
thread_concurrency = 128     # handlersocketが幾つかのスレッドを占有するため、大きめの値を指定してください
open_files_limit = 65535

/etc/init.d/mysql start
```

## php-handlersocketのインストール

### ソースのダウンロード

```
cd /tmp/
wget https://php-handlersocket.googlecode.com/files/php-handlersocket-0.3.1.tar.gz
tar xvzf php-handlersocket-0.3.1.tar.gz
```

### configure and make install

```
cd handlersocket
phpize
./configure
make
make install
Installing shared extensions:     /usr/lib64/php/modules/
Installing header files:          /usr/include/php/
```

### php.iniへ追加、httpdの再起動

```
echo "extension=handlersocket.so" > /etc/php.d/hs.ini
/etc/init.d/httpd restart
```

php.iniへの追加を行わなかったために、Class not found になってしまった。  
PHP Fatal error:  Class 'HandlerSocket' not found in /tmp/handlersocket/examples/example.php on line 12  
モジュールをインストール後は、/etc/php.d/以下に設定ファイルを配置し、httpdを再起動する。  

* [AWS EC2にhandlersocketを入れるまで](http://qiita.com/items/540fd2ecdc98a7549fcb)

### php-handlersocketのインストール確認

まずはphpinfo.phpを確認する。

>
handlersocket  
MySQL HandlerSocket support enabled  
extension Version           0.3.1  
hsclient Library Support    enabled
>

### exsampleを動かし、最終確認

handlersocketに付属しているexamplesを実行し最終確認する。

```
cd /tmp/handlersocket/examples/
```

テスト用のDB、テーブルを作成し、6件ほどレコードをinsertする

```
emacs development.ja.php

CREATE DATABASE hstestdb;

CREATE TABLE hstesttbl (
  k varchar(30) PRIMARY KEY,
  v varchar(30) NOT NULL,
  f1 varchar(30),
  f2 varchar(30)
) Engine = innodb;

CREATE INDEX i1 ON hstesttbl(v, k);
INSERT INTO hstesttbl VALUES ('k1', 'v1', 'f1', 'f2');
INSERT INTO hstesttbl VALUES ('k2', 'v2', 'f2', NULL);
INSERT INTO hstesttbl VALUES ('k3', 'v3', 'f3', '');
INSERT INTO hstesttbl VALUES ('k4', 'v4', 'f4', 'f24');
INSERT INTO hstesttbl VALUES ('k5', 'v5', 'f5', NULL);
INSERT INTO hstesttbl VALUES ('k6', 'v6', 'f6', '');
```

php example.php
<pre><code class="language-php">
array(1) {
  [0]=>
  array(2) {
    [0]=>
    string(2) "k1"
    [1]=>
    string(2) "v1"
   }
}
array(1) {
  [0]=>
  array(2) {
    [0]=>
    string(2) "k2"
    [1]=>
    string(2) "v2"
   }
}
bool(false)
56:op:
</code></pre>

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [CentOSにMySQLとHandler Socketを入れてみる](http://kotaroito.hatenablog.com/entry/20111213/1323755054)
* [Akito's IT技術 メモ](http://akitosblog.seesaa.net/article/193127939.html)
* [HandlerSocket-Plugin-for-MySQL](https://github.com/ahiguti/HandlerSocket-Plugin-for-MySQL)
* [php handler socket](https://code.google.com/p/php-handlersocket/)
* [HandlerSocket について + a](http://www.slideshare.net/akirahiguchi/handlersocket-etc-20110906)
