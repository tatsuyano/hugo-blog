+++
date = "2012-11-01T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "nXhtmlをインストールする"
slug = "emacs-nxhtml"
+++

## nXhtmlとは

htmlファイルには、javascriptやphp、perlなどhtml以外の複数の言語で書かれていたりする。  
このため今までは、今書いている言語に合わせて、自分でモードの切り替えを行なっていたのだが、nXhtmlを使うと、自動で今書いている言語に合わせてモードを切り替えてくれる。  

## インストール

nXhtmlの[本家サイト](http://ourcomments.org/cgi-bin/emacsw32-dl-latest.pl)の、Download latest nXhtml (zip file)からzipをダウンロードし、pathの通っている階層(add-to-list 'load-path "~/.emacs.d/elisp")に移動。  

```
wget http://ourcomments.org/Emacs/DL/elisp/nxhtml/zip/nxhtml-2.08-100425.zip
tar xvzf nxhtml-2.08-100425.zip
mv nxhtml .emacs.d/elisp/
```

.emacs.elに一行追加。  
※ 00_init.elに追加するとエラーになってしまったため、やむなく.emacs.elに追加  

``` scheme
cat >> .emacs.el
(load "nxhtml/autostart.el")
```

## バイトコンパイル

```
M-x nxhtmlmaint-start-byte-compilation
```

コンパイル時にでるwarningの対応。下記の警告が出てくるので  

``` scheme
Warning: `font-lock-beginning-of-syntax-function’ is an obsolete variable (as of Emacs 23.3); use `syntax-begin-function’ instead.
```

.emacs.d/elisp/nxhtml/util/mumamo.elの「font-lock-beginning-of-syntax-function」を「syntax-begin-function」に置換し、再度コンパイル。  

## nXhtmlのモードについて

「nXhtml/nxhtml Invalid)」や「PHP/nxhtml Completion AC Abbrev」にするには  

```
M-x nxhtml-mumamo
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Web開発のためのEmacsの設定](http://sanrinsha.lolipop.jp/blog/2011/05/web%E9%96%8B%E7%99%BA%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AEemacs%E3%81%AE%E8%A8%AD%E5%AE%9A.html)
* [Emacs23 に nXhtml をインストールしてみる](http://ubuntu-note.blog.so-net.ne.jp/2010-11-13)
