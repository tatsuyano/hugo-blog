+++
date = "2014-08-28T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "HelmをCaskでインストールする"
slug = "how-to-install-and-setup-helm-in-cask"
+++

`anything.el`をフォークして作られたツール、`Helm`を`Cask`でインストール、管理したいと思います。

### インストール

~/.emacs.d/Cask

    (depends-on "helm")

`cask`コマンドの実行

    $ cd .emacs.d
    $ cask
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/gnu/archive-contents
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/marmalade/archive-contents
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/melpa/archive-contents
    Making version-control local to helm-autoloads.el while let-bound!
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/helm-20140827.436/helm-autoloads.el

### 最小限の設定

とりあえず[README.md](https://github.com/emacs-helm/helm/blob/master/README.md)に書いてある最小限の設定をします。

~/.emacs.d/init.el

    ;; Helm
    (require 'helm-config)
    (global-set-key (kbd "C-c h") 'helm-mini)
    (helm-mode 1)

    ;;ミニバッファでC-hをバックスペースに割り当て
    (define-key helm-read-file-map (kbd "C-h") 'delete-backward-char)

### 基本的な操作

* `C-c h`で最近開いたバッファ(ファイル)を選択する
* `C-c f`で`helm-mode-find-file`が実行

`helm-mode 1`がどういう設定になっているかわかっていないので、今はこれぐらいしか把握していません(笑)。
もう少し理解したら書き足したいと思います。

#### 追加 2014.9.11 Helmの設定

[こちらのサイト](http://d.hatena.ne.jp/a_bicky/20140104/1388822688)を参考に設定してみました。設定はまったく同じなので、省略します。

### 参考サイト

* [emacs-helm](https://github.com/emacs-helm/helm)
* [helmでC-eとC-jが使えなくなったので取り戻した](http://rubikitch.com/2014/08/14/helm)
* [helm.elをインストールしてみた](http://konbu13.hatenablog.com/entry/2014/01/15/223014)
* [anything.elのフォークhelm-modeをインストールする](http://emacs.tsutomuonoda.com/emacs-anything-el-helm-mode-install)
* [syohex/dot_files/emacs/init_loader/02_helm.el](https://github.com/syohex/dot_files/blob/master/emacs/init_loader/02_helm.el)
* [Helm をストレスなく使うための個人的な設定](http://d.hatena.ne.jp/a_bicky/20140104/1388822688)

### 関連する記事

* [Caskをインストールする](http://blog.10rane.com/2014/08/20/install-cask/)
