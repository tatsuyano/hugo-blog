+++
date = "2012-11-02T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "emacs"]
title = "MacのPHP開発環境を整える"
slug = "mamp-php-mac"
+++

#### はじめに

勉強のためにローカル上でのPHP開発環境を作ろうと思います。  

#### MAMPのインストール

macにapache,mysql,phpをセットにしたものをインストールするためのソフト。  
M(ac) A(pache) M(ysql) P(hp)。同じ用途でXAMPPというのもありますが、私の環境ではうまく動きませんでした。  

* [MAMPのインストール for Mac](http://www.be-webdesigner.com/technotes/server/install/mamp.htm)
* [MAMPのダウンロードサイト](http://www.mamp.info/en/)

有償のPro版も同時にインストールされてしまいます。今回はPHPを少し触ってみたいだけなので、無償版を使います。  

#### emacsのPHP開発環境

* [php-mode.elをインストールする](/2012/10/29/emacs-php-mode/)
* [php-completion.elをインストールする](/2012/10/29/emacs-php-completion/)

<del datetime="2012-11-01T15:03:10+00:00">補完機能と色付けだけは欲しかったのでインストール。</del>  

php-mode.elだけだと、phpファイルにhtmlの記述がある(又はその逆)とインデントがくずれてしまう。  
なので最終的に、「php-mode + php-completion + nXhtml』を使う形に落ち着いた。  
＊php-completionはauto-completeの機能を利用している  

* [nXhtmlをインストールする](/2012/11/01/emacs-nxhtml/)


php-mode.elの設定(インデントと、phpファイルを開いたときのmode指定)をコメントアウト  

``` scheme
emacs ~/.emacs.d/conf/50_php.el
;; php-mode
(require 'php-mode)
;;(setq php-mode-force-pear t) ;PEAR規約のインデント設定にする
;;(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
```

## 実行画面

htmlとphpのコードで書かれているindex.phpを開いてみると、htmlのコードを編集すると(nXhtml/nxhtml lnValid)に  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/nxhtml_001.png)

phpのコードを編集すると(PHP/nxhtml Completion AC Abbrev)にmodeが切り替わる

![](https://dl.dropboxusercontent.com/u/159938/blog_images/nxhtml_002.png)

補完機能もちゃんと動いている  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/nxhtml_003.png)
＊nXhtmlで起動するさいに下記の警告がでるが、今のところ問題なく動いているので無視する  

```
Warning: `font-lock-syntactic-keywords' is an obsolete variable (as of 24.1);
use `syntax-propertize-function' instead.
```
