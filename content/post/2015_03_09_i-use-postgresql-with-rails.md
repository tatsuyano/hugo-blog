+++
date = "2015-03-09T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "heroku", "rails", "postgresql"]
title = "RailsでPostgreSQLを使う"
slug = "i-use-postgresql-with-rails"
+++

アプリをHerokuにdeployしたらSQLエラーが出てしまったので、
ローカルでの開発もPostgreSQLを使ってみようと思います。

## PostgreSQLのインストール

ちなみにインストール先のOSはamazon-linuxです。
<pre><code class='language-bash'>$ sudo yum -y install postgresql93
$ sudo yum -y install postgresql93-devel
$ sudo yum -y install postgresql93-server

$ psql --version
psql (PostgreSQL) 9.3.6

# データベースの初期化
$ sudo /etc/init.d/postgresql93 initdb

# データベースの起動
$ sudo /etc/init.d/postgresql93 start

# サーバー起動時にpostgresの起動設定
$ sudo chkconfig postgresql93 on
</code></pre>
<br>

### PostgreSQLアカウントの設定

まずは PostgreSQL にスーパーユーザ(postgres)で接続し、パスワードを設定します。
<pre><code class='language-bash'>$ sudo -u postgres psql
postgres=# alter role postgres with password 'hogehoge';  # alter role [user_name] with password '[password]';
</code></pre>
<br>

### データベースの作成
わかりやすいようにデータベース名とRailsのアプリ名(myapp)を同じにします。
ついでにテスト(RSpec)用のデータベースも作成しておきます。
<pre><code class='language-ruby'>postgres=# create database myapp owner postgres; # create database [database_name] owner [user_name];
postgres=# create database myapp_test owner postgres;
</code></pre>
<br>

### アクセス権限の設定
Rails から PostgreSQL に接続するために、`pg_hba.conf`の設定を変更します。

/var/lib/pgsql93/data/pg_hba.conf
<pre><code class='language-bash'># TYPE  DATABASE  USER  ADDRESS   METHOD
#local   all       all             peer  <= コメントアウト
local   all       all             md5   <= コメントイン
</code></pre>
変更後は、PostreSQL と Nginx を再起動
<pre><code class='language-bash'>$ sudo /etc/init.d/postgresql93 restart
$ sudo /etc/init.d/nginx restart
</code></pre>
<br>

## Rails側の設定

アプリを作成する前に、PostgreSQL を初めて使う場合は、先に`pg gem`をインストールしておきます。
<pre><code class='language-bash'>$ gem install pg
</code></pre>
<br>

### アプリの作成
`-d`パラメータでDBでPostgreSQLに指定する

<pre><code class='language-bash'>$ rails new myapp -T -d postgresql
</code></pre>
<br>

### 接続先情報の設定
先ほど作成したデータベースとアカウントの情報を設定します。

config/database.yml
<pre><code class='language-ruby'> default: &default
   adapter: postgresql
   encoding: unicode
   pool: 5

 development:
   <<: *default
   database: myapp
   username: postgres
   password: hogehoge

 test:
   <<: *default
   database: myapp_test
   username: postgres
   password: hogehoge
</code></pre>
<br>

###(おまけ) PostgeSQLのコマンド

* データベース一覧を取得する ... \l
* データベースを選択する     ... \c [データベース名]
* テーブル一覧を取得する     ... \z
* テーブルスキーマを取得する ... \d [テーブル名]
* ユーザ(Role)の一覧         ... \du
* パスワードの変更           ... alter role [ロール名] with password '[新しいパスワード]';
* データベースの作成         ... create database [データベース名] owner [ロール名];
* データベースの削除         ... drop database [データベース名];

### 参考サイト

* [RubyonRailsでsqliteでなくてPostgresqlを使う時](http://www.workabroad.jp/posts/1079)
