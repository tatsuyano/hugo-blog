+++
date = "2014-07-28T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "middleware"]
title = "fuelphpでredisを使う"
slug = "use-the-redis-in-fuelphp"
+++

まずはredisのインストールから

    $ yum --enablerepo=epel install -y redis
    $ /etc/init.d/redis start
    redis-server を起動中:                                  [  OK  ]

confの場所

    $ emacs /etc/redis.conf

redisが正常にインストールされているか確認

    $ redis-cli
    redis 127.0.0.1:6379> set name 'hoge'
    OK
    redis 127.0.0.1:6379> get name
    "hoge"


保存されているKEYを表示

    $ redis-cli

    redis 127.0.0.1:6379> KEYS *
    1) "particles"
    2) "name"
    redis 127.0.0.1:6379>


#### fuelphpの設定

config/db.php

    'redis' => array(
        'default' => array(
        'hostname' => '127.0.0.1',
        'port'     => 6379
      )
    ),

config.session.php

    'driver' => 'redis',
    ....
    // specific configuration settings for redis based sessions
    'redis' => array(
      'cookie_name' => 'fuelrid', // name of the session cookie 
      'database'    => 'default'  // name of the redis database to use

### 参考サイト
* [redisサーバー構築メモ](http://konboi.hatenablog.com/entry/2013/03/05/155107)
* [KOSHIGOE学習帳 - [KVS][Redis] Redis コマンド一覧](http://w.koshigoe.jp/study/?%5BKVS%5D%5BRedis%5D+Redis+%A5%B3%A5%DE%A5%F3%A5%C9%B0%EC%CD%F7)
* [Redis_Db クラス](http://fuelphp.jp/docs/1.7/classes/redis.html)
* [FuelPHPでRedisを使用する](http://btt.hatenablog.com/entry/2012/06/28/004230)
* [Redis でお手軽に複数台構成の検証環境を作る](http://d.hatena.ne.jp/akishin999/20130607/1370580692)

### 関連する記事
* [fuelphpでAgentClassを設定する](http://blog.10rane.com/2014/07/29/fuelphp-use-agent-class/)
* [fuelphpのエラー文言を日本語化する](http://blog.10rane.com/2014/07/29/fuelphp-validation/)
