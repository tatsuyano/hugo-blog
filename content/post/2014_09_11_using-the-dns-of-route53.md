+++
date = "2014-09-11T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["aws"]
title = "Route53のDNSを使う"
slug = "using-the-dns-of-route53"
+++

BlogをEC2で運用しているので、せっかくなのでネームサーバーもAmazonが提供しているRoute53 を使ってみたいと思います。
料金が少し気になるところですが、個人のサイトなら100円/月ぐらい見ておけばよさそうです(未確認)。

* [個人サイトでAmazon Route 53を一ヶ月間使った場合の料金](http://www.lancork.net/2014/05/amazon-route53-monthly-cost/)
* [Amazon Route 53 料金表](http://aws.amazon.com/jp/route53/pricing/)

今回変更するドメイン(10rane.com)はvalue-domainで管理しています。
作業の流れとしては、以下になります。

1. Route53にレコードを登録
2. レジストラ(value-domain)に設定しているネームサーバーを変更

### 変更前のネームサーバーの状態を確認

value-domainのネームサーバーを利用していることがわかります。

    $ nslookup -type=any 10rane.com
    10rane.com      nameserver = ns1.value-domain.com.
    10rane.com      nameserver = ns2.value-domain.com.
    10rane.com
            origin = ns1.value-domain.com
            mail addr = hostmaster.10rane.com
            serial = 1410401066
            refresh = 16384
            retry = 2048
            expire = 1048576
            minimum = 2560
            Name:   10rane.com
            Address: 49.212.146.214
　
 
    $ dig 10rane.com ns
    ;; ANSWER SECTION:
    10rane.com.             1124    IN      NS      ns2.value-domain.com.
    10rane.com.             1124    IN      NS      ns1.value-domain.com.

    $ dig 10rane.com soa
    ;; ANSWER SECTION:
    10rane.com.             802     IN      SOA     ns1.value-domain.com. hostmaster.10rane.com. 1410401066 16384 2048 1048576 2560

### Route53でZoneの設定

まずは`Zone`の設定をします。`Zone`を設定すると`NS`と`SOA`のレコードが自動で作成されます。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_1.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_2.jpg)

次にサブドメインの`A`レコードを登録します。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_3.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_4.jpg)

### ネームサーバーの変更

レジストラ(value-domain)に設定しているネームサーバーをRoute53のネームサーバーに切り替えます。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_5.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_6.jpg)

### 変更後のネームサーバーの状態を確認

1,2時間くらいで、DNSが切り替わります。

    $ nslookup -type=any 10rane.com
    10rane.com      nameserver = ns-918.awsdns-50.net.
    10rane.com      nameserver = ns-1101.awsdns-09.org.
    10rane.com      nameserver = ns-1918.awsdns-47.co.uk.
    10rane.com      nameserver = ns-157.awsdns-19.com.
    10rane.com
            origin = ns-918.awsdns-50.net
            mail addr = awsdns-hostmaster.amazon.com
            serial = 1
            refresh = 7200
            retry = 900
            expire = 1209600
            minimum = 86400
　
 
    $ dig 10rane.com ns
    10rane.com.             86400   IN      NS      ns-157.awsdns-19.com.
    10rane.com.             86400   IN      NS      ns-918.awsdns-50.net.
    10rane.com.             86400   IN      NS      ns-1918.awsdns-47.co.uk.
    10rane.com.             86400   IN      NS      ns-1101.awsdns-09.org.

    $ dig 10rane.com soa
    10rane.com.             888     IN      SOA     ns-918.awsdns-50.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400

### 参考サイト

* [Route53 : ValueDomain](http://qiita.com/SatoHiroyuki/items/7d45bd98a477f021cc82)
* [はじめてのRoute 53](http://www.dondari.com/%E3%81%AF%E3%81%98%E3%82%81%E3%81%A6%E3%81%AERoute_53)
* [営業でも簡単！Route 53の基本設定](http://blog.serverworks.co.jp/tech/2013/03/08/route53_basic/)

### 関連する記事
* [Route53にドメインを移管する](http://blog.10rane.com/2014/09/16/transferred-a-domain-to-route53/)
* [aws はじめました。](http://blog.10rane.com/2014/07/21/start-aws/)
