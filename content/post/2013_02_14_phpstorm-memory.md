+++
date = "2013-02-14T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "PhpStormのメモリ使用量を増やす"
slug = "phpstorm-memory"
+++

こちらのサイトの[記事](http://webinthelife.com/?p=274)まんまなんですが、結構速くなったのでメモ。

```
emacs /Applications/PhpStorm.app/Contents/Info.plist

<key>VMOptions.x86_64</key>
<string>-Xms128m -Xmx800m -XX:MaxPermSize=350m -XX:ReservedCodeCacheSize=64m -XX:+UseCodeCacheFlushing -XX:+UseCompressedOops</string>
↓
<key>VMOptions.x86_64</key>
<string>-Xms256m -Xmx1024m -XX:MaxPermSize=512m -XX:ReservedCodeCacheSize=128m -XX:+UseCodeCacheFlushing -XX:+UseCompressedOops</string>
```

![](https://dl.dropboxusercontent.com/u/159938/blog_images/php_storm_memory_001.png)
 -> 
![](https://dl.dropboxusercontent.com/u/159938/blog_images/php_storm_memory_002.png)

最大791Mだったのが、1015Mになり、少し速くなりました。  

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

- [Mac版PHPStormのメモリ割り当てを増やして快適に](http://webinthelife.com/?p=274)
