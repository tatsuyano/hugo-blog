+++
date = "2014-07-02T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["peco", "go"]
title = "流行りのpecoをcentosにインストールしました"
slug = "installed-the-peco-to-centos"
+++

最近購読しているブログとかに、pecoインストールしました的な記事をよく見かけるようになったので、御多分に洩れず入れてみました。

で、そもそもpecoって何なの？って話なんですが、[README](https://github.com/peco/peco/blob/master/README.md)に書いてあるように、コマンドの履歴をいい感じにフィルタリングしてくれるツールみたいです。

元々はpercolというpythonで作られたツールを、JPAのMakiさんがgolangで作りなおしたものらしいです。
ツール作るのに、golang流行ってますね。。

こうやってperlの人が他の言語で開発しているのを見ると、少しノスタルジックな気持ちになるのは秘密です。

### golangのインストール

anyenvでバージョン管理できるのかなと思ったけどなかった & yumのリストになかった(2014/06)ので、手動でmake?しました。
まずは[golangのパッケージ](http://golang.org/dl/)を選びます。対象はcentos6の64bit(2014/06)。

[インストール方法](http://golang.jp/install#install)は、詳しくは本家の翻訳サイトに乗っています。

    $ sudo bash
    $ cd /usr/local/src
    # wget http://golang.org/dl/go1.3.linux-amd64.tar.gz
    # tar -C /usr/local -xzf go1.3.linux-amd64.tar.gz

### golangの環境変数

今回はシステムワイドにインストールしたので、/etc/profileに設定します。

    # emacs /etc/profile

    export GOROOT=/usr/local/go
    export GOPATH=/usr/local/mygo
    export PATH=$PATH:$GOPATH/bin:$GOROOT/bin

    # source /etc/profile

    # which go
    /usr/local/go/bin/go

`GOPATH`には、golangのソフトウェア(pecoとか)が入ります。

### pecoのインストール

golangのソフトウェアは、`go get`コマンドを使うみたいです。

    # go get github.com/peco/peco/cmd/peco

    # which peco
    /usr/local/mygo/bin/peco


* `Ctrl-r` ... リスト(履歴)の表示
* `Ctrl-g` ... リストから抜ける

### pecoの設定

pecoの設定はこちら([pecoを使い始めた](http://shibayu36.hatenablog.com/entry/2014/06/27/223538))を参考にさせてもらいました。

どうでもいい事なんですが、キーバインドのファイルはhome直下に`.config`というディレクトリを掘って、
設定ファイルを置く感じなんですが、home直下がかなりカオスな感じです。。みなさんどうしているんですかね。

#### 関連する記事
* [goパッケージ(peco)のバージョンを上げる](http://blog.10rane.com/2014/08/14/updating-the-package-go/)
* [pecoで接続先hostを選ぶスクリプトを書いた](http://blog.10rane.com/2014/08/21/select-host-for-peco/)

