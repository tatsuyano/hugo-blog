+++
date = "2016-04-06T15:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["js","nodejs","casperjs"]
title = "CasperJsでAlert、Confirmのイベントをキャッチする"
slug = "i-have-catched-alert-or-confirm-events-in-casperjs"
+++

AlertやConfirmダイアログのOKボタン、キャンセルボタンを押下するには、`casper.page.onAlert`、`casper.page.onConfirm`を使う。

下の例では、購入ボタン(#buy)を押下すると、
Confirmダイアログが立ち上がり `商品を購入します。よろしいですか？`とメッセージが表示される。
OKボタンを押下すると、商品購入処理が実行され
商品の購入が成功すれば `商品を購入しました`、
ポイントが足りず購入に失敗すると `ポイントが足りません` とメッセージが表示される。

``` javascript
var casper = require('casper').create({clientScripts: ['./src/includes/jquery.min.js']});

var point = 100;

casper.start();
casper.open('http://192.168.33.10/shopping/cart').viewport(1024, 690);

casper.then(function() {

  // AlertダイアログはOKボタンしかないので、返り値は必ずtrueを返す。
  // 同一画面で、複数のダイアログを表示する場合は、引数のメッセージで分岐する
  this.page.onAlert = function(message) {
    if (message == '商品を購入しました') {
        return true;
    }
    else if (message == 'ポイントが足りません') {
        return true;
    }
  };

  // Confirmダイアログが立ち上がると発火する。
  // trueを返すとOKボタンが押下され、falseを返すとキャンセルボタンが押下される
  this.page.onConfirm = function(message) {
    if (message == '商品を購入します。よろしいですか？') {
        return true;
    }
  };

  // #add_pointというテキストフィールドに100ポイントを入力し、#buyボタンを押下する
  this.evaluate(function(p) {
    $('#add_point').val(p);
    document.getElementById("buy").click();
  }, point);
});

casper.run();
```

CasperJsはPhantomJsを拡張(内封)しているライブラリなので、PhantomJsをメソッドをそのまま?使うことができる。
今回の`onAlert`などもPhantomJsで定義されているメソッドで、`this.page`の中にPhantomJsが入っている。


### 参考サイト

* [PhantomJS + Jasmine で alert, confirm, prompt などの標準ダイアログのテスト方法](http://tetsuwo.tumblr.com/post/58696806251/jasmine-phantomjs-alert-confirm-prompt)
* [onAlert](http://docs.casperjs.org/en/latest/modules/casper.html#onalert)
