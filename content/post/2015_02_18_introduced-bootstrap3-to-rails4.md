+++
date = "2015-02-18T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "Rails4でBootstrap3を導入"
slug = "introduced-bootstrap3-to-rails4"
+++

※ [Bootstrapをより簡単に導入する(sass版)記事](/2015/03/31/introduced-bootstrap3-to-rails4-sass/)
もあるので、そちらも参照してみてください。

`twitter-bootstrap-rails`というGemを使って、Rails4にBootstrap3を導入します。

<pre><code class="language-bash">$ rails -v Rails 4.1.1
$ ruby -v  ruby 2.1.0p0
</code></pre>
<br>

## サンプルアプリの用意

まずはbootstrap3という名前のサンプルアプリを生成し、次に確認用のページを生成します。

<pre><code class="language-bash">$ rails new bootstrap3
$ cd bootstrap3
$ rails g scaffold Book title:string price:integer
$ rake db:migrate
$ rails server
</code></pre>
<br>

![](https://dl.dropboxusercontent.com/u/159938/blog_images/introduced-bootstrap3-to-rails4_01.jpg)

## Gemfileの設定

Bootstrapのcssがlessで書かれているので、Railsでもlessが扱えるようにし、さらにlessをコンパイルするためのGemもインストールします。

bootstrap3/Gemfile
<pre><code class="language-bash">gem 'therubyracer'            # javascript runtime。lessをコンパイルするために必要
gem 'less-rails'              # Railsでlessを使えるようにする。Bootstrapがlessで書かれているため
gem 'twitter-bootstrap-rails' # Bootstrapの本体
</code></pre>
<br>

Gemを反映します。
<pre><code class="language-bash">$ bundle install
</code></pre>
<br>

## Bootstrapの設定

まずはBootstrapのJsとCssを生成します。

<pre><code class="language-bash">$ rails g bootstrap:install
      insert  app/assets/javascripts/application.js
      create  app/assets/javascripts/bootstrap.js.coffee
      create  app/assets/stylesheets/bootstrap_and_overrides.css.less
      create  config/locales/en.bootstrap.yml
        gsub  app/assets/stylesheets/application.css
</code></pre>
<br>

### ヘッダー、フッターのBootstrap化

次にヘッダー、フッターをBootstrapのコンポーネントに差し替えるため、
`app/views/layouts/application.html.erb`を上書き(Overwrite)します。

<pre><code class="language-bash">$ rails g bootstrap:layout application
    conflict  app/views/layouts/application.html.erb
    Overwrite /bootstrap3/app/views/layouts/application.html.erb? (enter "h" for help) [Ynaqdh] Y
           force  app/views/layouts/application.html.erb
</code></pre>
<br>

![](https://dl.dropboxusercontent.com/u/159938/blog_images/introduced-bootstrap3-to-rails4_02.jpg)

### scaffoldしたページのBootstrap化

次に、scaffoldしたページ全体に対してBootstrap化をしていきます。
ページはすでに生成されているので、ここでも上書きするかメッセージが出ますが、すべて`Y`で上書きます。

<pre><code class="language-bash">$ rails g bootstrap:themed Books

    conflict  app/views/books/index.html.erb
Overwrite /bootstrap3/app/views/books/index.html.erb? (enter "h" for help) [Ynaqdh] Y
       force  app/views/books/index.html.erb
    conflict  app/views/books/new.html.erb

Overwrite /bootstrap3/app/views/books/new.html.erb? (enter "h" for help) [Ynaqdh] Y
       force  app/views/books/new.html.erb
    conflict  app/views/books/edit.html.erb

Overwrite /bootstrap3/app/views/books/edit.html.erb? (enter "h" for help) [Ynaqdh] Y
       force  app/views/books/edit.html.erb
    conflict  app/views/books/_form.html.erb

Overwrite /bootstrap3/app/views/books/_form.html.erb? (enter "h" for help) [Ynaqdh] Y
       force  app/views/books/_form.html.erb
    conflict  app/views/books/show.html.erb

Overwrite /bootstrap3/app/views/books/show.html.erb? (enter "h" for help) [Ynaqdh] Y
       force  app/views/books/show.html.erb
</code></pre>
<br>

![](https://dl.dropboxusercontent.com/u/159938/blog_images/introduced-bootstrap3-to-rails4_03.jpg)

## Glyphicons(Webフォント)の設定

Bootstrap3からGlyphiconsは、Webフォントなっており、別途サーバーにフォントをインストールする必要があります。

本家サイトからbootstrap本体をダウンロードし、fontを`app/vendor/assets/`配下にコピーします。


<pre><code class="language-bash">wget https://github.com/twbs/bootstrap/releases/download/v3.3.2/bootstrap-3.3.2-dist.zip
$ unzip bootstrap-3.3.2-dist.zip
$ cp -a bootstrap-3.3.2-dist/fonts /bootstrap3/vendor/assets/
</code></pre>
<br>

次にダウンロードしたフォントを読み込むため、configに設定します。

bootstrap3/config/application.rb
<pre><code class="language-css">module Bootstrap3
  class Application < Rails::Application
      config.assets.paths << "#{Rails}/vendor/assets/fonts" #<- 追加
...
</code></pre>
<br>

最後にlessファイルに対して、ダウンロードしたフォントのパスを設定します。

bootstrap3/app/assets/stylesheets/bootstrap＿and＿overrides.css.less
<pre><code class="language-bash">@font-face {
  font-family: 'Glyphicons Halflings';
  src: url('/assets/glyphicons-halflings-regular.eot');
  src: url('/asstes/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),
  url('/assets/glyphicons-halflings-regular.woff') format('woff'),
  url('/assets/glyphicons-halflings-regular.ttf') format('truetype'),
  url('/assets/glyphicons-halflings-regular.svg#glyphicons-halflingsregular') format('svg');
}
</code></pre>
<br>

試しにindex.html.erbに、Glyphiconsのspanタグを書いてみます。

bootstrap3/app/views/books/index.html.erb

<pre><code class="language-markup">&lt;%-&nbsp;model_class&nbsp;=&nbsp;Book&nbsp;-%&gt;
&lt;div&nbsp;class='page-header'&gt;
&nbsp;&nbsp;&lt;h1&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;%='.title',&nbsp;:default&nbsp;=&gt;&nbsp;model_class.model_name.human.pluralize.titleize&nbsp;%&gt;
&nbsp;&nbsp;&nbsp;&nbsp;&lt;span&nbsp;class='glyphicon&nbsp;glyphicon-heart'&nbsp;aria-hidden='true'&gt;&lt;/span&gt;
&nbsp;&nbsp;&lt;/h1&gt;
...
</code></pre>
<br>

![](https://dl.dropboxusercontent.com/u/159938/blog_images/introduced-bootstrap3-to-rails4_04.jpg)

## 参考サイト

* [RailsにTwitter Bootstrapの導入と簡易な使い方](http://ruby-rails.hatenadiary.com/entry/20140801/1406818800)
* [Rails4でサイト構築をする – Bootstrap導入編](http://techracho.bpsinc.jp/kazumasa-ogawa/2014_03_27/15892)
* [Bootstrap3をRails4で使うときのメモ](http://d.hatena.ne.jp/next49/20141110/p2)
* [rails4でbootstrap3のglyphiconを使う方法](http://hivecolor.com/id/125)

## 関連する記事
* [Rails4でBootstrap3を導入(sass版)](/2015/03/31/introduced-bootstrap3-to-rails4-sass/)
