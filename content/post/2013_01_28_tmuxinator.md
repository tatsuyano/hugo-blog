+++
date = "2013-01-28T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["linux"]
title = "tmuxinatorのインストール"
slug = "tmuxinator"
+++

## RubyGemsからtmuxinatorのインストール
念のため、RubyGemsをupdateしてからインストール

```
gem update
gem install tmuxinator
```

tmuxinatorがインストールされているか確認
```
gem which tmuxinator
usr/local/Cellar/ruby/1.9.3-p0/lib/ruby/gems/1.9.1/gems/tmuxinator-0.5.0/lib/tmuxinator.rb
```

## メッセージに従い、初期設定
```
echo "[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator" >> ~/.zshrc
source ~/.zshrc
```

## $SHELL、$EDITORの確認
設定が必須なので確認

```
echo $EDITOR $SHELL
emacs /bin/bash
```

## .tmuxinatorを移動させる

他のdotfile同様、Dropboxのディレクトリ配下に移動させ、リンクをはる。  
ただし「.tmuxinator」をリネームして「tmuxinator」としてはいけない。  
隠しファイルでないと、再度「.tmuxinator」というディレクトリが作成されてしまう。  

```
mv .tmuxinator Dropbox/dotfile/
ln -s Dropbox/dotfile/.tmuxinator .tmuxinator
```

## RubyGemsの環境変数をbash_profileに設定

```
cat .bash_profile
export GEM_BIN=$(ruby -e 'require "rubygems"; puts Gem::bindir')
export PATH=$GEM_BIN:$PATH

source .bash_profile
```

環境変数の確認

```
printenv | grep ruby
GEM_BIN=/usr/local/Cellar/ruby/1.9.3-p0/bin
PATH=/usr/local/Cellar/ruby/1.9.3-p0/bin:....
```

## tmuxinatorのプロジェクトを作成

```
mux new php
# ~/.tmuxinator/php.yml
# you can make as many tabs as you wish...

project_name: php
project_root: ~/
tabs:
  - doc:
        layout: main-vertical
        panes:
          - emacs Dropbox/doc/memo.md
          - #empty, will just run plain bash
          - sql: ./mysql -uroot -proot
          - hoge: ssh hoge
```

作成したプロジェクトの実行

```
mux php
```

プロジェクトを変更、反映するには、一度tmuxのセッションを閉じる

## 参考にしたサイト
以下のサイトを参考にさせていただきました。ありがとうございます！

- [tmux + tmuxinator が便利](http://d.hatena.ne.jp/akishin999/20121204/1354621657)
- [カッとなってUbuntu/DebianでRubygems実行コマンドのパスを通した](http://rh7.hateblo.jp/entry/20110220/1298209273)
