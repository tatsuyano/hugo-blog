+++
date = "2013-05-10T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog"]
title = "Octopressのコードスニペットの設定"
slug = "octopress-clippit"
+++

### Octopressの標準設定
rdiscountの場合、<code>```</code>で囲う。色は付かない。  

```
ls
CHANGELOG.markdown Gemfile.lock       Rakefile           config.rb          plugins            sass
Gemfile            README.markdown    _config.yml        config.ru          public             source
```

### scheme

<code>``` scheme</code>で囲う。自分はelispの場合などに使っている。

``` scheme
(defvar dvar 1 "実験用") ;; -> 1
(defvar dvar 2)         ;; -> 1 値が上書きされない
(setq dvar 3)           ;; -> 3 defvar以外で宣言(代入)された場合は上書きされてしまう。
```

### Js

<code>codeblock lang:js</code>で囲う。

<pre><code class="language-javascript">
class Animal
  constructor:(name) -> @name = name
      say:(word) -> console.log "#{@name} said: #{word}"

class Dog extends Animal
  constructor:(name) -> super name
      say:(word) -> super "Bowwow, #{word}"

dog = new Dog("Bob")
dog.say("hello")
</code></pre>

### PHP

[コードスニップ PHP](https://gist.github.com/clippit/2398211)からファイルをDLし、ファイルを上書きする。  

```
cd octopress/plugins
wget https://gist.github.com/clippit/2398211/raw/a320b384db407d32b0b2034bbc660f15576ff4aa/pygments_code.rb
```

<code>codeblock lang:phpinline</code>で囲う。

<pre><code class="language-php">
public $default = array(
    'datasource' => 'Database/Mysql',
    'persistent' => false,
    'host' => 'localhost',
    'login' => 'USERNAME',
    'password' => 'PASSWORD',
    'database' => 'DATABASE',
    'encoding' => 'utf8',
    'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock'
    );
</code></pre>

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Octopressでのコードの表示やコメントのあれこれ](http://rcmdnk.github.io/blog/2013/05/05/blog-octopress/)
* [コードスニップ PHP](https://gist.github.com/clippit/2398211)
* [Include Code Snippets](http://dreamrunner.org/wiki/public_html/docs/Web/octopress.html#sec-9-3)
