+++
date = "2014-09-09T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog", "nginx"]
title = "Amazon LinuxにGhostをインストールする"
slug = "how-to-install-and-setup-ghost-on-amazon-linux"
+++

新たに`t2.micro`のインスタンスを立ち上げたので、Ghost を新規でインストールしようと思います。

### Node.js のインストール

`anyenv`を使って、`0.10.0`以上のNode.js をインストールしてください。

    $ which node
    HOME/.anyenv/envs/ndenv/shims/node

    $ which npm
    $HOME/.anyenv/envs/ndenv/shims/npm

    $ node --version
    v0.10.26

* [anyenvをインストールする](http://blog.10rane.com/2014/08/19/install-anyenv/)

### nginx のインストール

`yum`でのバージョンは1.4.7と少し古いですが、面倒なのでインストール

    $ sudo yum -y install nginx

    $ /usr/sbin/nginx -v
    nginx version: nginx/1.4.7

    $ sudo chkconfig nginx on
    $ sudo chkconfig --list | grep nginx
    nginx           0:off   1:off   2:on    3:on    4:on    5:on    6:off

念のため起動テスト

    $ sudo /etc/init.d/nginx start

#### confの設定

特定のドメインのみを停止(切り離)したりするために、
設定ファイルのリンクを貼ったディレクトリを`include`します。

`sites-available`にはドメインごとの設定ファイル、
`sites-enabled`には設定ファイルのリンクを貼ります。

    $ sudo bash
    # cd /etc/nginx
    # mkdir sites-available sites-enabled

    # touch sites-available/blog.10rane.com.conf
    # ln -s /etc/nginx/sites-available/blog.10rane.com.conf /etc/nginx/sites-enabled/blog.10rane.com.conf

sites-available/blog.10rane.com.conf

    server {
        listen 80;
        server_name blog.10rane.com;

        location / {
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   Host      $http_host;
            proxy_pass         http://127.0.0.1:2368;
        }
    }

シンボリックリンク`sites-enabled`を`httpディレクティブ`に`include`します。

/etc/nginx/nginx.conf

    http {
        include       /etc/nginx/sites-enabled/*.conf; # for Ghost
    ....

Nginx の再起動

    $ sudo /etc/init.d/nginx restart

* [LinuxにGhostをインストールする方法](http://docs.ghost.org/ja/installation/linux/)

### Ghost のダウンロード&インストール

`npm start`を実行すると、フォアグラウンドで実行される。実運用するためには、
Ghost を永続的に動かすためには、Nodeパッケージの`forever`などを使う必要があります。

    $ mkdir $HOME/app
    $ cd app
    $ curl -L https://ghost.org/zip/ghost-latest.zip -o ghost.zip
    $ unzip -uo ghost.zip -d ghost

    $ cd ghost
    $ npm install --production
    $ npm start

$HOME/app/ghost/config.js

    config = {
        // ### Development **(default)**
        development: {
            // The url to use when providing links to the site, E.g. in RSS and email.
    //        url: 'http://my-ghost-blog.com',
            url: 'http://blog.10rane.com',
    ...

        production: {
    //        url: 'http://my-ghost-blog.com',
            url: 'http://blog.10rane.com',

### forever のインストール

Ghost をバックグラウンドで動かすために`forever`を使います。
`forever`は別パッケージでもよく使うので、`-g`オプションを指定してグローバルインストールします。

    $ npm install forever -g

    $ ndenv which forever
    $HOME/.anyenv/envs/ndenv/versions/v0.10.26/bin/forever

    $ source $HOME/.zshrc

`forever`の起動

    $ cd $HOME/app/ghost
    $ NODE_ENV=production forever start index.js

これでGhost を運用できるようになりました。  
お疲れ様でした。

### 参考サイト

* [anyenvをインストールする](http://blog.10rane.com/2014/08/19/install-anyenv/)
* [LinuxにGhostをインストールする方法](http://docs.ghost.org/ja/installation/linux/)
* [Foreverを使う場合](http://docs.ghost.org/ja/installation/deploy/#foreverを使う場合-(https://npmjs.org/package/forever))
