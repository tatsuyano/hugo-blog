+++
date = "2013-02-23T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["git"]
title = "作業ごとにブランチを切る(git branch)"
slug = "git-branch"
+++

## branchを切る

機能追加、バグ修正など、作業ごとにブランチを切るべき

ブランチの作成
```
git branch new-branch
```

ブランチを確認する
```
git branch -a
* master # <- 現在のブランチがmasterであることがわかる
  new-branch
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
```

作成したブランチに切り替える
```
git checkout new-branch
Switched to branch 'new-branch'
```

ブランチを作成すると同時にブランチを切り替える
```
git checkout -b new-branch
Switched to a new branch 'new-branch'
```

ブランチの削除
```
git branch -d new-branch
Deleted branch new-branch (was 6de2d75).

# 削除対象のブランチを選択していると削除できない
git branch -d new-branch
error: Cannot delete the branch 'new-branch' which you are currently on.

# この場合、いったんmasterに切り替えてから削除
git checkout master
git branch -d new-branch
```

## branchをmasterにmergeする

```
# まずはmasterにcheckout
git checkout master

# 対象のbranchをmerge
git merge new-branch
```
