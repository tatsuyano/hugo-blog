+++
date = "2014-08-21T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["zsh", "peco", "go"]
title = "pecoで接続先hostを選ぶスクリプトを書いた"
slug = "select-host-for-peco"
+++

pecoを使って、sshするhostを選択するスクリプトを書きました。

.oh-my-zsh/custom/peco.zsh

    function peco-select-host () {
        host=$(grep -iE '^host\s+(\w|\d)+' ~/.ssh/config | awk '{print $2}' | peco)

        if [ -n "$host" ]; then
            ssh $host
        fi
    }
    alias hs=peco-select-host

元々ある`~/.ssh/config`をgrepしてhost名を渡してるだけ。  
1、2文字のキーバインド(ストローク？)がなかったので、aliasで`hs`にしました。

    $ hs
    QUERY>
    clgit
    support
    test01
    ghost    #<-接続先のhostを選ぶ

peco楽しいですね。お疲れ様でした。

#### 関連する記事
* [流行りのpecoをcentosにインストールしました](http://blog.10rane.com/2014/07/02/installed-the-peco-to-centos/)
* [goパッケージ(peco)のバージョンを上げる](http://blog.10rane.com/2014/08/14/updating-the-package-go/)
