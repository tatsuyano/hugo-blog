+++
date = "2013-02-18T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["git"]
title = "Nike Flyknit Lunar1+"
slug = "nike-flyknit-lunar-plus"
+++

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_001.jpg" width="70%" />

青系のシューズがほしくて、いろいろなメーカのものを眺めていたが、つい買ってしまいました。Nike Flyknit Lunar1+。  
このFlyknitシリーズは3種類、Racer、Trainer+、Lunar1+とあって、Luna1+はエントリーモデルになる。  
Nikeショップの人の話しでは、Racerがサブ３、Trainer+がサブ４、Lunar1+が4〜4.5時間ぐらいが目安とのことだったのでLunar1+をチョイス。  

## サイズ

自分の正確なサイズは24.5〜25.0(甲高)なので、いつもはだいたい25インチを買っているのだが、このLunar1+では甲の部分に圧迫感があったので、25.5インチにした。  

## 軽さ

人によって違うだろうが、自分がいつも履いてるadidas Boston2と較べて明らかに軽さを体感できた。ただ10km、20kmだとあまりその軽さの良さを感じない。  
たぶんフルマラソンぐらいじゃないと、その軽さの恩恵を感じないと思う。

## クッション性

見た目はそれなりに厚みがあるが、自分は少しクッション性は低いと感じた。  
たぶんいつも履いているadidas Boston2がクッション性が高いシューズだからだと思う。ただ軽さを考えれば、十分だと思う。  

## ソックス

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_002.jpg" width="40%" />

自分はシューズと同じ色に合わせたい派なので、同じNikeの「ランニング エリート リサイクル クッション ローカット タブ ソックス」のネオタークのLサイズを購入。  
このソックス、実によくできていて、くるぶしの前後ろの２箇所が厚くなっており、靴ずれを防いでくれている。  
Nikeはこういう細かい部分もよく考えているなあと少し感心。

## 総評

値段は少し高め(14,800円)のシューズではあるが、その軽さやデザイン性を考えると「買い」だったと思う。  
まだ10kmちょっとしか走っていないので、もう少し走りこんでから、改めてレビューをまとめたいと思う。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_003.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_004.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_005.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_006.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_007.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_008.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_009.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_010.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_011.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/nike_flyknit_lunar1_plus_012.jpg)
