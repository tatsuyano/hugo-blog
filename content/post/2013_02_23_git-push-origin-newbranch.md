+++
date = "2013-02-23T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["git"]
title = "master以外のbranchをリモート(origin)にpushする"
slug = "git-push-origin-newbranch"
+++

master以外の別branchをリモートにpushする

```
git push origin new-branch

git branch -a
  master
* new-branch
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/new-branch # <- 追加される
```


origin/master以外のリモートにあるbranchをローカルに追加する

```
git branch new-branch origin/new-branch # <- 本来ならこれでうまくいくのだが

# なぜかすでにローカルにあるリポジトリから、新たに追加されたリモートのブランチが参照されていない
git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
# remotes/origin/new-branch <- 本来参照されるはずのブランチ
```

うまくいかないので、いったんローカルのリポジトリを破棄し、新たにcloneしなおしてみる

```
mv local-repository local-repository.bk

git clone ssh://hoge/var/git/remote-repository.git local-repository
cd local-repository/

git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/new-branch # <- 追加されている
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

- [remote に branch を push し、remote から clone したリポジトリの branch を変更して push する](http://blog.basyura.org/entry/20100323/p1)
