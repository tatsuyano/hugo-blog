+++
date = "2013-03-19T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "el-get.elでパッケージ管理を行う (el-get.elのインストール)"
slug = "emacs-el-get"
+++

今までパッケージを手動でインストールしていて、自分が何のパッケージをインストールしたかを  
把握できなくなってきていたので、el-get.elでパッケージ管理をすることにしました。  

## el-get.elでのパッケージ管理の特徴
自分がel-get.elでいいなあと思ったのは、以下の２つ  

* 自分がどのパッケージをインストールしているのかを簡単に把握できること
* レシピといって、パッケージごとのインストール方法を柔軟に設定できること

## emacsのバージョン
emacsのバージョンは homebrew でインストールした「24.1.1」です。  

## emacs.dの構成
今回、今使っているemacs.dを捨てて、一から作り直すことにしました。  

.emacs.d  
├── conf ... init-loaderで分割したファイルを入れる  
└── elisp ... 手動でパッケージを入れた場合はここに直接入れる  
│　　    ├── el-get       ... el-get.elでインストールしたパッケージをディレクトリ単位で入れていく  
│　　    └── auto-install ... auto-installでインストールした場合  
│    
└── init.el ... .emacs.elの代わり。load-pathの設定など  
  
今回は .emacs.el は使わずに代わりに .emacs.d 直下に init.el というファイルを作成します。  
また、できるかぎり el-get.el でパッケージ管理を行なっていき、インストールできなかった場合は auto-install でインストールを行うことにします。  
auto-install でもインストールできなかった場合は、elispディレクトリ直下に手動でインストールしていきます。  

今後のインストール方法(パッケージ管理)の優先度  
1.el-get > 2.auto-install > 3.手動

## el-get.elのインストール
まずは el-get.el 本体のインストールから。事前に上記の .emacs.d の構成を作っておくこと。  
el-get.el でインストールしたパッケージは、.emacs.d/elisp/el-get/ 直下に入れていきます。  
init.el に以下の設定を入れて、emacsを起動させると自動でインストールが始まります。  

init.el
``` scheme
(setq load-path
  (append
    (list
      (expand-file-name "~/.emacs.d/")
      (expand-file-name "~/.emacs.d/elisp/")
      (expand-file-name "~/.emacs.d/elisp/el-get/el-get/")
      (expand-file-name "~/.emacs.d/elisp/auto-install/")
    )
    load-path))

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
    (url-retrieve-synchronously
      "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(el-get 'sync)
```

以下のサイトを参考にさせていただきました。ありがとうございます！

* [el-get を使って Emacs でパッケージ管理](http://shishithefool.blogspot.jp/2012/04/el-get-emacs.html)
* [el-getを使ってみる(インストール編)](http://shibayu36.hatenablog.com/entry/2013/03/14/081524)
* [github dimitri/el-get](https://github.com/dimitri/el-get)
