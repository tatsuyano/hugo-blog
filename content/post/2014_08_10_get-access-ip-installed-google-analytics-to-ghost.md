+++
date = "2014-08-10T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog", "nginx"]
title = "GhostにGoogle Analyticsに設置しアクセス元IPを取得する"
slug = "get-access-ip-installed-google-analytics-to-ghost"
+++

アクセス元IPを取得するには、Google Analyticsのデフォルト設定ではできません。
IPを取得、集計するには

1. Google Analyticsにdimensionを設定し、アクセス元IPを集計できるようにする
2. PHPを使って、blogの参照時のIPを取得する
3. 取得したIPをdimensionに渡す
4. 集計結果をGoogle Analyticsで参照できるよう、カスタムレポートを作成する

という手順で対応します。

#### この記事で紹介するシステム構成
* CentOS6
* Ghost
* Nginx

何はともあれ、まずは[GoogleAnalytics](http://www.google.com/intl/ja_jp/analytics/)を導入してください。

* [初心者必見！【Google Analytics(アナリティクス)】の導入と使い方](http://viral-community.com/wordpress/google-analytics-setting-864/)
* [How to Add Google Analytics to Ghost](http://www.ghostforbeginners.com/how-to-add-google-analytics-to-ghost/)


#### Google Analyticsにdimensionを設定し、アクセス元IPを集計できるようにする

Analyticsが提供していない値(今回でいうところのアクセス元IP)を集計するには、dimension(変数)を設定し、その変数に値を代入することで、集計することができるようになります。

`アナリティクス設定 > カスタム定義 > カスタム ディメンション`
![](https://dl.dropboxusercontent.com/u/159938/blog_images/get-access-ip-installed-google-analytics-to-ghost01.png)

`新しいカスタム ディメンション > カスタム ディメンションを編集 > 保存`
![](https://dl.dropboxusercontent.com/u/159938/blog_images/get-access-ip-installed-google-analytics-to-ghost02.png)

保存すると`dimension1`という変数がAnalyticsに設定されます。


#### PHPを使って、blogが参照された時のIPを取得する

このblogにはGhostを動かすための最小の設定しかしていなかったので、まずはNginx上でPHPが動く環境を整えます。

    # yum -y install php php-fpm

次に、事前に用意したdomainをバーチャルホスト(VH)に割り振り、そのVHに対してPHPが動くように設定します。

/etc/nginx/conf.d/php.conf

````
server {
    listen       80;
    server_name  ga.10rane.com;

    location / {
          root   /var/www/html;
          index  index.html index.htm index.php;
    }

    location ~ \.php$ {
          root           html;
          fastcgi_pass   127.0.0.1:9000;
          fastcgi_index  index.php;
          fastcgi_param  SCRIPT_FILENAME  /var/www/html$fastcgi_script_name;
          include        fastcgi_params;
    }
}
```

php-fpmの起動のおよびNginxの再起動

    # /etc/init.d/php-fpm start
    # chkconfig php-fpm on
    # /etc/init.d/nginx restart

`/var/www/html/`配下に、アクセス元IPを取得するスクリプトを配置します。  
このスクリプト自体はPHPですが、ブラウザ上からはJsとして認識させたいので、
`header`には`header("Content-type: application/x-javascript");`を設定します。

/var/www/html/remote_addr.php

```
<?php
header("Content-type: application/x-javascript");
echo "var remote_addr = '" . $_SERVER['REMOTE_ADDR'] . "';";
?>
```

`http://ga.10rane.com/remote_addr.php`にアクセスし、自分(アクセス元)のIPが参照されれば完了です。

* [CentOSにてnginxでPHPを動かす](http://qiita.com/utano320/items/36b6eac2bbd5bb5657f6)
* [javascriptでPHPファイルを実行してそれを出力する方法](http://ysklog.net/javascript/1870.html)
* [Google AnalyticsでIPアドレスを表示できるタグ「analyticsIP」を公開](http://lfll.blog73.fc2.com/blog-entry-258.html)


#### 取得したIPをdimensionに渡す
先ほど作成したソースで、変数`remote_addr`にIPを代入するようにしたので、
今度は`remote_addr`をAnalyticsの`dimension1`に代入します。

変更前のタグ

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-****', 'auto');
      ga('send', 'pageview');
    </script>

変更後のタグ。Analyticsのタグより前に先ほど用意したスクリプト(PHPファイル)をJsとして読み込んでいます。

    <script type="text/javascript" src="//ga.10rane.com/remote_addr.php"></script>
    <noscript><img src="//ga.10rane.com/remote_addr.php"></noscript>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-****', 'auto');
      ga('set', 'dimension1', remote_addr);
      ga('send', 'pageview');
    </script>

ghostの再起動。このブログは`forever`で管理しているので、以下のように再起動します。

    $ cd src/ghost/
    $ forever stop index.js
    $ NODE_ENV=production forever start index.js

#### 集計結果をGoogle Analyticsで参照できるよう、カスタムレポートを作成する

`カスタム > 新しいカスタム レポート >`
![](https://dl.dropboxusercontent.com/u/159938/blog_images/get-access-ip-installed-google-analytics-to-ghost03.png)

`ディメンションを追加 > カスタムディメンション > remote_addr(自分で設定した名前)`
![](https://dl.dropboxusercontent.com/u/159938/blog_images/get-access-ip-installed-google-analytics-to-ghost04.png)

`カスタム > 設定したカスタムレポート`
![](https://dl.dropboxusercontent.com/u/159938/blog_images/get-access-ip-installed-google-analytics-to-ghost05.png)

集計結果は翌日反映されるので、明日以降にIPの集計が確認できれば成功です。

お疲れ様でした。
