+++
date = "2015-06-08T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["js"]
title = "Jsのスコープについて"
slug = "for-the-scope-of-the-js"
+++

## グローバルスコープとローカルスコープしかない

Jsには2種類のスコープしかない。グローバルスコープ内で宣言した変数をグローバル変数、ローカルスコープ内で宣言した変数をローカル変数という。

* スクリプト全体(トップレベル)で有効なグローバル変数
 - プログラム終了時までメモリを確保してしまう
 - 多用すると、名前がバッティングする可能性がある
<br>
* 関数内でのみ有効なローカル変数
 - 関数終了時にメモリが開放される
 - 宣言時に`var`をつけないと、*グローバル変数*として認識されてしまう

<pre><code class="language-javascript">var hoge = 'global';  // => グローバル変数

function func () {
  var fuga = 'local'; // => ローカル変数、必ずvarをつける
  console.log(fuga);
}
</code></pre>
<br>

また、他言語のようにブロックスコープは存在しない。

<pre><code class="language-javascript">if (true) {
  var hoge = 'block';
}

console.log(hoge); // => block ブロック内で宣言した変数も参照できてしまう
</code></pre>
<br>

## 変数の巻き上げ(hoisting)

Jsには「変数の巻き上げ」という概念がある。

<pre><code class="language-javascript">var hoge = 'global';

function func() {
  console.log(hoge); // => undefined なぜか global と出力されない

  var hoge = 'local';
  console.log(hoge); // => local 期待どおりの出力
}
</code></pre>

一回目の`console.log(hoge);`で、なぜか`undefined`が出力されている。これが「変数の巻き上げ」の挙動。

何が起きているかというと、Jsでは、*関数内のどこで変数宣言をしても、関数の先頭で、宣言のみしたと認識されてしまうため。*

つまり上記のコードは以下のように処理されている。

<pre><code class="language-javascript">var hoge = 'global';

function func() {
  var hoge; // 初期値は入っていない。宣言のみ
  console.log(hoge); // => 初期値が入っていないので、undefined

  hoge = 'local';
  console.log(hoge); // => 値が入っているので、local
}
</code></pre>

これを防ぐには、変数宣言は、*関数の先頭*で行うようにする。

## まとめ

* グローバル変数は多用しない
* 変数宣言時は、必ず var をつける
* ローカル変数は、関数の先頭で宣言する

## 参考サイト

* [JavaScript のスコープを理解する](http://tacamy.hatenablog.com/entry/2012/12/30/191125)
* [JavaScriptのスコープを理解しよう](http://itpro.nikkeibp.co.jp/atcl/column/14/091700069/091700002/)
* [知らないと怖い「変数の巻き上げ」とは？](http://analogic.jp/hoisting/)

## 関連記事

* [Jsの関数について](/2015/06/08/for-function-of-js/)
