+++
date = "2015-05-13T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "heroku", "rails"]
title = "Rails4の非公開情報をdotenvで管理する"
slug = "dotenv-manages-secret-of-rails4"
+++

TwitterのAPIキーなど、公開したくない情報をどこに設定すればいいか調べたところ、
`config/secrets.yml`に設定して、`.gitignore`して使うようなので、設定してみました。

<pre><code class='language-ruby'># config/secrets.yml
development:
  twitter_api_key: hogefuga.....

# 呼び出し側
key = Rails.application.secrets.twitter_api_key
</code></pre>
<br>

しかしherokuを利用する場合、この`secrets.yml`というファイル自体必須なので、`.gitignore`するわけにはいきません。

* [How to solve error "Missing secret_key_base   for production environment" on
Heroku](http://stackoverflow.com/questions/23180650/how-to-solve-error-missing-secret-key-base-for-production-environment-on-h)

## dotenv-railsの設定

そこで検討した結果、`dotenv-rails`というGemを使うことにしました。
このGemを使うと、アプリケーションごとに環境変数(非公開情報)が設定できるようになり、
`secrets.yml`に公開したくない情報を設定しなくても良くなります。

<pre><code class='language-ruby'># Gemfile
gem 'dotenv-rails'
</code></pre>

まず、アプリケーションのディレクトリの直下に`.env`というファイルを作成し、環境変数を設定します。

<pre><code class='language-ruby'># .env
TWITTER_API_KEY="hogehoge"
TWITTER_API_SECRET="fugafuga"
</code></pre>

読み出す時は、以下のように指定します。
<pre><code class='language-ruby'>puts ENV['TWITTER_API_KEY'] # => "hogehoge"
</code></pre>

最後に`.env`ファイルを`.gitignore`します。  
以上で完了です。

## herokuに環境変数を設定する

ちなみに、herokuに環境変数を設定するには以下のように指定します。

<pre><code class='language-bash'>$ heroku config:add TWITTER_API_KEY="hogehoge"    --app アプリ名
$ heroku config:add TWITTER_API_SECRET="fugafuga" --app アプリ名
</code></pre>
<br>

## 参考サイト

* [Rails4.1のsecrets.ymlはfigaroと違うの？](http://xoyip.hatenablog.com/entry/2014/04/09/214405)
* [Rails セキュリティガイド 10 利用環境のセキュリティ
](http://railsguides.jp/security.html#%E5%88%A9%E7%94%A8%E7%92%B0%E5%A2%83%E3%81%AE%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)
* [Rails4.1でherokuへのデプロイに失敗(Missing 'secret_key_base')](http://jangajan.com/blog/2014/10/05/secrets-dot-yml-on-heroku-in-rails4-dot-1/)
* [RailsチュートリアルをRails(4.1)で勉強する：secret_key_baseからsecrets.yml](http://t4traw.github.io/20141215/study-rails-secrets-yml.html)
* [環境によって変わる設定値はdotenvを使うと便利](http://qiita.com/closer/items/f8d8ba00ae86d7051764)

