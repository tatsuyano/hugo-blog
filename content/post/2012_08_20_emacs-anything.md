+++
date = "2012-08-20T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "anything.elをインストールする"
slug = "emacs-anything"
+++

## はじめに

すでにauto-intall.elがインストールされている前提で話をすすめます。  
auto-installのインストール手順は[こちら](/2012/08/20/emacs-auto-install/)をご覧ください。  

##インストール手順

emacsを起動し、

```
M-x auto-install-batch <RET>
Extension name: anything <RET>
```

auto-isntall-batchを実行、次にインストールしたいパッケージ名(anything)を入力します。  
すると自動で複数ソースのダウンロードが始まります。  
ダウンロードし終わると「モード行」に  

```
Type C-c C-c to continue; Type C-c C-d for view diff; Type C-c C-q to quit.
```

というメッセージが出てくるので、「C-c C-c」と入力し、ダウンロードしたソースを順番にコンパイルしていきます。  
すべてコンパイルし終えると、以下のようなメッセージが表示され、インストールが完了します。  

```
Installation is completed.
```

## インストールされるソース一覧

``` scheme
cd .emacs.d/elisp/
tree
.
├── anything-auto-install.el
├── anything-auto-install.elc
├── anything-complete.el
├── anything-complete.elc
├── anything-config.el
├── anything-config.elc
├── anything-grep.el
├── anything-grep.elc
├── anything-gtags.el
├── anything-gtags.elc
├── anything-ipa.el
├── anything-ipa.elc
├── anything-match-plugin.el
├── anything-match-plugin.elc
├── anything-menu.el
├── anything-menu.elc
├── anything-migemo.el
├── anything-migemo.elc
├── anything-obsolete.el
├── anything-obsolete.elc
├── anything-show-completion.el
├── anything-show-completion.elc
├── anything-startup.el
├── anything-startup.elc
├── anything.el
├── anything.elc
├── descbinds-anything.el
├── descbinds-anything.elc
├── ipa.el
└── ipa.elc
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [auto-install.elでEmacsLispパッケージを自動インストール -> anything.el関連を一括インストール](http://d.hatena.ne.jp/rubikitch/20091221/autoinstall)
