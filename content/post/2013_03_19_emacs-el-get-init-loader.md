+++
date = "2013-03-19T06:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "el-get.elでinit-loader.elをインストールする"
slug = "emacs-el-get-init-loader"
+++

#### インストールしたいパッケージが、el-get のパッケージリストにない場合
自分でレシピを作成(設定)し、パッケージリストに追加します。リストに追加する方法は２つあります。  

* レシピ(rcpファイル)を作成し .emacs.d/elisp/el-get/el-get/recipes/ 以下にファイルを置く方法
* init.el などに直接設定を記述する方法

どちらも設定する内容は一緒なので、今回は直接設定することにします。設定する内容は主に以下のようになります。  

```
(:name {パッケージ名}
       :type {パッケージ配布方法: elpa, emacswiki, git, svn, http, ...}
       :url {パッケージURL}
       :after {後処理} ...)
```

* [el-getありきのinit.elに書き換えてみた](http://d.hatena.ne.jp/koshigoeb/20110503/1304425417)

#### 最新のinit-loader.el
init-loader.elの最新ファイルは 現在[gist](https://gist.github.com/zqwell/1021706) にあるので、  
まずは [rawファイル](http://blog.10rane.com/tech/2013/03/19/gist-raw-url/)のURLを確認します。
  
https://raw.github.com/gist/1021706/init-loader.el  
  
#### レシピの追加
init.el
``` scheme
(setq el-get-dir "~/.emacs.d/elisp/el-get/")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
    (url-retrieve-synchronously
      "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(setq el-get-sources
      '(
      (:name init-loader
      :type http
      :url "https://raw.github.com/gist/1021706/init-loader.el"
      :description "[My Recipes] Split management init.el.")
      ))
(el-get 'sync)
```

#### インストール
レシピが正しい場合、 M-x el-get-list-packages で init-loader が追加されているので、インストールできます。  

#### init.elの分割
今まで、init.elにすべて記述していた内容をパッケージごとに分割します。  
  
conf  
├── 00-basic.el  
├── 01-truncate-lines.el  
├── 10-el-get.el  
├── 11-auto-install.el  
└── 12-auto-complete.el  
  
init.el
``` scheme
(setq load-path
  (append
  (list
  (expand-file-name "~/.emacs.d/")
  (expand-file-name "~/.emacs.d/elisp/")
  (expand-file-name "~/.emacs.d/elisp/el-get/init-loader/")
  (expand-file-name "~/.emacs.d/elisp/el-get/el-get/")
  (expand-file-name "~/.emacs.d/elisp/auto-install/")
  )
  load-path))

(require 'init-loader)
(init-loader-load "~/.emacs.d/conf")
```

10-el-get.el
``` scheme
(setq el-get-dir "~/.emacs.d/elisp/el-get/")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
    (url-retrieve-synchronously
    "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(setq el-get-sources
      '(
      (:name init-loader
      :type http
      :url "https://raw.github.com/gist/1021706/init-loader.el"
      :description "[My Recipes] Split management init.el.")
      ))

(el-get 'sync)
```
