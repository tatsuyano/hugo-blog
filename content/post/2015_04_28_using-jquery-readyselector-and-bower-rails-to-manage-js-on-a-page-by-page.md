+++
date = "2015-04-28T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "jquery-readyselectorとbower-railsを使って、ページ単位でJsを管理する"
slug = "using-jquery-readyselector-and-bower-rails-to-manage-js-on-a-page-by-page"
+++

ページごとに読み込むJsを制限したいなあと思って、ググってみたところいろいろ出てくる。
ただ、今回はできるかぎりシンプルにやりたかったので`jquery-readyselector`というJqueryプラグインを使うことにした。

このプラグインはBowerで管理されている(Node)モジュールみたいなので、まずは`bower-rails`をインストールして、
`bower-rails`経由で`jquery-readyselector`をインストールすることにした。

*bower-railsを使うには、事前にBowerをインストールしておく必要がある*

## bower-railsのインストール

まずはGemfileの設定し、インストール。
```
$ cat Gemfile
gem "bower-rails"

$ bundle install
```
<p>

次に、Nodeモジュールを管理するための設定ファイル(Gemfileみたいなもの)の形式を決める。
形式はjson形式の`bower.json`の場合か、
```
$ rails g bower_rails:initialize json
      create  bower.json
      create  config/initializers/bower_rails.rb
```

DSL形式の`Bowerfile`のどちらか選べる。今回はこちらの方法を選択。
```
$  rails g bower_rails:initialize
      create  Bowerfile
      create  config/initializers/bower_rails.rb
```
<p>
`bower-rails`の設定は`config/initializers/bower_rails.rb`で行う。
今回は特に設定なし(デフォルトのまま)。

```
BowerRails.configure do |bower_rails|
   # Tell bower-rails what path should be considered as root. Defaults to Dir.pwd
   # bower_rails.root_path = Dir.pwd

   # Invokes rake bower:install before precompilation. Defaults to false
   # bower_rails.install_before_precompile = true

   # Invokes rake bower:resolve before precompilation. Defaults to false
   # bower_rails.resolve_before_precompile = true

   # Invokes rake bower:clean before precompilation. Defaults to false
   # bower_rails.clean_before_precompile = true

   # Invokes rake bower:install:deployment instead rake bower:install. Defaults to false
   # bower_rails.use_bower_install_deployment = true
end
```
<br>

## jquery-readyselectorのインストール

まずはインストールするNodeモジュールを設定し、インストールの実行。

```
$ cat Bowerfile
asset 'jquery-readyselector'

$ bundle exec rake bower:install
```
<p>

すると`vendor/assets`配下に`bower_components`というディレクトリが生成され、
その中にNodeモジュールがインストールされる。
```
$ ll vendor/assets/bower_components/
query-readyselector/
...
```
<p>

`bower-rails`でインストールしたNodeモジュールは、`bower-rails`側で管理したかったので、gitignore した。
```
$ cat .gitignore
vendor/assets/bower_components/*
```
<p>

インストールしたNodeモジュールのpathを設定する。
```
$ cat config/application.rb
config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components')
```
<p>

次に`jquery-readyselector`を`application.js`に読み込む。
```
$ cat app/assets/javascripts/application.js
//= require jquery-readyselector
//= require_tree .
```
<p>

最後にbodyタグに、アクセスされた時の`コントローラ名`と`アクション名`をclassに設定するようにする。
```
$ emacs app/views/layouts/application.html.erb
<body class="<%= controller_name -%> <%= action_name -%>">
```
<br>

## ページ別にJsが読み込まれているか確認

まずは適当なページ`http://localhost:3000/books`を用意する。
```
$ rails g scaffold Book title:string price:integer
...
app/assets/javascripts/books.coffee
...

$ rake db:migrate
```
<p>

次に生成されたJs(coffee)に、ページ単位(action単位)にコードを書く。
`http://localhost:3000/books`にアクセスした時のみ、`alert`がでれば成功。
```
$ emacs app/assets/javascripts/books.coffee
$('.books.index').ready ->
  alert('book#index')
```
<br>

### 参考サイト

* [Railsで外部JavaScriptライブラリなどのAssetを管理するなら、bower-railsが便利](http://blog.mah-lab.com/2014/04/14/bower-rails/)
* [bower-rails](https://github.com/rharriso/bower-rails)
* [bodyにclassを付けて特定のページでのみjsが動作するようにする](http://blog.hello-world.jp.net/javascript/1673/)
