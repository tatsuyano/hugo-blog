+++
date = "2015-03-25T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "ruby"]
title = "emacsにrcodetoolsをインストールして、返り値をコメントに出力する"
slug = "install-rcodetools-to-emacs"
+++

返り値をコメントに出力したい場合、rcodetoolsのxmpfilterを使うことで可能になります。

<pre><code class='language-ruby'>3 + 4
↓
3 + 4 # => 7
</code></pre>
<br>

## インストール

まずはrcodetools本体をインストールします。

<pre><code class='language-bash'>$ gem install rcodetools
</code></pre>
<br>

次にインストールしたgemの中にある`rcodetools.el`をemacsのload-pathが通っている階層にコピーします。

<pre><code class='language-bash'>$ cp -a $HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/gems/2.2.0/gems/rcodetools-0.8.5.0/rcodetools.el $HOME/src/github.com/tatsuyano/dot.emacs.d/init_loader/
</code></pre>
<br>

## emacsの設定

今回は、`M-p`にxmpfilterを割り当てます。

<pre><code class='language-ruby'>(require 'rcodetools)
(define-key ruby-mode-map (kbd "M-p") 'xmp)
</code></pre>
<br>

## 操作方法

1. まずは返り値を出力したい行で、`M-;(comment-dwim)`を2回実行すると「# =>」というコメントがソースに出力されます。
2. その状態で、`M-p(xmp)`を実行すると、コメントに返り値が出力されます。

ちなみに`M-;`はAltを押しながら`;`を押下です。

<pre><code class='language-ruby'>3 + 4
↓
3 + 4 # =>          1. comment-dwimを2回実行
↓
3 + 4 # => 7        2. xmpfilterを実行
</code></pre>
<br>

※ _環境によっては、`gems/rcodetools/bin/`をPATHに加える必要があるようなので、動かない場合は試してみてください。_

## 参考サイト

* [EmacsでRubyの開発環境をさらにめちゃガチャパワーアップしたまとめ](http://futurismo.biz/archives/2213)
* [Emacs で rcodetools を使って "=>" で値を表示する](http://qiita.com/ironsand/items/ce7c02eb46fcc25a438b)
