+++
date = "2014-10-21T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["fluentd"]
title = "elasticsearch&kibanaをインストール"
slug = "how-to-install-and-setup-elasticsearch-and-kibana"
+++

[fluentdをインストールする](/2014/10/20/how-to-install-and-setup-fluentd/)の続きです。

インストールする環境は以下になります。

* AmazonLinux
* nginx
* ruby 2.1.0 (anyenv)
* fluentd 0.10.55
* java 1.7.0_71

### elasticsearchのインストール

elasticsearch には java が必要なので、事前にインストールしてください。

<pre><code class="language-bash">$ cd /opt
$ wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.3.4.tar.gz
$ tar xvzf elasticsearch-1.3.4.tar.gz
$ mv elasticsearch-1.3.4 elasticsearch
$ rm -rf elasticsearch-1.3.4.tar.gz
</code></pre>
<br>

#### セットアップ

elasticsearch/config/elasticsearch.yml

<pre><code class="language-bash">index.number_of_replicas: 0 # レプリカセットの数
index.number_of_shards: 1   # シャードの数

path.data: /opt/elasticsearch/data     # index(データ)の保存先
path.logs: /var/log/elasticsearch/logs # ログの保存先
</code></pre>

`elasticsearch.yml`に設定したディレクトリを作成

<pre><code class="language-bash">$ mkdir /opt/elasticsearch/data
$ sudo bash
# mkdir -p /var/log/elasticsearch/logs
# /var/log
# chmod 777 -R elasticsearch/logs
</code></pre>
<br>

#### 起動方法

バッググラウンドで動かす場合は`-d`をつけます。

<pre><code class="language-bash">$ /opt/elasticsearch/bin/elasticsearch

[2014-10-20 09:23:31,007][INFO ][node                     ] [Blue Bullet] version[1.3.4], pid[1729], build[a70f3cc/2014-09-30T09:07:17Z]
[2014-10-20 09:23:31,008][INFO ][node                     ] [Blue Bullet] initializing ...
[2014-10-20 09:23:31,012][INFO ][plugins                  ] [Blue Bullet] loaded [], sites []
[2014-10-20 09:23:33,921][INFO ][node                     ] [Blue Bullet] initialized
[2014-10-20 09:23:33,922][INFO ][node                     ] [Blue Bullet] starting ...
[2014-10-20 09:23:33,995][INFO ][transport                ] [Blue Bullet] bound_address {inet[/0:0:0:0:0:0:0:0:9300]}, publish_address {inet[/172.31.22.71:9300]}
[2014-10-20 09:23:34,011][INFO ][discovery                ] [Blue Bullet] elasticsearch/ti8R_MimTTOQX2PRMWWMGA
[2014-10-20 09:23:37,078][INFO ][cluster.service          ] [Blue Bullet] new_master [Blue Bullet][ti8R_MimTTOQX2PRMWWMGA][ghost.localdomain][inet[/172.31.22.71:9300]], reason: zen-disco-join (elected_as_master)
[2014-10-20 09:23:37,102][INFO ][http                     ] [Blue Bullet] bound_address {inet[/0:0:0:0:0:0:0:0:9200]}, publish_address {inet[/172.31.22.71:9200]}
[2014-10-20 09:23:37,102][INFO ][node                     ] [Blue Bullet] started
[2014-10-20 09:23:37,114][INFO ][gateway                  ] [Blue Bullet] recovered [0] indices into cluster_state
</code></pre>
<br>

9200ポートに接続できれば成功です。

<pre><code class="language-bash">$ curl localhost:9200
{
  "status" : 200,
  "name" : "Blue Bullet",
  "version" : {
    "number" : "1.3.4",
    "build_hash" : "a70f3ccb52200f8f2c87e9c370c6597448eb3e45",
    "build_timestamp" : "2014-09-30T09:07:17Z",
    "build_snapshot" : false,
    "lucene_version" : "4.9"
  },
  "tagline" : "You Know, for Search"
}
</code></pre>
<br>

##### Nginxの設定

elasticsearch の出力結果を、kibana に渡すためには、外部から参照できるよう
設定しないといけないので、サブドメインを用意しました。

*本当は同じドメインで、port番号を変えることで、サービスを切り替えたかったのですが、Nginxの設定がうまくできませんでした。。。*

/etc/nginx/sites-available/fluentd.10rane.com.conf

<pre><code class="language-bash"># elasticsearch
server {
    listen 80;
    server_name es.10rane.com;

    location / {
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   Host      $http_host;
        proxy_pass         http://127.0.0.1:9200;
    }
}
</code></pre>
<br>

### Kibanaをインストールする

<pre><code class="language-bash">$ cd /opt
$ wget https://download.elasticsearch.org/kibana/kibana/kibana-3.1.1.tar.gz
$ tar xvzf kibana-3.1.1.tar.gz
$ rm kibana-3.1.1.tar.gz
$ mv kibana-3.1.1 kibana
</code></pre>
<br>

#### セットアップ

kibina/config.js

<pre><code class="language-bash">//elasticsearch: "http://"+window.location.hostname+":9200",
elasticsearch: "http://es.10rane.com",
</code></pre>
<br>

#### Nginxの設定

こちらも新たにサブドメインを用意しました。

/etc/nginx/sites-available/fluentd.10rane.com.conf

<pre><code class="language-bash"># kibana
server {
    listen 80;
    server_name kibana.10rane.com;

    location / {
        root /opt/kibana/;
        index index.html index.htm;
    }
}
</code></pre>
<br>

Nginx 再起動後、設定したURLにアクセスし、下記の画面が出れば成功です。

*設定を変更しても、ブラウザのキャッシュが更新されていない場合があるので、設定変更後は、キャッシュをクリアしてみてください。*

![](https://dl.dropboxusercontent.com/u/159938/blog_images/elasticsearch-and-kibana_01.jpg)

### 参考サイト

* [fluentd + Elasticsearch + Kibanaで始めるログ解析 (セットアップ編)](http://blog.excale.net/index.php/archives/1929)
* [elasticsearch 1.3.4](http://www.elasticsearch.org/download/)
* [kibana 3.1.1 installation](http://www.elasticsearch.org/overview/kibana/installation/)

### 関連記事

* [fluentdをインストールする](http://blog.10rane.com/2014/10/20/how-to-install-and-setup-fluentd/)
