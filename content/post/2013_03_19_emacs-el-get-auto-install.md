+++
date = "2013-03-19T05:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "el-get.elでauto-install.elをインストールする"
slug = "emacs-el-get-auto-install"
+++

まずは [el-get.el のインストール](/2013/03/19/emacs-el-get/) が完了していることが前提になります。

#### インストール
まずは M-x el-get-list-packages で利用できるパッケージを一覧表示します。  
次に インストールしたいパッケージに「i」を押してチェックをいれ、「x」を押すと自動でインストールが始まります。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/emacs-install-auto-install_001.png)

インストールしたパッケージは、 (setq el-get-dir "~/.emacs.d/elisp/el-get/") で指定したディレクトリ直下にディレクトリ単位にインストールされます。  
  
またインストールしたパッケージを削除する場合は、M-x el-get-remove で削除します。  

#### auto-installでインストールしたパッケージの置場所
el-get.el でインストールしたパッケージと、auto-install.el でインストールしたパッケージを分けて管理したいので、  
auto-install用のディレクトリとパスの設定をします。
  
.emacs.d  
└── elisp  
　　├── auto-install ... auto-installでインストールしたパッケージの置場所  
　　└── el-get ... el-getでインストールしたパッケージの置場所  
　　　　　├── auto-install ... auto-install.elの本体。本体はel-getでインストールしたため  
　　　　　└── el-get ... el-get.el自身もまた、el-getに管理されている  

#### auto-installの設定
init.el  
``` scheme
(setq load-path
  (append
  (list
  ...
  (expand-file-name "~/.emacs.d/elisp/auto-install/") ;; <- パスの追加
  )
  load-path))
  ...
  (require 'auto-install)
  (setq auto-install-directory "~/.emacs.d/elisp/auto-install/") ;; auto-installでインストールしたパッケージの置場所を指定
```
