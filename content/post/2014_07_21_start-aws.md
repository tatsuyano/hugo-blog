+++
date = "2014-07-21T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["aws"]
title = "aws はじめました。"
slug = "start-aws"
+++

現在AWSの無料キャンペーンをやっているので、試してみました。

## AWSのアカウント作成

こちらAmazonのサイトに詳細な説明があるので、省略。

* [無料アカウント作成の流れ](http://aws.amazon.com/jp/register-flow/)

## インスタンスの作成
まずはEC2のコンソール画面を開きます。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_0.png" />

### REGIONの選択
レスポンスが気になるので、REGIONは「Tokyo」に。真ん中あたりの「Launch Instance」で作成画面に遷移します。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_1.png" />

### Image(AMI)の選択
「Free tier only」が無料Image(AMI)なので、「Amazon Linux」を選択。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_2.png" />

### Instanceの選択
こちらも無料の「Micro instances」を選択。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_3.png" />

### InstanceにTagをつける
とりあえず Name というkeyに、「amz_tokyo」というvalueをつけました。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_4.png" />

### firewallの設定
allow(許可)の条件を設定する模様。固定IPがあるので、SSHにIPを指定します。
とりあえずHTTPとping(Custom ICMP Rule)も設定しました。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_5.png" />

### 秘密鍵の作成、及びダウンロード
最後に秘密鍵を作成しダウンロードします。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_6.png" />

DLした秘密鍵のpermissionを600にしてください。
```
$ chmod 600 .ssh/amz_tokyo.pem
```

## DNS経由で、ssh接続

ユーザは「ec2-user」、接続先IPは、public DNSのURLを指定します。
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_9.png" />

```
$ ssh -i .ssh/amz_tokyo.pem ec2-user@ec2-xxxx-xxxx-.ap-northeast-1.compute.amazonaws.com
```

       __|  __|_  )
       _|  (     /   Amazon Linux AMI
      ___|\___|___|

## DNSから固定IP(Elastic Ips)に変更する

### Elastic Ips(EIP)の料金について
インスタンスが「起動」しているIPに対してEIPを割り当てても無料だが、
「停止」しているインスタンスに対して、EIPを割り当てたまま放置しておくと1hに$0.01課金されるので注意。

### Instanceを指定し、固定IPと紐付ける
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_7.png" />
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_8.png" />

### firewallの再設定
固定IPに変更したので、先ほど設定したfirewallの設定が再度行う。

* [EC2でpingを許可する](http://www.happyquality.com/2013/06/21/2675.htm)

### pingでレスポンスの測定
「Tokyo」以外のREGIONと比較しないと意味がないが、計測方法をメモ。
下記のコマンドは 64byteのリクエストを10回投げて、レスポンスが帰ってくるまで計測結果。

```
$ ping -c 10 54.178.xxx.xxx
PING 54.178.xxx.xxx (54.178.xxx.xxx): 56 data bytes
64 bytes from 54.178.xxx.xxx: icmp_seq=0 ttl=56 time=14.303 ms
...
--- 54.178.xxx.xxx ping statistics ---
10 packets transmitted, 10 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 14.269/15.842/19.001/1.569 ms  -> avg = 15.842ms
```

## コマンドラインツールのインストール
まだ試していないが、とりあえずインストールだけしてみた。

* [awsのコマンドラインツールをインストール](http://halucolor.blogspot.jp/2014/02/macawsaws.html)
* [AWS コマンドラインインターフェイス](http://aws.amazon.com/jp/cli/)
* [AWS Command Line Interface(awscli)を使ってみた](http://d.hatena.ne.jp/rx7/20130813/p1)

#### 関連する記事
* [VagrantとVirtualBoxのインストール](http://blog.10rane.com/2014/07/21/install-vagrant-and-virtualbox/)
* [vagrant-aws-pluginをインストール](http://blog.10rane.com/2014/07/21/install-vagrant-aws-plugin/)
* [Route53のDNSを使う](http://blog.10rane.com/2014/09/11/using-the-dns-of-route53/)
* [Route53にドメインを移管する](http://blog.10rane.com/2014/09/16/transferred-a-domain-to-route53/)
