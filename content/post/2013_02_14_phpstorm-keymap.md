+++
date = "2013-02-14T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "PhpStormのkeymapを変更し、C-hにdelete-backward-charを割り当てる"
slug = "phpstorm-keymap"
+++

C-h(delete-backward-char)のことを「hungry backspace」というらしい。  

```
Preference > Keymap > Main menu > Navigate > Type Hierarchy > Remove ^H
Preference > Keymap > hungry backspace > Add Keyboard Shortcut ^H
```

ついでにCtrl+i に設定されていた「Implement Mehtods」のショートカットを削除  

```
Preference > Keymap > Main menu > Code > Implement Methods > Remove ^I
```
