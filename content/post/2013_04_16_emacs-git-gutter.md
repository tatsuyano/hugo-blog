+++
date = "2013-04-16T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "git"]
title = "git-gutter.elをインストールする"
slug = "emacs-git-gutter"
+++

gitの差分を表示してくれる git-gutter を入れてみました。  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/git_gutter_001.png)

## インストール

はじめREADME.mdにかいてあるとおり、M-x package-install git-gutter で  

>>
Installation
You can install git-gutter.el from MELPA with package.el (M-x package-install git-gutter).
And you can also install it with el-get.

ELPA経由インストールしてみたが、versionが0.28と少し古くエラーがでたので、el-getのrecipeを用意してインストールした。  

``` scheme
(:name emacs-git-gutter
  :type github
  :website "https://github.com/syohex/emacs-git-gutter"
  :description "[My Recipes] git-gutter.el is port of GitGutter which is a plugin of Sublime Text."
  :pkgname "syohex/emacs-git-gutter")
```

## 設定

とりあえず差分の表示だけがほしいので、最小限の設定に。  
  
.emacs.d/conf/81-git-gutter.el

``` scheme
(require 'git-gutter)
;;(global-git-gutter-mode t)

(setq git-gutter:separator-sign "|")
(set-face-foreground 'git-gutter:separator "yellow")

(add-hook 'cperl-mode-hook 'git-gutter-mode)
(add-hook 'js2-mode-hook   'git-gutter-mode)
(add-hook 'php-mode-hook   'git-gutter-mode)
(add-hook 'ruby-mode-hook  'git-gutter-mode)
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [github](https://github.com/syohex/emacs-git-gutter/blob/master/README.md)
* [git-gutter.el](http://d.hatena.ne.jp/naoya/20130414/1365967372)
