+++
date = "2013-03-19T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "el-get.elでauto-complete.elをインストールする"
slug = "emacs-el-get-auto-complete"
+++

## el-getにインストールされているパッケージリストが古かったので、レシピを更新
パッケージリストには、auto-complete.elがあるのですが、レシピが古くインストールしてもディレクトリの中は空なので、レシピを新たに設定します。  

また、auto-complete.el は popup.el と fuzzy.el というelispが必須なのですが、今回 popup.el のレシピを用意し、auto-complete とは別にインストールする必要があります。  
※ とりあえず fuzzy.el は別途インストールしなくても動いているので、fuzzy.el は保留。  

## レシピの追加

10-el-get.el
``` scheme
(setq el-get-dir "~/.emacs.d/elisp/el-get/")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
    (url-retrieve-synchronously
      "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(setq el-get-sources
  '(
    (:name init-loader
      :type http
      :url "https://raw.github.com/gist/1021706/init-loader.el"
      :description "[My Recipes] Split management init.el. ")
    (:name popup ;; <- 追加
      :type github
      :website "https://github.com/auto-complete/popup-el"
      :description "[My Recipes] This section describes the basic data structures and operations of popups."
      :pkgname "auto-complete/popup-el")
    (:name auto-complete ;; <- 追加
      :type github
      :website "https://github.com/auto-complete/auto-complete"
      :description "[My Recipes] Auto Complete Mode renews an old completion interface and provides an environment that makes users could be more concentrate on their own works."
      :pkgname "auto-complete/auto-complete")
    ))
(el-get 'sync)
```

## インストール
これで、auto-complete.el と popup.el のレシピが更新されたので、M-x el-get-list-packages　でインストールします。  

## 設定
12-auto-complete.el
``` scheme
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/elisp/el-get/auto-complete/dict")
(ac-config-default)
(setq ac-use-menu-map t)
```
