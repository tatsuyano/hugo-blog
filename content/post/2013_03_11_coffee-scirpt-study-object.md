+++
date = "2013-03-11T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["coffeescript"]
title = "CoffeeScriptの勉強 その３(オブジェクト)"
slug = "coffee-scirpt-study-object"
+++

#### Objectの生成

this.x の代わりに @x と書くことができる

<pre><code class="language-coffeescript">pos =
  x:100
  y:200
  dump: -> console.log "x:#{@x},y:#{@y}"

pos.dump()
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">var pos;
pos = {
  x: 100,
  y: 200,
  dump: function() {
    return console.log("x:" + this.x + ", y:" + this.y);
  }
};
</code></pre>

一行で書く場合、「,」で区切る

<pre><code class="language-coffeescript">size = width:100, height:200
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">var size;
size = {
  width: 100,
  height: 100
};
</code></pre>
<br>

#### 配列

for .. in でループが書けます。

<pre><code class="language-coffeescript">seq = ["a","b","c"]
for x in seq
  console.log x

 a
 b
 c
</code></pre>

ループ中に配列のインデックスを拾いたい場合はforとinの間に2つ変数を置きます。

<pre><code class="language-coffeescript">seq = ["a","b","c"]
for x,i in seq
  console.log "#{i} -> #{x}"
{% endcodeblock %}
0 -> a
1 -> b
2 -> c
</code></pre>
<br>

#### 連想配列

for .. of を使って配列と同じように反復処理ができます。

<pre><code class="language-coffeescript">data =
  x:100
  y:200

for key,value of data
  console.log "#{key} -> #{value}"

x -> 100
y -> 200
</code></pre>
<br>

#### 存在チェック

変数名や関数名の直後に ? を付けると「その変数あるいは関数が定義されておりnull以外の値が入っているかどうか」をテストできます。

<pre><code class="language-coffeescript">if myName?
  console.log "yes"
else
  console.log "no"
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">if (typeof myName != "undefined" && myName !== null) {
  console.log("yes");
} else {
  console.log("no");
}
</code></pre>

また「.」や 「()」の前に ? を置くことで、変数や関数が存在する場合のみ処理を進めるようにできます。

<pre><code class="language-coffeescript">console?.log?("Hello World")
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">if (typeof console != "undefined" && console !== null) {
  if (typeof console.log == "function") {
    console.log("Hello World");
  }
}
</code></pre>
<br>

#### ヒアドキュメント

<pre><code class="language-coffeescript">html = '''
aaa
bbb
ccc
'''
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">var html;
html = 'aaa\nbbb\nccc';
</code></pre>
<br>

#### thisのバインド

関数定義時に -> の代わりに => を使うと、thisがバインドされて外側のthisを参照するようになります。

<pre><code class="language-coffeescript">pos =
  x:100
  y:200
  dump: ->
    # 関数内部で@x(this.x)を使いたいのでfuncの定義は=>にしないといけない
    func = => console.log "x:#{@x}, y:#{@y}"
        func()
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">var pos;

pos = {
  x: 100,
  y: 200,
  dump: function() {
    var func,
    _this = this;
    func = function() {
      return console.log("x:" + _this.x + ", y:" + _this.y);
    };
    return func();
  }
};
</code></pre>
