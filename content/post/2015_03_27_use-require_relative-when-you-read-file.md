+++
date = "2015-03-27T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby"]
title = "ファイルを読み込む時は require_relative を使う"
slug = "use-require_relative-when-you-read-file"
+++

こちら[[requireとrequire_relativeの違いとは？](http://blog.livedoor.jp/sasata299/archives/52111648.html)]
に詳しく書いてあるのですが、

ファイルを読み込む時は`require`ではなく、`require_relative`を使いましょう(Ruby1.9.2以降で、かつ`$LOAD_PATH`に含まれていない場合)。

Ruby1.9.2以降、`$LOAD_PATH`にカレントディレクトリが含まれなくなったため、
例えば、同じ階層の`file.rb`を読み込もうと思って`require 'file'`と書いたらエラーになります。

<pre><code class="language-ruby">$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/2.2.0/rubygems/core_ext/kernel_require.rb:54:in
`require': cannot load such file -- file (LoadError)
</code></pre>

その回避方法として、`require ./file`　と相対パスで書くことも可能ですが、
違う階層から読み込むと結局エラーになってしまいます。

どうしても`require`と使いたい場合は、`require` する前に、
カレントディレクトリを`$LOAD_PATH`に追加する方法があります。

<pre><code class="language-ruby">$:.unshift File.dirname(__FILE__)  # ロードパスにカレントディレクトリを追加
require 'file'
</code></pre>

因みに`$:`は`$LOAD_PATH`の別の書き方です。

* `$LOAD_PATH` ... プログラム実行時に読み込まれるパス
* `$:`         ... $LOAD_PATHの別の書き方

## 参考サイト
* [requireとrequire_relativeの違いとは？](http://blog.livedoor.jp/sasata299/archives/52111648.html)
* [Rubyのrequireとrequire_relative](http://d.hatena.ne.jp/torazuka/20150115/require)
