+++
date = "2012-08-22T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "markdown-mode.elをインストールする"
slug = "emacs-markdown"
+++

## インストール

本来、直接.emacs.d/elisp/以下でgit cloneして  
.emacs.d/elisp/markdown-mode/ というディレクトリ構成にしたかったのですが、  
あとで行う、文字色の設定が(ファイルの読み込み順序の関係で)うまくいかなかったので、  
このようなディレクトリ構成になりました。  

```
git clone git://jblevins.org/git/markdown-mode.git
mv markdown.el webpage.sh .emacs.d/elisp/
rm -rf markdown-mode
```

``` scheme
emacs .emacs.d/conf/80_markdown.el
;;(add-to-list 'load-path "~/.emacs.d/elisp/markdown-mode") ;; 今回elisp直下にファイルを置いたのでコメントアウト
(autoload 'markdown-mode "markdown-mode.el" "Major mode for editing Markdown files" t)
(setq auto-mode-alist (cons '("\\.md" . markdown-mode) auto-mode-alist))
```

## 文字(見出し等)に色を付ける

デフォルトのままだと見出し等に色がついてなく、見にくいので  
[こちらのサイト](http://d.hatena.ne.jp/CortYuming/20120602/p1)を参考に色をつけてみました。  

``` scheme
cat >> .emacs.d/conf/80_markdown.el
;; custom color
(defface markdown-header-face-1
  '((((class color) (background light))
       (:foreground "DeepPink1" :underline "DeepPink1" :weight bold))
           (((class color) (background dark))
                (:foreground "DeepPink1" :underline "DeepPink1" :weight bold)))
                  "Face for level-1 headers.")

(defface markdown-header-face-2
  '((((class color) (background light))
       (:foreground "magenta1" :underline "magenta1" :weight bold))
           (((class color) (background dark))
                (:foreground "magenta1" :underline "magenta1" :weight bold)))
                  "Face for level-2 headers.")

(defface markdown-header-face-3
  '((((class color) (background light))
       (:foreground "purple1" :underline "purple1" :weight bold))
           (((class color) (background dark))
                (:foreground "purple1" :underline "purple1" :weight bold)))
                  "Face for level-3 headers.")

(defface markdown-header-face-4
  '((((class color) (background light))
       (:foreground "DeepPink4" :underline "DeepPink4" :weight bold))
           (((class color) (background dark))
                (:foreground "DeepPink4" :underline "DeepPink4" :weight bold)))
                  "Face for level-4 headers.")

(defface markdown-header-face-5
  '((((class color) (background light))
       (:foreground "magenta4" :underline "magenta4" :weight bold))
           (((class color) (background dark))
                (:foreground "magenta4" :underline "magenta4" :weight bold)))
                  "Face for level-5 headers.")

(defface markdown-header-face-6
  '((((class color) (background light))
       (:foreground "purple4" :underline "purple4" :weight bold))
           (((class color) (background dark))
                (:foreground "purple4" :underline "purple4" :weight bold)))
                  "Face for level-6 headers.")
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

* [Emacsのmarkdown-modeを使ってみる](http://blog.s-amemiya.com/development/emacs%E3%81%AEmarkdown-mode%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%A6%E3%81%BF%E3%82%8B/)
* [markdown-mode のヘッダーが全て同じ色で見づらいから色付けて分かりやすくしてみた](http://d.hatena.ne.jp/CortYuming/20120602/p1)
