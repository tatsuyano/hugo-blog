+++
date = "2015-03-31T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "Rails4でBootstrap3を導入(sass版)"
slug = "introduced-bootstrap3-to-rails4-sass"
+++

以前、`twitter-bootstrap-rails` をつかって[Bootstrap3を導入](/2015/02/18/introduced-bootstrap3-to-rails4/)という記事を書いたんですが、
Twitter社が提供している`bootstrap-sass`のほうが導入が簡単だったので、今後はこちらを使っていきたいと思います。

## 設定

まずはGemfileの設定から。

<pre><code class="language-ruby">cat Gemfile
gem 'bootstrap-sass', '~> 3.3.4'
gem 'sass-rails', '>= 3.2'

$ bundle install
</code></pre>
<br>

`application.css` を削除し、新たに `application.sass` を生成します。

<pre><code class="language-bash">$ rm app/assets/stylesheets/application.css
$ cat > app/assets/stylesheets/application.scss
@import "bootstrap-sprockets";
@import "bootstrap";
</code></pre>
<br>

`application.js` の設定を変更します。読み込み順に注意してください。

<pre><code class="language-bash">$ cat > app/assets/javascripts/application.js
//= require jquery
//= require bootstrap-sprockets
</code></pre>
<br>

これでGlyphicons(Webフォント)も使えるようになります。便利ですね。

## 参考サイト

* [twbs/bootstrap-sass](https://github.com/twbs/bootstrap-sass)
* [全部はいらないよね？bootstrap-sassをカスタマイズして使う方法](http://qiita.com/masamitsu-konya/items/e3630046774ac1fbd346)


## 関連する記事

* [Rails4でBootstrap3を導入](/2015/02/18/introduced-bootstrap3-to-rails4/)
