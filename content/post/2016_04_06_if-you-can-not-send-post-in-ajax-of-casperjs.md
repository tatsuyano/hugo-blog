+++
date = "2016-04-06T12:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["js","nodejs","casperjs"]
title = "CasperJSのajaxでPOSTの送信ができない"
slug = "if-you-can-not-send-post-in-ajax-of-casperjs"
+++

地味にハマった。Chromeだとパラメータに`type:"POST"`がなくてもPOSTでリクエストを送信してくれるが、
CasperJsのブラウザでは、`type:"POST"`を明示しないと`GET`でリクエストを送信してしまう。

```javascript
$.ajax({
    url:  "/shopping/addpoint/",
    type: "POST", // <-このパラメータが必須
    data: data,
    success: function(data, dataType) {
        var data_arr = JSON.parse(data);
        $('#point').text(data_arr['point']);
        alert(data_arr['msg']);
    }
});
```

手動で操作するぶんには問題なく動いていたので(typeパラメータが設定されていなくても)、
CasperJs(自分で書いたソース)側に問題があると思い込んでしまった。

思い込みで判断せずに、まずはアクセスログを確認しようという教訓でした。
