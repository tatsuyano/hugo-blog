+++
date = "2014-07-29T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "middleware"]
title = "memcachedのインストール"
slug = "install-memcached"
+++

まずはyumでmemcachedのインストール

    # yum install -y memcached memcached-devel

libmemcachedのインストール

* [最新のソース](https://launchpad.net/libmemcached/+download)

```
# cd /usr/local/src
# wget https://launchpad.net/libmemcached/1.0/1.0.18/+download/libmemcached-1.0.18.tar.gz
# tar xvzf libmemcached-1.0.18.tar.gz
# ./configure
# make
# make install
```

/etc/sysconfig/memcached

    PORT="11211"
    USER="apache"
    MAXCONN="1024"
    CACHESIZE="64"
    OPTIONS=""

### PHPで使うために以下の設定

    # yum install -y php-pecl-memcached

PHP Extensionのインストール
```
# git clone https://github.com/php-memcached-dev/php-memcached.git
# cd php-memcached
# phpize
# ./configure --with-libmemcached-dir=/usr/local
# make
# make install
```

PHPで設定が有効になっているか確認

```
$ php -i | grep memcached

memcached
memcached support => enabled
libmemcached version => 1.0.18
memcached.compression_factor => 1.3 => 1.3
memcached.compression_threshold => 2000 => 2000
memcached.compression_type => fastlz => fastlz
memcached.serializer => php => php
memcached.sess_binary => 0 => 0
memcached.sess_connect_timeout => 1000 => 1000
memcached.sess_consistent_hash => 0 => 0
memcached.sess_lock_expire => 0 => 0
memcached.sess_lock_max_wait => 0 => 0
memcached.sess_lock_wait => 150000 => 150000
memcached.sess_locking => 1 => 1
memcached.sess_number_of_replicas => 0 => 0
memcached.sess_prefix => memc.sess.key. => memc.sess.key.
memcached.sess_randomize_replica_read => 0 => 0
memcached.sess_remove_failed => 0 => 0
memcached.sess_sasl_password => no value => no value
memcached.sess_sasl_username => no value => no value
memcached.store_retry_count => 2 => 2
memcached.use_sasl => 0 => 0
Registered save handlers => files user memcached
PWD => /tmp/php-memcached
_SERVER["PWD"] => /tmp/php-memcached
```

起動

    # /etc/init.d/memcached start
    # /etc/init.d/httpd restart

    ps auxwww|grep mem
    apache   24255  0.0  0.0 330844   988 ?        Ssl  18:04   0:00 memcached -d -p 11211 -u apache -m 64 -c 1024 -P /var/run/memcached/memcached.pid

 memcachedに保存されている情報の確認

    $ memcached-tool localhost display
    $ memcached-tool localhost dump | strings

* [memcached のインストール。作業メモ](http://d.hatena.ne.jp/modified/20130107/1357553481)
* [FuelPHPのキャッシュライブラリ – memcached編](http://9ensan.com/blog/programming/php/fuelphp-cache-memcached/)
* [PHPのMemcachedモジュールをインストール](http://takashi-kun.hatenablog.com/entry/2013/11/23/031343)
* [memcached の中身を確認するなら memcached-tool コマンド](http://c-brains.jp/blog/wsg/10/07/30-105035.php)
* [Memcached の key 一覧を取得するまでの過程](http://colopl.co.jp/tech/blog/2012/06/1877/)
* [PHPでmemcachedを使うときのモジュールパフォーマンス比較](http://d.hatena.ne.jp/masayuki14/touch/20120710/1341877792)
* [memcachedコマンド一覧](http://l-w-i.net/t/memcached/command_001.txt)

お疲れ様でした。

#### 関連する記事
* [deamontoolsのインストール](http://blog.10rane.com/2014/07/29/install-deamontools/)
