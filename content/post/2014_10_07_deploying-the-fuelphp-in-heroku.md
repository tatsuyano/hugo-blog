+++
date = "2014-10-07T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "heroku"]
title = "herokuでfuelphpをデプロイ"
slug = "deploying-the-fuelphp-in-heroku"
+++

まずはherokuの準備をしてください。

* [herokuことはじめ](http://blog.10rane.com/2014/10/07/start-heroku/)

herokuにデプロイするアプリを、github上に用意してください。

* [fuelphpアプリのgit管理](http://blog.10rane.com/2014/10/06/git-management-of-fuelphp/)

#### herokuにアプリを登録

今回は sample-of-chat という名前のアプリを登録します。
アプリ名はわかりやすいように、リポジトリと同じ名前がいいと思います。
また、アプリ名がURLになるので、注意してください。

`http://sample-of-chat.herokuapp.com`

<pre><code class="language-bash">$ cd sample-of-chat
$ heroku create sample-of-chat

git remote -v
heroku  git@heroku.com:sample-of-chat.git (fetch)
heroku  git@heroku.com:sample-of-chat.git (push)
</code></pre>

webサーバーと`DocumentRoot`を指定するファイル`Procfile`を作成します。
webサーバーは`apache`か`nginx`から選択します。

<pre><code class="language-bash">$ cd sample-of-chat
$ cat > Procfile
web: vendor/bin/heroku-php-apache2 public/
</code></pre>

* [Setting the document root](https://devcenter.heroku.com/articles/custom-php-settings#setting-the-document-root)

次に、`.gitignore`から`composer.lock`をコメントアウトし、  
`composer.json`に設定(`bin-dir`)を追加します。

<pre><code class="language-bash">"config": {
    "bin-dir": "vendor/bin",
    "vendor-dir": "fuel/vendor"
},
</code></pre>

#### herokuにデプロイ

herokuに`push`することで、デプロイが行われます。

<pre><code class="language-bash">git push heroku master
</code></pre>

これでデプロイ完了です。デプロイが完了すると、

`http://sample-of-chat.herokuapp.com`

にアクセスが可能になります。

またデプロイされたアプリは、一定時間アクセスがないと、自動で`sleep`状態になります。

#### 関連する記事

* [herokuことはじめ](http://blog.10rane.com/2014/10/07/start-heroku/)
* [fuelphpアプリのgit管理](http://blog.10rane.com/2014/10/06/git-management-of-fuelphp/)

#### 参考にしたサイト

* [Herokuの使い方まとめ(2013年12月時点、Rails使用)](http://exfreeter.hatenablog.com/entry/2013/12/09/235902)
* [HerokuがPHPに正式対応したのでFuelPHP動かしてみた](http://spicy-space.hatenablog.com/entry/2014/06/22/013708)
* [Setting the document root](https://devcenter.heroku.com/articles/custom-php-settings#setting-the-document-root)
