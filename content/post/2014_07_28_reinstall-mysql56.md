+++
date = "2014-07-28T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["mysql"]
title = "mysqlを5.1->5.6に再インストールする"
slug = "reinstall-mysql56"
+++

CentOS6の標準のyumでMysqlをインストールすると、バージョンが5.1だったので、再度yumで5.6を入れなおす。

### mysql5.6の性能
* [MySQL 5.6での機能強化点 - パフォーマンスと使い勝手を大きく向上](http://thinkit.co.jp/story/2013/11/19/4670)
* [開発スピードアクセル全開ぶっちぎり！日本よ、これがMySQL 5.6だッ！！](http://nippondanji.blogspot.jp/2012/10/mysql-56.html)
* [MySQL 5.1->5.6のmy.cnfの差分とか](http://d.hatena.ne.jp/hirose31/20121003/134924992)

### yumでインストールしたmysqlのパッケージの確認

    $ rpm -qa | grep mysql
    mysql-libs-5.1.73-3.el6_5.x86_64
    mysql-server-5.1.73-3.el6_5.x86_64
    php-mysqlnd-5.5.10-1.el6.remi.1.x86_64 # <- これはphp5.5のパッケージ
    mysql-5.1.73-3.el6_5.x86_64

### 依存関係の解消
ただmysql-libsはcronやpostfixが使用しているため、先に依存関係を解消する必要がある

    $ wget ftp://ftp.jaist.ac.jp/pub/mysql//Downloads/MySQL-5.6/MySQL-shared-compat-5.6.17-1.el6.x86_64.rpm

    $ rpm -Uvh MySQL-shared-compat-5.6.17-1.el6.x86_64.rpm
    準備中...                ########################################### [100%]
    1:MySQL-shared-compat    ########################################### [100%]
    警告: /etc/my.cnf は /etc/my.cnf.rpmsave として保存されました。

MySQL-shared-compatは、mysql-libsと同等のライブラリを提供し、mysql-libsを置き換えてくれる

### パッケージの削除

    yum remove mysql*
    
削除されたパッケージ

    ==========================
    Package
    =========================
    Removing:
      mysql
      mysql-community-release
      mysql-server

    Transaction Summary
    ==========================
    Remove        3 Package(s)

* [CentOS6.5 64bitにMySQL5.6.17をRPMからインストール](http://www.kakiro-web.com/linux/mysql-install.html)

### mysql5.6用のrpmを追加し、再度インストールする

    yum install http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm
    yum install mysql mysql-devel mysql-server

インストールされたパッケージ

    $ rpm -qa | grep mysql
    mysql-community-server-5.6.19-2.el6.x86_64
    mysql-community-release-el6-5.noarch
    php-mysqlnd-5.5.10-1.el6.remi.1.x86_64
    mysql-community-common-5.6.19-2.el6.x86_64
    mysql-community-client-5.6.19-2.el6.x86_64
    mysql-community-devel-5.6.19-2.el6.x86_64
    mysql-community-libs-5.6.19-2.el6.x86_64

* [CentOS6にMySQL5.6をyumで簡単にインストールする手順](http://blog.ybbo.net/2014/01/22/%E3%80%90%E3%82%B7%E3%83%B3%E3%83%97%E3%83%AB%E3%80%91centos6%E3%81%ABmysql5-6%E3%82%92yum%E3%81\%A7%E7%B0%A1%E5%8D%98%E3%81%AB%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB%E3%81%99%E3%82%8B/)

### create table でSQLエラーが出る

Mysql5.6から、インストール時に強制的にsql_modeを設定するになった関係で、今まで動いていたSQLがエラーになる

    BLOB/TEXT column 'hoge' can't have a default value
    Field 'updated_at' doesn't have a default value


この`STRICT_TRANS_TABLES`が有効になっていると、text型やdatetime型に初期値を設定しているとエラーになる模様

    # mysql -uroot -e 'SELECT @@GLOBAL.sql_mode;'
    +--------------------------------------------+
    | @@GLOBAL.sql_mode                          |
    +--------------------------------------------+
    | STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION |
    +--------------------------------------------+

### sql_modeの設定を変える
/etc/my.cnf

    # Recommended in standard MySQL setup
    sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES

この部分をコメントアウトし、mysqlを再起動すると、sql_modeが変更された

    # mysql -uroot -e 'SELECT @@GLOBAL.sql_mode;'
    +------------------------+
    | @@GLOBAL.sql_mode      |
    +------------------------+
    | NO_ENGINE_SUBSTITUTION |
    +------------------------+

コメントアウトしたのに、なぜ`NO_ENGINE_SUBSTITUTION`が消えないのか不思議だ(別の場所にもmy.cnfがあるのだろう)が、
とりあえずこれでエラーがでなくなる。

ちなみに、sql_modeの設定をナシにしたい場合は、下記のように設定する

    # Recommended in standard MySQL setup
    sql_mode=''

* [MySQL5.6で今までのVerでは問題無かったSQL文がエラーになった場合の対処法](http://d.hatena.ne.jp/oranie/20130402/1364906656)
* [CentOS 6.4にMySQL 5.5をrpmインストール](http://d.hatena.ne.jp/ozuma/20130323/1364002513)
* [MySQL5.6にしていくつかのSQLでエラーが出るようになった](http://www.seeds-std.co.jp/seedsblog/1035.html)
