+++
date = "2014-07-21T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["vagrant", "aws"]
title = "vagrant-aws-pluginをインストール"
slug = "install-vagrant-aws-plugin"
+++

vagrant-awsプラグインを使うことで、コマンドラインで
インスタンスの作成、起動ができるようになります。

### vagrant-aws(plugin)のインストール
```
$ vagrant plugin install vagrant-aws
Installing the 'vagrant-aws' plugin. This can take a few minutes...
Installed the plugin 'vagrant-aws (0.4.1)'!
```

### インストールされているpluginの確認
```
$ vagrant plugin list
vagrant-aws (0.4.1)
vagrant-login (1.0.1, system)
vagrant-share (1.0.1, system)
```

### BOXのインストール
```
$ vagrant box add dummy https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
==> box: Adding box 'dummy' (v0) for provider:
    box: Downloading: https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box
    ==> box: Successfully added box 'dummy' (v0) for 'aws'!
$ vagrant box list
chef/centos-6.5 (virtualbox, 1.0.0)
dummy           (aws, 0)
```

### aws用のディレクトリを作成 & 初期化
```
$ mkdir -p Vagrant/Aws
$ cd Vagrant/Aws
$ vagrant init dummy
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

### Access_Key_IDとSecret_Access_Keyの設定
自分のアカウントのIDとKeyを設定するか、
もしくはIAMでユーザを作成し、そのユーザのIDとKeyを設定する

#### 自分のアカウントのIDとKey
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_12.png" />

#### IAMのIDとKey
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_10.png" />
<img width="70%" src="https://dl.dropboxusercontent.com/u/159938/blog_images/aws_ec2_11.png" />

### Vagrantfileの設定
```
VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "dummy"
  config.vm.provider :aws do |aws, override|
    aws.access_key_id = 'XXXX'
    aws.secret_access_key = 'XXXX'
    aws.region = "ap-northeast-1" # Tokyo
    aws.ami = 'ami-c9562fc8' # Amazon Linux AMI 2014.03.1
    aws.instance_type = 't1.micro'
    aws.security_groups = ['XXXX'] # 複数設定可
    aws.keypair_name = 'XXXX' # 事前に作成した秘密鍵
    aws.tags = {
      'Name' => 'XXXX'
    }
    override.ssh.username = "ec2-user"
    override.ssh.private_key_path = 'DLした秘密鍵のフルパス'
  end
end
```


### インスタンスの起動
```
$ vagrant up --provider=aws
```

### インスタンスへの接続
```
$ vagrant ssh
```

### 参考サイト
* [Vagrantで AWS EC2インスタンス（RHEL5.5）を作成する](http://qiita.com/Humangas/items/2997cc6701aa90687c04)
* [vagrant-awsを使用する際のVagrantファイル詳細](http://qiita.com/yuma_iwasaki/items/817d54cf075a67778a0a)
* [IAM とは?](http://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/IAM_Introduction.html)
* [EC2インスタンス作成＆最初にしたこと](http://qiita.com/tuboc/items/38da97cb96b2ebc69f88#1-5)

#### 関連する記事
* [VagrantとVirtualBoxのインストール](http://blog.10rane.com/2014/07/21/install-vagrant-and-virtualbox/)
* [aws はじめました。](http://blog.10rane.com/2014/07/21/start-aws/)
