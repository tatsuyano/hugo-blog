+++
date = "2015-06-02T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["nodejs", "es6", "js"]
title = "Promiseを使って、非同期処理を任意のタイミングで実行する"
slug = "use-the-promise-to-perform-the-async-processing-at-anytime"
+++

Promiseを使って、非同期処理を任意の順番で実行するサンプル。
ES5でも順番を指定して実行する事ができるが、Promiseを使うと綺麗に実装できる。

<script src="http://gist-it.appspot.com/https://github.com/tatsuyano/study-es6promise/blob/master/sample01-nodejs.js">
</script>
<br>

### 参考サイト

* [JavaScript Promiseの本](http://azu.github.io/promises-book/)
* [JavaScript Promises](http://www.html5rocks.com/ja/tutorials/es6/promises/)
* [es6-promise](https://github.com/jakearchibald/es6-promise)
* [Babel repl](http://babeljs.io/repl/)

### 関連する記事

* [webpackを使ってes6-promiseを読み込む](/2015/06/03/use-webpack-to-read-the-es6-promise/)
