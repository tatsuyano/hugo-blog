+++
date = "2014-07-29T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["middleware"]
title = "daemontoolsのインストール"
slug = "install-daemontools"
+++

ダウンロード

    # mkdir -p /package
    # chmod 1755 /package
    # cd /package

    # wget http://cr.yp.to/daemontools/daemontools-0.76.tar.gz
    # tar zxvf daemontools-0.76.tar.gz


インストールスクリプトの修正

/package/admin/daemontools-0.76/src/conf-cc

```
gcc -O2 -Wimplicit -Wunused -Wcomment -Wchar-subscripts -Wuninitialized -Wshadow -Wcast-qual -Wcast-align -Wwrite-strings #<- 削除
gcc -O2 --include /usr/include/errno.h #<-書き換え
```

インストール

    # cd /admin/daemontools-0.76
    # /package/install

daemontools本体の起動設定
/etc/init/svscan.conf

    start on runlevel [12345]
    respawn
    exec /usr/local/bin/svscanboot
    
* [CentOS6.4にmemcachedを入れた。](http://itochin2.hatenablog.com/entry/2014/01/26/151224)
* [memcachedの運用と互換アプリケーション](http://gihyo.jp/dev/feature/01/memcached/0005?page=1)
* [私家版 daemontools FAQ) 3. トラブルシューティング](http://www.unixuser.org/~euske/doc/daemontools/myfaq/faq-3.html)    
    
#### daemontoolsのコマンド

ややこしいのですが、実際のコマンドは、` /package/admin/daemontools/command`にあります。

`/usr/local/bin` => `/command` => `/package.../command`  
の順番にシンボリックリンクが貼ってあります。

### 監視用スクリプトの作成

memcachedを監視するためのスクリプトを作成します。

    # chmod +x /var/memcached/run
    # ln -s /var/memcached /service/memcached
    # /etc/init.d/memcached stop

/var/memcached/run

```
#!/bin/sh

if [ -f /etc/sysconfig/memcached ];then
        . /etc/sysconfig/memcached
fi

exec 2>&1
exec /usr/bin/memcached -p $PORT -u $USER  -m $CACHESIZE -c $MAXCONN
$OPTIONS
```

daemontoolsの起動

    sh -c 'svscan /service &'

daemontoolsを起動すると、監視対象のmemcachedが起動されます。

    # ps aux|grep [m]em
    root     10011  0.0  0.0   4048   348 ?        S    07:52   0:00 supervise memcached
    apache   10012  0.0  0.1 316772  1104 ?        Sl   07:52   0:00 /usr/bin/memcached -p 11211 -u apache -m 64 -c 1024

### daemontoolsのコマンド

監視対象の起動、停止

    # svc -d /service/memcached   #=> down
    # svc -u /service/memcached   #=> up
    # svc -t /service/memcached   #=> restart


状態の確認

    svstat /service/memcached
     /service/memcached: down 216 seconds, normally up
    
お疲れ様でした。
    
#### 関連する記事
* [memcachedのインストール](http://blog.10rane.com/2014/07/29/install-memcached/)
