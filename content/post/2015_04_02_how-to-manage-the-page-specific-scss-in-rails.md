+++
date = "2015-04-02T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "Railsでページごとのscssを管理する方法"
slug = "how-to-manage-the-page-specific-scss-in-rails"
+++

GitHubにある[README.md](https://github.com/twbs/bootstrap-sass/blob/master/README.md)を参考に、[bootstrap-sassをインストール](/2015/03/31/introduced-bootstrap3-to-rails4-sass/)してみました。

### application.scssの設定

すると`application.scss`にimportするファイルは、`bootstrap`と`bootstrap-sprockets`の2ファイルのみとなっています。

<pre><code class="language-css">// 'bootstrap-sprockets' must be imported before "bootstrap" and "bootstrap/variables"
@import "bootstrap-sprockets";
@import "bootstrap";"
</code></pre>

そのため、このままですと、ページごとに用意したscssファイルを読み込むためには、
毎回`application.scss`にimportする必要がでてきてしまいます。

### デフォルトのapplication.css

通常、設定なしで `rails new` を行うと、`application.css` には、

<pre><code class="language-css">$ cat app/assets/stylesheets/application.css
/*
 *= require_tree . # 指定されたディレクトリ以下のすべてを再帰的にインクルード(順番の保証なし)
 *= require_self   # require_self呼び出しが行われたその場所にCSSファイルがあれば読み込みます
 */
</code></pre>

というディレクティブが設定され、stylesheetsディレクトリ以下にcssを置くと、
自動で読み込まれる(順番は保証されない)ようになります。
### RailsでSassファイルをどう管理するべきか

いろいろ方法は有ると思うのですが、今回は[こちらの記事](http://qiita.com/natsu871/items/53b38a2543df97df751d)を参考に
ディレクトリ構成を作ってみました。

まず、読み込み順を守る必要のあるファイル(`bootstrap`と`bootstrap-sprockets`)は、そのまま`application.scss`に。  
ページ個々のscssに関しては、`app/assets/stylesheets/partials`ディレクトリを生成し、そちらに配置しておきます。  
`partials`ディレクトリでは、読み込み順が保証されないので、注意してください。
<pre><code class="language-css"># app/assets/stylesheets/application.scss
@import "bootstrap-sprockets";
@import "bootstrap";
@import "partials/*"; // glob importing
</code></pre>

※ `sass-rails`のバージョンが古いと、`glob importing` には対応していないので、動かない場合はバージョンを上げてみて下さい。

### partialsディレクトリにファイルがないとエラーになる

ただこのままの状態だと、partialsディレクトリにファイルがないので、`File to import not found or unreadable`エラーが出てしまいます。
それを防ぐために、とりあえずブランクのscssファイルをディレクトリに配置するようにしました。

<pre><code class="language-bash">$ touch app/assets/stylesheets/partials/blank.scss
</code></pre>

最後のブランクのscssを用意するのはかなり微妙ですが、
ディレクトリに別のscssが用意された時点で、blank.scssを削除するなど対応する形で当面運用しようと思います。

### application.jsの設定

最低限必要なのは、下記のJsになります。

<pre><code class="language-javascript">//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
</code></pre>
<br>

## 参考サイト

* [RailsGuide::アセットパイプライン](http://railsguides.jp/asset_pipeline.html)
* [twbs/bootstrap-sass](https://github.com/twbs/bootstrap-sass)
* [Is it possible to import a whole directory in sass using
@import?](http://stackoverflow.com/questions/4778627/is-it-possible-to-import-a-whole-directory-in-sass-using-import)
* [Rails での適切な Sass の構成手法について（翻訳）](http://qiita.com/natsu871/items/53b38a2543df97df751d)
