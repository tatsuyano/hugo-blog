+++
date = "2016-04-11T14:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["linux","windows"]
title = "netcatを使って、ポートの状況を確認する"
slug = "check-the-status-of-the-port-using-the-netcat"
+++

ローカルマシン(今回はWindows)から、ホストのLinux(ローカルマシン内のVagrant)に、
メッセージを送ってポートの確認を行う方法


### netcatのインストール

まずはyumでLinuxにインストール
 
``` shell
sudo yum -y install nc.x86_64
```

次に下記URLからWindowsにインストール

* [windows版のnetcat](https://eternallybored.org/misc/netcat/netcat-win32-1.12.zip)


### WindowsからLinuxの指定のポートにメッセージを送る

Windowsのコンソールを立ち上げ、Linux(192.168,33.10)の(例えば)8082ポートにecho
これで8082ポートが開いている確認できる

``` shell
C:\Program Files (x86)\netcat> echo 'hello' | nc 192.168.33.10 8082
```

次に Linux側で8082ポートをListen。すると期待どおりメッセージが表示される

``` shell
$ nc -l 8082
'hello'
```

### ホスト側の現在開いているポートを確認する

ホスト側でポート状況を確認するには、以下のパラーメータをつける

``` shell
$ netstat -ant
```

#### オプション
* -t TCPポートを表示する
* -u UDPポートを表示する
* -a すべての有効なポートを表示する
* -n 一切の名前解決を行なわない

<!--
### telnetをつかったポートの状況確認

telnetをつかっても確認ができる。下記のコマンドは8082ポート

http://togattti.hateblo.jp/entry/2014/01/19/124912

```
$ telnet localhost 8082
Trying ::1...
telnet: connect to address ::1: Connection refused
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
```
8082がLISTENできる場合は、`Connected to xxx` と表示される


* [telnetとnetstatでポート番号の状態を確認する](http://togattti.hateblo.jp/entry/2014/01/19/124912)
* [TELNETプロトコルでリモートマシンに接続 - Linuxコマンド](http://webkaru.net/linux/telnet-command/)

-->

### 参考サイト

* [Netcat でネットワークをもう少し活用する](http://www.usupi.org/sysad/190.html)
* [netstatコマンドを使いこなす](http://www.atmarkit.co.jp/ait/articles/0207/20/news003.html)
* [netstat（1）――TCP通信の接続状況を調べる](http://itpro.nikkeibp.co.jp/article/COLUMN/20100308/345506/)
