+++
date = "2014-07-29T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "fuelphpでAgentClassを設定する"
slug = "fuelphp-use-agent-class"
+++

ブラウザの識別やモバイルフォンとPCの識別をするには、fuelではAgentクラスを使用する。

AgentクラスはPHPのget_browser関数を利用する。

get_browser関数は、UA情報を[Browser Capabilities Project](http://tempdownloads.browserscap.com/)というサイトから取得する。
fuelではこのサイトからUA情報を取得(キャッシュ化し2回目のアクセスからキャッシュを参照)し、識別する。

#### configファイルにUA情報(サイト)の取得先URLなどを設定

fuel/app/config/config.php
```
'browscap' => array(
    'enabled' => true,
    'url'     => 'http://browsers.garykeith.com/stream.asp?BrowsCapINI',
    'method' => 'wrapper',
    'file'   => '/tmp/php_browscap.ini',
),
```

#### キャッシュの保存先について
fuelでは fuel/app/cache にキャシュを保存している

```
cache
└── fuel
    └── agent
            ├── browscap.cache
            └── browscap_file.cache
```

キャッシュ周りで権限エラーがでたので、権限を`777`にする

    chmod -R 777 fuel/app/cache/

#### Agentクラスの実装

    Agent::is_mobiledevice();

### 参考サイト
* [FuelPHPのAgentクラスと拡張](http://btt.hatenablog.com/entry/2012/07/04/001254)
* [Agent Class](http://fuelphp.com/docs/classes/agent/config.html)
* [FuelPHPのAgentクラスによる端末振り分けが簡単すぎる](http://fuelphp.seesaa.net/article/279094100.html)

### 関連する記事
* [fuelphpのエラー文言を日本語化する](http://blog.10rane.com/2014/07/29/fuelphp-validation/)
* [fuelphpでredisを使う](http://blog.10rane.com/2014/07/28/use-the-redis-in-fuelphp/)
