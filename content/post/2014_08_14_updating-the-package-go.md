+++
date = "2014-08-14T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["peco", "go"]
title = "goパッケージ(peco)のバージョンを上げる"
slug = "updating-the-package-go"
+++

`--prompt` オプションを使いたかったので、バージョンをv0.1->v0.2.4にあげました。

`GOROOT`と`GOPATH`の設定が必要なので、[環境変数](http://blog.10rane.com/2014/08/14/take-over-the-environment/)が引き継いだ状態でroot昇格します。

    $ sudo -E bash

    # printenv |grep GO
    GOROOT=/usr/local/go
    GOPATH=/usr/local/mygo

`go get -u`でアップデートされます。

    # go get -u github.com/peco/peco/cmd/peco

バージョンが上がりました。

    # peco --verision
    peco: v0.2.4

* [How does Go update third-party packages?](http://stackoverflow.com/questions/10383498/how-does-go-update-third-party-packages)

#### 関連する記事
* [流行りのpecoをcentosにインストールしました](http://blog.10rane.com/2014/07/02/installed-the-peco-to-centos/)
