+++
date = "2014-11-29T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["misc"]
title = "Octocat Figurineを衝動買いした"
slug = "i-bought-octocat-figurine"
+++

![](https://dl.dropboxusercontent.com/u/159938/blog_images/I_bought_octocat_figurine_01.jpg)

なんとなく[Octocat Figurine](https://github.myshopify.com/products/5-inch-octocat-figurine)を衝動買いした。

購入方法はpaypalで、配送期間は9日間と思っていたより短かった。
料金は配送手数料込みで$102.19、円安の影響も受け、最終的に12,216円。ちと高い。

配送方法は2パターンあって、安いほうを使ったみたが、特に問題はなかった。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/I_bought_octocat_figurine_02.jpg)

サイズが5インチということでいがいと存在がある。ひげと台座が取り外し可能。  
けっこうよく出来てると思う。
