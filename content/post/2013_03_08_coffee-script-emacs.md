+++
date = "2013-03-08T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["coffeescript"]
title = "CoffeeScriptの勉強 その１(インストール、環境構築)"
slug = "coffee-script-emacs"
+++

#### npm(node.js)でのインストール

<pre><code class="language-bash"># node.jsのversionを指定
nvm ls
v0.8.14 v0.9.4
current:        v0.9.4

nvm use 0.9.4
Now using node v0.9.4

npm install -g coffee-script

$ coffee -v
CoffeeScript version 1.6.1
</code></pre>

今後頻繁にCoffeeScriptを使いそうなので、zshrcにnode.jsのバージョンを指定しておく

$HOME/.zshrc
<pre><code class="language-bash">[[ -s $HOME/.nvm/nvm.sh ]] && nvm use 0.9.4

source $HOME/.zshrc
</code></pre>

#### emacsにcoffee-modeをインストール

<pre><code class="language-bash">cd $HOME/.emacs.d/elisp
git clone git://github.com/defunkt/coffee-mode.git
</code></pre>

$HOME/.emacs.d/conf/52_coffee.el
<pre><code class="language-bash">(add-to-list 'load-path "~/.emacs.d/elisp/coffee-mode")
(require 'coffee-mode)

;; デフォルトだとインデントが Space 8 なので、Space 2 に変更
;; http://qiita.com/items/a8d96ae95a1838500e37
(defun coffee-custom ()
  "coffee-mode-hook"
   (set (make-local-variable 'tab-width) 2)
    (setq coffee-tab-width 2))

(add-hook 'coffee-mode-hook
  '(lambda() (coffee-custom)))
</code></pre>

#### インデントに色をつける

CoofeeScriptはrubyのようにインデントごとにブロックを表現するので、  
emacsに「Highlight-Indentation-for-Emacs」をインストールする。

<pre><code class="language-bash">cd $HOME/.emacs.d/elisp
git clone https://github.com/antonj/Highlight-Indentation-for-Emacs.git
</code></pre>

「Highlight-Indentation-for-Emacs」をインストールすると、  
カーソルを合わせているブロック(インデント)に色を付けてくれる「highlight-indentation-current-column-mode」。  
ブロック(インデント)ごとに色を付けてくれる「highlight-indentation-mode」などが使えるようになる。  
  
ただ現在「highlight-indentation-mode」をcoffee-modeで初めから使えるようにhookするとerrorは出ないが、色付けをしてくれない。  
手動で M-x highlight-indentation-mode と起動させる分には色付けされる。  
とりあえず「highlight-indentation-current-column-mode」しか使わないので、こちらだけhookする。  

$HOME/.emacs.d/conf/61_highlight.el
<pre><code class="language-bash">(add-to-list 'load-path "~/.emacs.d/elisp/Highlight-Indentation-for-Emacs")
(require 'highlight-indentation)(setq highlight-indentation-offset 2) ;;default:4
(set-face-background 'highlight-indentation-current-column-face "#ff0000") ;;default:#000000
(add-hook 'coffee-mode-hook 'highlight-indentation-current-column-mode) ;; coffee-mode-hook
</code></pre>

#### 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

* [本家チュートリアル](http://coffeescript.org/)
* [「CoffeeScript」の開発環境を用意して、サンプルを表示してみよう](http://codezine.jp/article/detail/6392)
* [インデントをハイライトしてくれる「Highlighting indentation」で脱・インデント迷子](http://blog.iss.ms/2012/03/17/095152)
