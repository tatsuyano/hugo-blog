+++
date = "2015-02-19T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "heroku", "rails"]
title = "Rails4をherokuにdeployする"
slug = "deploy-the-rails4-to-heroku"
+++

備忘のため、Rails4をheroku上にdeployする方法をメモしておきます。

*herokuのアカウントの作成、及び鍵の設定は終っているものとします。*

<pre><code class='language-ruby'>$ ruby  -v 2.2.0p0
$ rails -v 4.2.0
</code></pre>
<br>

## サンプルアプリの作成

まずはherokuにdeployするアプリを生成

<pre><code class='language-ruby'>$ rails new heroku-rails
$ cd heroku-rails
</code></pre>
<br>

herokuではsqlite3はサポートされていないので、productionからは外すようにする。今回productionで使うDBは、heroku標準のpostgressを選択。
また、Rail4から`rails_12factor`が必須になったらしいので設定。

Gemfile
<pre><code class='language-ruby'>group :development do
  gem 'sqlite3'
end
group :production do
  gem 'pg'
  gem 'rails_12factor'
end
</code></pre>
<br>

開発環境では、postgressを使っていないので、`--without production`をつけて
productionでのみ必要なGemは開発環境には入れないようにする。

<pre><code class='language-ruby'>$ bundle install --without production
</code></pre>
<br>

開発環境でも`pg`を使う場合は、以下のようにpostgress本体と、develをインストールする。

<pre><code class='language-ruby'>$ sudo yum install -y postgresql-devel
$ sudo yum install -y postgresql
$ gem install pg
</code></pre>
<br>

ここまでで空のアプリができたので、いったん`git commit`。

<pre><code class='language-ruby'>$ git init
$ git add .
$ git commit -m 'initial commit'
</code></pre>
<br>

次にherokuにログインし、`heroku create`してheroku上にアプリを生成する。
この時、アプリ名を指定しなければ、適当な名前で生成される(後から変更可)。
*アプリ名はURLに含まれるため、heroku全体でユニークである必要がある*

<pre><code class='language-ruby'>$ heroku login
$ heroku create <アプリ名>
$ git push heroku master  # この段階でheroku上にdeployされる。
</code></pre>
<br>

ちなみに`git commit`する前に`heroku create`すると、herokuのremote-urlが設定されないので、
その場合は、自分で設定する必要がある。

<pre><code class='language-ruby'>$ git remote set-url heroku  https://git.heroku.com/<アプリ名>.git
</code></pre>
<br>

アプリの情報は、`heroku app:info`で確認ができる。

<pre><code class='language-ruby'>$ heroku apps:info
=== fast-stream-xxxx
Addons:        heroku-postgresql:hobby-dev
Dynos:         1
Git URL:       https://git.heroku.com/fast-stream-xxxx.git
Owner Email:   xxxxxx@xxxx.com
Region:        us
Repo Size:     24k
Slug Size:     27M
Stack:         cedar-14
Web URL:       https://fast-stream-xxxx.herokuapp.com/
Workers:       0
</code></pre>
<br>

しかし、作成したアプリのURLをたたくと、以下のようなエラーが出る。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploy-the-rails4-to-heroku_01.png)

heroku上のアプリでは、いつものRailsのTOP画面は出てこない。

ここまでの状態で、heroku上で動いているのを確認したい場合は、
仮のTOPページを生成してgit commitし、再度`heroku push origin master`してdeployすると確認できる。

<pre><code class='language-ruby'>$ cat >> public/index.html
index.html
</code></pre>
<br>

## herokuでDBを使う　

`scaffold`で、DBにアクセスする機能を実装してみる。
開発環境で、`rake db:migrate`した場合は、herokuでも`migrate`する必要がある。

<pre><code class='language-ruby'>$ rails g scaffold Book title:string price:integer
$ rake db:migrate
$ git add .
$ git commit -m 'rails g scaffold Book title:string price:integer'

$ git push heroku master
$ heroku run rake db:migrate　# <= This!
</code></pre>

![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploy-the-rails4-to-heroku_02.png)

## TwitterBootStrapのGlyphiconを使う

BootStrapのGlyphiconを使ったアプリをherokuにdeployしても、そのままでは使えない。
使うためには、production環境(heroku)でも、assets.compileを有効にする必要がある。

config/environments/production.rb
<pre><code class='language-ruby'>#config.assets.compile = false
 config.assets.compile = true 
</code></pre>
<br>

## 参考サイト

* [Heroku に Rails アプリをアップ](http://railsgirls.jp/heroku/)
* [Railsで簡単なアプリをscaffoldで作ってHerokuにデプロイするまで](http://qiita.com/tstomoki/items/cd2391e028dc44dd6f40)
* [HerokuでRailsアプリを公開しようとして躓いた所](http://blog.machacks.net/2014/01/21/heroku%E3%81%A7rails%E3%82%A2%E3%83%97%E3%83%AA%E3%82%92%E5%85%AC%E9%96%8B%E3%81%97%E3%82%88%E3%81%86%E3%81%A8%E3%81%97%E3%81%A6%E8%BA%93%E3%81%84%E3%81%9F%E6%89%80/)
* [Rails4でBootstrap3を導入](/2015/02/18/introduced-bootstrap3-to-rails4/)
* [Glyphicon works locally but not on Heroku](http://stackoverflow.com/questions/20588786/glyphicon-works-locally-but-not-on-heroku)
