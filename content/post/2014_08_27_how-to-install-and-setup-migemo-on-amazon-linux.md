+++
date = "2014-08-27T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "Amazon LinuxにMigemoをインストールする"
slug = "how-to-install-and-setup-migemo-on-amazon-linux"
+++

migemoを使うと、日本語の文章をローマ字で検索できるようになります。

### 作業の流れ

1. `RPM`で`nkf`をインストール
2. `ghq`で`cmigemo`のインストール
3. `cask`で`migemo.el`をインストール
4. `init.el`に`migemo`の設定を行う

### RPMでnkfをインストール

migemoをインストールするには`nkf`が必要です。`nkf`がないと下記のようなエラーが出ます。
[ERROR: Require qkc or nkf installed for encode filter.](https://github.com/koron/cmigemo/blob/master/configure#L49)

ただAmazon Linuxのyumには、nkfパッケージがないので、RPMでインストールします。

    $ sudo bash
    # cd /tmp/
    # wget http://mirror.centos.org/centos/6/os/x86_64/Packages/nkf-2.0.8b-6.2.el6.x86_64.rpm
    # rpm -ivh nkf-2.0.8b-6.2.el6.x86_64.rpm

    # which nkf
    /usr/bin/nkf

### ghqでcmigemoのインストール

`migemo`は元々rubyで実装されていたらしいのですが、今はC言語で実装された`cmigemo`をインストールします。

    $ ghq get https://github.com/koron/cmigemo.git
    $ cd $HOME/src/github.com/koron/cmigemo
    $ ./configure
    $ make gcc
    $ make gcc-dict
    $ sudo make gcc-install

    $ which cmigemo
    /usr/local/bin/cmigemo

#### cmigemoが正常にインストールされているか確認

    $ cmigemo -d /usr/local/share/migemo/utf-8/migemo-dict

適当なローマ字(toukyoなど)を入力すると正規表現を生成される -> 正常にインストールされている

    migemo_open("/usr/local/share/migemo/utf-8/migemo-dict")=0x21d3010
    clock()=0.090000
    QUERY: toukyo
    PATTERN: (ﾄｳｷｮ|トウキョ|等距離|登極|当(局|協会)|東[教京]|とうきょ|ｔｏｕｋｙｏ|toukyo)


### caskでmigemo.elをインストール

~/emacs.d/Cask

    (depends-on "migemo")

`cask`コマンドでインストール

    $ cd ~/.emacs.d
    $ cask
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/gnu/archive-contents
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/marmalade/archive-contents
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/archives/melpa/archive-contents
    Making version-control local to migemo-autoloads.el while let-bound!
    Wrote $HOME/.emacs.d/.cask/24.3.1/elpa/migemo-20140823.2003/migemo-autoloads.el

### init.elにmigemoの設定を行う

~/.emacs.d/init.el

    (require 'migemo)
    (setq migemo-command "/usr/local/bin/cmigemo")
    (setq migemo-options '("-q" "--emacs"))
    (setq migemo-dictionary "/usr/local/share/migemo/utf-8/migemo-dict")
    (setq migemo-user-dictionary nil)
    (setq migemo-coding-system 'utf-8-unix)
    (setq migemo-regex-dictionary nil)
    (load-library "migemo")
    (migemo-init)

これで`C-s`でミニバッファに `[MIGEMO] I-search` と表示され日本語インクリメンタルサーチが可能になります。すごい！

### 参考サイト

* [Amazon EC2（Amazon Linux AMI) にnkf をインストールする手順](http://tkuchiki.hatenablog.com/entry/2012/12/01/004833)
* [c/migemoをインストールしてvimから使う方法](http://deris.hatenablog.jp/entry/20120812/1344731592)
* [migemo を emacs 24.3.1 にインストールしたときのメモ, Mac OS X](http://weblog.ymt2.net/blog/html/2013/08/23/install_migemo_to_emacs_24_3_1.html)

### 関連する記事

* [Caskをインストールする](http://blog.10rane.com/2014/08/20/install-cask/)
