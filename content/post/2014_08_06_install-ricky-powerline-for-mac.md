+++
date = "2014-08-06T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["zsh", "mac"]
title = "Ricky for powerlineをmacにインストールする"
slug = "install-ricky-powerline-for-mac"
+++

agnosterは`powerline.otf`にある依存文字を使うことを前提しているので、
macにpowerline系のフォントをインストールします。

少しややこしいのですが、元々powerlineというフォントはvimで使う用？だったので
ググるとよくvim-powerlineとかがヒットしますが同じもののようです。

また、既存のフォントをpowerline化(既存のフォントに依存文字を使えるようにする)することもできます。

今回は、今使っているRictyをそのまま使いたかったので、
新たに`Ricty for powerline`というフォントをbrewでインストールしてみました。

    brew update
    brew uninstall ricty
    brew tap sanemat/font
    brew install --vim-powerline ricty
    cp -f /usr/local/Cellar/ricty/3.2.3/share/fonts/Ricty*.ttf ~/Library/Fonts/

* [homebrewを使ってiTerm2でRicty for powerline設定した](http://qiita.com/osakanafish/items/731dc31168e3330dbcd0)

インストールが完了したら、iTermのフォントの設定を変更して、iTermを再起動してください。

お疲れ様でした。

#### 関連する記事
* [oh-my-zshをインストールする](http://blog.10rane.com/2014/08/06/install-oh-my-zsh/)
