+++
date = "2014-08-20T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "Caskをインストールする"
slug = "install-cask"
+++

やろうやろうと思ってやっていなかったCaskを導入しようと思います。
今ままでは主に`el-get`を中心に使っていたのですが、
どのパッケージをインストールしたかとか、環境の切り替えが少し面倒でした。

今回導入する`Cask`はrubyのBundlerチックに管理してくれるため、上記の問題を解決してくれます。

*Caskのインストールにはpythonが必要です。*
*インストールされていない場合、[anyenv](http://blog.10rane.com/2014/08/19/install-anyenv/)でpythonをインストールしてください*

### Emacs 24のインストール

`Cask`は Emacs 24以上が必須です。
今回うっかりyum経由でEmacsをインストールしていたため、バージョンが23.1.1.だったので
いったん削除し、再度バージョン24以上のEmacsをインストールします。

yumでインストールしたEmacs 23を削除

    # yum list installed | grep emacs
    # yum -y remove emacs.x86_64 emacs-common.x86_64

make & install

    # yum install gcc make ncurses-devel
    # wget http://ftp.jaist.ac.jp/pub/GNU/emacs/emacs-24.3.tar.gz

    # tar zxvfp emacs-24.3.tar.gz
    # cd emacs-24.3
    # ./configure -without-x -without-selinux
    # make
    # make install

バージョンの確認

    $ emacs --version
    GNU Emacs 24.3.1
    Copyright (C) 2013 Free Software Foundation, Inc.

* [CentOS6.5にEmacs24.3をインストール](http://www.beard-bear.com/blog/archives/123)

### Caskのインストール

    $ curl -fsSkL https://raw.github.com/cask/cask/master/go | python
    Successfully installed Cask!  Now, add the cask binary to your $PATH:
      export PATH="$HOME/.cask/bin:$PATH"

~/.zshrc

    echo >> export PATH="$HOME/.cask/bin:$PATH" >> ~/.zshrc
    source $HOME/.zshrc

インストールするパッケージのリストは`Cask`という名前のファイルに設定します。`Cask`ファイルを作るには、`cask init`で生成されます。
パッケージのインストール先は`.cask`ディレクトリです。

    $ cd ~/.emacs.d/
    $ cask init
    
ディレクトリ構成

    ~/.emacs.d
     ├── ac-comphist.dat
     ├── auto-save-list
     ├── .cask #<= パッケージがインストールされるディレクトリ
     ├── Cask
     └── init.el

~/.emacs.d/Cask

    (source gnu)
    (source marmalade)
    (source melpa)

    (depends-on "markdown-mode")

    ;; Ruby
    (depends-on "ruby-block")
    (depends-on "ruby-compilation")
    (depends-on "ruby-end")
    (depends-on "ruby-interpolation")
    (depends-on "ruby-mode")
    (depends-on "ruby-test-mode")
    (depends-on "ruby-tools")
    (depends-on "inf-ruby")

~/.emacs.d/init.el

    ;; Emacs package system
    (require 'cask "~/.cask/cask.el")
    (cask-initialize)

パッケージのインストール

    $ cd ~/.emacs.d
    $ cask

Caskファイルの精査はまた今度行おうと思います。
とりあえずCaskをインストールまでをまとめました。

お疲れ様でした。

* [Cask](http://d.hatena.ne.jp/naoya/20140424/1398318293)
* [package.elから Caskに切り替えました](http://d.hatena.ne.jp/syohex/20140424/1398310931)
* [Emacs のパッケージ管理を package.el + el-get から Cask + pallet に乗り換えました](http://gongo.hatenablog.com/entry/2014/05/09/230836)
* [Cask Usage](http://cask.github.io/usage.html)
