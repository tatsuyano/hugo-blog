+++
date = "2014-08-19T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["zsh"]
title = "zshをバージョン5 に上げる"
slug = "install-zsh5"
+++

yumでインストールするとバージョンが4.3だったので、makeしてインストールしました。

まずはmakeするために必要なライブラリのインストール

    # yum -y install gcc ncurses-devel

ダウンロード

    # wget http://www.zsh.org/pub/zsh-5.0.5.tar.bz2
    # tar jxvf zsh-5.0.5.tar.bz2

make & install

    # cd zsh-5.0.5
    # ./configure
    # make
    # make install

インストールが正常に終わったら、zshを登録します。

shellの登録 & 変更

    # echo /usr/local/bin/zsh >> /etc/shells
    # chsh

*/usr/local/bin/zsh以外にインストールされている可能性があるので、確認してください*

    $ zsh --version
    zsh 5.0.5 (x86_64-unknown-linux-gnu)

ログイン時にzshが起動するようbashrcに追加する。

~/.bashrc

    # Use zsh
    if [ -f /usr/local/bin/zsh ]; then
        exec /usr/local/bin/zsh
    fi

一度ログアウトし、ログイン後shellがzshに切り替わっていれば完了です。

    $echo $0
    /usr/local/bin/zsh   
    
* [linux/zsh](http://tzono.com/wiki/index.php?linux%2Fzsh)
* [現在の shell を shellscript の中から確認する](http://tkuchiki.hatenablog.com/entry/2014/05/08/222135)

お疲れ様でした。

#### 関連する記事
* [oh-my-zshをインストールする](http://blog.10rane.com/2014/08/06/install-oh-my-zsh/)
