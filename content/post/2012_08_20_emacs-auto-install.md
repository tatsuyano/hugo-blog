+++
date = "2012-08-20T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "auto-install.elをインストールする"
slug = "emacs-auto-install"
+++

anything.elやperl-completion.elを簡単にインストールできるauto-install.elをインストールします。  

## emacs.d以下の構成

.emacs.d以下の構成は[Perl Hacks on Emacs](http://typester.stfuawsc.com/slides/perlcasual2/start.html)に習い、以下のようにします。  

```
.emacs.d
├── conf … init-loader.elで使う。
├── elisp … auto-install.elでインストールしたソースや、手動でダウンロードしたソースの置場所。
└── site … それ以外のライブラリの置場所。
```

## ダウンロード

```
cd .emacs.d/elisp/
wget http://www.emacswiki.org/emacs/download/auto-install.el
```

## .emacsの設定

``` scheme
;; emacs起動時に、読み込むソース(auto-installなど)の置場所を指定します。
(add-to-list 'load-path "~/.emacs.d/elisp")

;; auto-installでインストールしたソースの置場所を指定します。
(require 'auto-install)
(setq auto-install-directory "~/.emacs.d/elisp/")
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Perl Hacks on Emacs](http://typester.stfuawsc.com/slides/perlcasual2/start.html)
* [auto-install.el のインストール](http://yasuwagon.blogspot.jp/2011/02/auto-installel.html)
