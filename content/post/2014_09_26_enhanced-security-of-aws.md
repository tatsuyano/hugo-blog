+++
date = "2014-09-26T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["aws"]
title = "AWSのセキュリティを強化する"
slug = "enhanced-security-of-aws"
+++

AWSを本格的に使い始めて来たので、[こちらの記事](http://qiita.com/yoshidashingo/items/cd206daca0596659b440)を参考にセキュリティの設定を行いました。改めて記事にするか迷いましたが、いちよ備忘のために残しておこうと思います。

今回の作業の流れは以下になります。

1. Authyを使ってrootアカウントに2段階認証を導入する
2. IAMユーザを作成し、IAMユーザでAWSを運用する
3. rootアカウントのAccessKeyを削除する

### Authyの設定、インストール

セキュリティといってもいろいろあると思うのですが、まずはAWSコンソールにログインできるrootアカウント
のセキュリティを強化することだと思います。

今回はAuthyというアプリケーションを使って、AWSに2段階認証を導入しようと思います。

通常の2段階認証だと、ログインする度に事前に登録してあるSMSにPINコードを送ってもらい、そのPINコードを入力すると思うのですが、
Authyを使うことで、Authyが生成するPINコードを入力してログインが可能になります。

Authyの設定は[こちらの記事](http://www.appbank.net/2014/02/13/iphone-application/750689.php)を参考にしてください。

### MFAを有効にし、rootアカウントを2段階認証に対応させる

[IAM](https://console.aws.amazon.com/iam/home)に、MFA(multi-factor authentication)という外部のサービスを使って
2段階認証する機能を使って、AuthyにrootアカウントのPINコードを生成できるように設定します。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_01.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_02.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_03.jpg)

### IAMユーザを作成する

Administrator権限のIAMユーザを作成し、こちらもAuthyに紐付けます。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_04.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_05.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_06.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_07.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_08.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_09.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_10.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_11.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_12.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_13.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_14.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_15.jpg)

### rootアカウントのAccessKeyを削除する

今後rootアカウントを使って、インスタンスの操作はしないので、`AccessKey`を削除します。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_16.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_17.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/enhanced-security-of-aws_18.jpg)

ここまでの作業で、

* rootアカウントに2段階認証の導入
* IAMユーザの作成、及び2段階認証の導入

今後はコンソールにもIAMユーザでログインして作業してください。
お疲れ様でした。

### 参考サイト
* [Authy: 2段階認証のコードをまとめて管理! 紛失時の復元・データ同期もできる](http://www.appbank.net/2014/02/13/iphone-application/750689.php)
* [AWSアカウント作ったらこれだけはやっとけ！IAMユーザーとAuthyを使ったMFAで2段階認証](http://qiita.com/yoshidashingo/items/cd206daca0596659b440)

### 関連する記事
* [Route53のDNSを使う](http://blog.10rane.com/2014/09/11/using-the-dns-of-route53/)
* [Route53にドメインを移管する](http://blog.10rane.com/2014/09/16/transferred-a-domain-to-route53/)
