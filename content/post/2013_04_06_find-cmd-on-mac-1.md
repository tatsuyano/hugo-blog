+++
date = "2013-04-06T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["perl"]
title = "ディレクトリ中のファイルサイズの合計値を再帰的に集計したい その１"
slug = "find-cmd-on-mac-1"
+++

たまにディレクトリ中のファイルサイズの合計値を、再帰的に集計したいときがある。  
で、いつも考えなしにこちらのサイトで紹介されているコマンドを叩いていたわけなんですが、  
  
[ディレクトリ中のファイルサイズ合計値を バイト表示](http://sonic64.com/2004-05-26.html)

```
find -type f -printf "%s\n" |perl -ne '$i++; $byte += $_; $str = "\r$i files, $byte byte"; $str =~ s/(\d{1,3})(?=(?:\d\d\d)+(?!\d))/$1,/g; print $str'
```

mac(osx)だと動かない。どうやらmacのfindのオプションに printf がないもよう。  

[Why does Mac's $find not have the option -printf?](http://stackoverflow.com/questions/752818/why-does-macs-find-not-have-the-option-printf)

そこで printf f '%s\n' のかわりに stat -f '%z' を代用してみた。  
```
find ./ -print0 | xargs -0 stat -f '%z' | perl -ne '$i++; $byte += $_; $str = "\r$i files, $byte byte"; $str =~ s/(\d{1,3})(?=(?:\d\d\d)+(?!\d))/$1,/g; print $str';
```

とりあえず期待した結果になるが、カレントディレクトリのサイズも合計されてしまう。。  
ちょっとカレントディレクトリのサイズを除外する方法がわからなかったので、このまま関数にして使うことにした。  

```
# linux
function bu(){ find -type f -printf "%s\n" |perl -ne '$i++; $byte += $_; $str = "\r$i files, $byte byte"; $str =~ s/(\d{1,3})(?=(?:\d\d\d)+(?!\d))/$1,/g; print $str'; }

# mac
function bu(){ find ./ -print0 | xargs -0 stat -f '%z' | perl -ne '$i++; $byte += $_; $str = "\r$i files, $byte byte"; $str =~ s/(\d{1,3})(?=(?:\d\d\d)+(?!\d))/$1,/g; print $str'; }
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [ディレクトリ中のファイルサイズ合計値を バイト表示](http://sonic64.com/2004-05-26.html)
* [Why does Mac's $find not have the option -printf?](http://stackoverflow.com/questions/752818/why-does-macs-find-not-have-the-option-printf)
