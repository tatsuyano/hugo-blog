+++
date = "2014-10-07T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["heroku"]
title = "herokuことはじめ"
slug = "start-heroku"
+++

まずは[heroku](https://www.heroku.com/)アカウントを作成します。  
2段階認証にも対応しているので、設定したほうがいいと思います。

#### heroku-toolbeltのインストール

CentOS(AmazonLinux)に、`herokuコマンド`をインストールします。

<pre><code class="language-bash">$ wget -qO- https://toolbelt.heroku.com/install.sh | sh
</code></pre>

$HOME/.zshrc

<pre><code class="language-bash">if [ -d /usr/local/heroku ]; then
  export PATH=/usr/local/heroku/bin:$PATH
fi
</code></pre>

<pre><code class="language-bash">$ source $HOME/.zshrc

$ which heroku
/usr/local/heroku/bin/heroku
</code></pre>

* [Vagrant 上の CentOS 6.5 から Heroku を使うには](http://qiita.com/tsumekoara/items/ea5e1b144620ffc4acca)

#### herokuに公開鍵を登録

秘密鍵を作成

<pre><code class="language-bash">$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Home/hoge/.ssh/id_rsa): /Home/hoge/.ssh/heroku_rsa
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Home/hoge/.ssh/heroku_rsa.
Your public key has been saved in /Home/hoge/.ssh/heroku_rsa.pub.
</code></pre>

$HOME/.ssh/config

<pre><code class="language-bash">Host heroku.com
    HostName heroku.com
    IdentityFile ~/.ssh/heroku_rsa
    IdentitiesOnly yes
</code></pre>

herokuにログインし、公開鍵をアップロード

<pre><code class="language-bash">$ heroku login

$ heroku keys:add
Found existing public key: /Home/hoge/.ssh/heroku_rsa.pub
Uploading SSH public key /Home/hoge/.ssh/heroku_rsa.pub... done
</code></pre>

* [Managing Your SSH Keys](https://devcenter.heroku.com/articles/keys)
