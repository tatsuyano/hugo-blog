+++
date = "2015-07-26T21:15:01+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog"]
title = "Hugoに独自ドメインを適応したので、sitemap.xmlを置換する"
slug = "replacing-sitemap-of-hugo"
+++

Hugoに独自ドメインを適応したところ、自動生成されるsitemap.xmlが  
Github pagesのURLで生成されていたので、deploy.shにsedコマンドを追加した。

sedコマンドで上書き置換(sed -i)したら、バックアップファイルが自動で生成  
されるを止めるのはどうしたらいいのだろうか。。

<script src="https://gist.github.com/tatsuyano/57c42f1b050402abd7ea.js"></script>
