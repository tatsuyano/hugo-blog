+++
date = "2014-10-15T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["mac"]
title = "Homebrew CaskでMacアプリをインストールする"
slug = "how-to-install-and-setup-homebrew-cask"
+++

Emacsの`Cask`のように、インストールしたMacアプリをファイルに記述して、一括管理できる方法を探していたところ、
`Homebrew Cask`というものを見つけました。

`Homebrew Cask`はHomebrewの拡張で、コマンドラインでMacアプリをインストールできるようになります。
また、Homebrewの拡張なので、`Brewfile`を使って、アプリも一括インストール(管理)できるようになります。

#### Homebrew Caskのインストール

念のため、先にHomebrew本体、インストール済みのコマンドをアップデートします。

<pre><code class="language-bash">$ sudo -E bash
# brew update
# brew upgrade

# brew tap phinze/homebrew-cask
# brew install brew-cask
</code></pre>
<br>

#### Macアプリの検索

`search`に続けて、検索文字を入力すれば、絞り込み検索ができます。

<pre><code class="language-bash"># brew cask search
</code></pre>
<br>

#### Macアプリの(単体)インストール、アンインストール

デフォルトだと、インストール先は/Users/hoge/Application 配下になります。
変更する場合は、`HOMEBREW_CASK_OPTS`を設定してください。

<pre><code class="language-bash"># export HOMEBREW_CASK_OPTS="--appdir=/Applications"
# brew cask install github
# brew cask uninstall github
</code></pre>
<br>

#### Brewfileでのインストール

Macアプリ以外にもHomebrewでインストールしたコマンドもインストール(管理)できます。

<pre><code class="language-bash"># brew bundle
</code></pre>
<br>

こちらにgitにあげた私の`Brewfile`を貼っておきます。`gist-it`を使うと、
gistではなく、gitに上げたファイルも読み込めるようになりました。

<script src="http://gist-it.appspot.com/github/tatsuyano/dotfiles/blob/master/mac/Brewfile"></script>
<br>

#### 参考サイト
* [homebrew-caskを使って簡単にMacの環境構築をしよう！](http://nanapi.co.jp/blog/2014/03/05/homebrew-cask/)
* [Homebrewで*.dmgなアプリをインストールしたい→それhomebrew-caskで出来るよ](http://qiita.com/paming/items/15ec3543cc094d411428)
