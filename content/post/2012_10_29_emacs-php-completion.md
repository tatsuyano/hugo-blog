+++
date = "2012-10-29T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "emacs"]
title = "php-completion.elをインストールする"
slug = "emacs-php-completion"
+++

## インストール

```
M-x auto-install-batch <RET>
Extension name: php-completion <RET>
```

## .emacsの設定、50_php.elに追加

``` scheme
cat >> .emacs.d/conf/50_php.el
;; php-mode-hook
          (lambda ()
            (require 'php-completion)
            (php-completion-mode t)
            (define-key php-mode-map (kbd "C-o") 'phpcmp-complete)
            (make-local-variable 'ac-sources)
            (setq ac-sources '(
                               ac-source-words-in-same-mode-buffers
                               ac-source-php-completion
                               ac-source-filename
                               ))))
```

補完はするときはC-o、又はauto-complete.elの機能で自動で補完対象が出るようになります。  

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [PHP開発環境 on Emacs](http://blog.fusic.co.jp/archives/94)
