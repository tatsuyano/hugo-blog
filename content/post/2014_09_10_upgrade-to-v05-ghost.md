+++
date = "2014-09-10T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog"]
title = "Ghostをv0.5にアップグレードする"
slug = "upgrade-to-v05-ghost"
+++

運用中のGhost(v0.4)にGhost(v0.5)のファイルを一部コピーするだけで、アップグレードすることができます。

以下のような構成で話をすすめます。

    $ tree $HOME/app -d -L 1

    $HOME/app
    ├── ghost     #<- 運用中のGhost(v0.4)
    └── ghost.v5  #<- コピー元のGhost(v0.5)

### Ghost v0.5のダウンロード

    $ cd app
    $ curl -L https://ghost.org/zip/ghost-latest.zip -o ghost.zip
    $ unzip -uo ghost.zip -d ghost.v5

### Ghost v0.5から一部ファイルをコピーする

コピーする前に`forever`を止めてください。

    $ cd $HOME/app/ghost
    $ NODE_ENV=production forever stop index.js
　
 
    $ cd ../ghost.v5/
    $ cp -a *.md *.js *.json ../ghost/
    $ cp -R core ../ghost
    $ cp -R content/themes/casper ../ghost/content/themes

nodeパッケージを再インストール

    $ cd ../ghost
    $ npm install --production

`config.js`も上書いた(コピーした)ので、urlを変更します。

ghost/config.js

    config = {
        // ### Development **(default)**
        development: {
            // The url to use when providing links to the site, E.g. in RSS and email.
    //        url: 'http://my-ghost-blog.com',
            url: 'http://blog.10rane.com',

        production: {
    //        url: 'http://my-ghost-blog.com',
            url: 'http://blog.10rane.com',
            mail: {},

`forever`を起動

    $ NODE_ENV=production forever start index.js

これでアップグレードの完了です。  
お疲れ様でした。

### 参考サイト

* [Ghostのアップグレードについて](http://docs.ghost.org/ja/installation/upgrading/)

### 関連する記事

* [Amazon LinuxにGhostをインストールする](http://blog.10rane.com/2014/09/09/how-to-install-and-setup-ghost-on-amazon-linux/)
* [GhostにGoogle Analyticsに設置しアクセス元IPを取得する](http://blog.10rane.com/2014/08/10/get-access-ip-installed-google-analytics-to-ghost/)
