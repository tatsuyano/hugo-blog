+++
date = "2017-09-20T10:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["swift","wkwebview"]
title = "wkwebviewからsafariを開く"
slug = "open_safari_from_wkwebview"
+++

wkwebviewで開いているHTML内の`itune.apple.com`へのリンクが反応しなかったので調査した。

開かないリンク(appleのSubscriptionページ)
`https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/DirectAction/manageSubscriptions`

上記のリンクはブラウザやメモから押下すると、ブラウザ -> ituneアプリ 起動するディープリンクになっているのが原因ぽかったので、
`itune.apple.com`を押下した場合はブラウザが立ち上がるよう修正したところ、遷移する(ブラウザ経由だが)ようになった。

修正方法は下記リンクの内容そのまんま(swift3.1用に少し修正)です。自分は`WKNavigationDelegate`を使っているので下記メソッドを追加しました。
``` swift
func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

    guard let url = navigationAction.request.url else {
        decisionHandler(.cancel)
        return
    }

    if url.absoluteString.range(of: "//itunes.apple.com/") != nil {
        if #available(iOS 10.0, *) {
            if UIApplication.shared.responds(to: #selector(UIApplication.open(_:options:completionHandler:))) {
                UIApplication.shared.open(url, options: [UIApplicationOpenURLOptionUniversalLinksOnly:false], completionHandler: { (finished: Bool) in })
            }
            else {
                // iOS 10 で deprecated 必要なら以降のopenURLも振り分ける
                // iOS 10以降は UIApplication.shared.open(url, options: [:], completionHandler: nil)
                UIApplication.shared.openURL(url)
            }
        } else {
            // Fallback on earlier versions
        }
        decisionHandler(.cancel)
        return
    }
    else if !url.absoluteString.hasPrefix("http://") && !url.absoluteString.hasPrefix("https://") {
        // URL Schemeをinfo.plistで公開しているアプリか確認
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
            decisionHandler(.cancel)
            return
        }
    }

    switch navigationAction.navigationType {
    case .linkActivated:
        if navigationAction.targetFrame == nil
            || !navigationAction.targetFrame!.isMainFrame {
            // <a href="..." target="_blank"> が押されたとき
            webView.load(URLRequest(url: url))
            decisionHandler(.cancel)
            return
        }
    case .backForward:
        break
    case .formResubmitted:
        break
    case .formSubmitted:
        break
    case .other:
        break
    case .reload:
        break
    }

    decisionHandler(.allow)
}
```
* [WKWebViewでtarget="_blank"なリンクが開かない時の対処法](http://qiita.com/ShingoFukuyama/items/b3a1441025a36ab7659c)
