+++
date = "2015-08-06T16:51:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs","js"]
title = "EmacsにternをインストールしてjQueryを補完する"
slug = "how-to-install-and-setup-tern"
+++

EmacsでjQueryの補完をするには、[Tern](http://ternjs.net/)というツールを使うことで実現できる。

<br>
### Ternのインストール

まずはTern本体をインストール。このTernとEmacs(tern-mode)が裏で通信しながら補完を行う。
Rubyでいうところのrobeと同じ形。

```
npm install -g tern
```
<br>
### tern、tern-auto-completeのインストール

自分はCaskで管理しているので、Caskファイルに追加し、インストール。
`tern-auto-complate`は「auto-complate」を使ってpopupで補完候補を表示する。

Cask
```
(depends-on "tern")
(depends-on "tern-auto-complete")
```

「js2-mode」が起動したら`tern-mode`と`tern-auto-complate`も起動するよう設定する。

init_loader/03_javascript.el
```
(autoload 'js2-mode "js2-mode" nil t)
(add-to-list 'auto-mode-alist '("\.js$" . js2-mode))
(add-hook 'js2-mode-hook
          (lambda ()
            (tern-mode t)))
 
(eval-after-load 'tern
   '(progn
      (require 'tern-auto-complete)
      (tern-ac-setup)))
```
<br>
### tern-configの設定

ホーム直下に`.tern-config`というJsonファイルを設定する。ここで補完するライブラリなどを指定できる。

```
{
  "libs": [
    "browser",
    "jquery"
  ],
  "plugins": {
     "node": {}
  }
}
```
<br>
### tern-modeの実行

正常にインストールが完了すると、以下のようにポップアップで補完が出るようになる。

<center>
![](https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-install-and-setup-tern_01.png)
</center>

<br>
## 参考サイト

* [Tern demo](http://ternjs.net/doc/demo.html)
* [emacs で javascript なら js2-mode, tern-mode](http://syati.info/?p=2163)
* [Emacs の JavaScript 開発環境を整備する(Tern編)](http://suzuki.tdiary.net/20150120.html)
