+++
date = "2014-08-28T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "init-loaderをCaskでインストールする"
slug = "how-to-install-and-setup-initloader-in-cask"
+++

最近`cask`に乗り換えたので、`init-loader.el`も再度設定し直しました。

### init-loaderのインストール

まずはCaskファイルに設定し、`cask`コマンドで`init-loader`をインストールします。

~/.emacs.d/Cask

    ;; Setup
    (depends-on "init-loader")

次に設定ファイル(elisp)を格納するディレクトリ、`init_loader`を作成します。

    mkdir $HOME/.emacs.d/init_loader

先ほど作成したディレクトリを`init-loader`がロードするように設定します。

~/.emacs.d/init.el

    ;; init-loader
    (custom-set-variables
     '(init-loader-show-log-after-init 'error-only))
     (init-loader-load (concat user-emacs-directory "init_loader"))

### init-loaderで読み込むファイル

とりあえずinit-loaderで読み込むファイルは以下のように設定しました。
まだすべて`cask`に移行しきれていないので、順々に対応していきたいと思います。

    init_loader
    │
    ├── 01_basic.el
    ├── 02_helm.el
    └── 02_migemo.el

お疲れ様でした。

### 参考サイト

* [syohex/dotfiles/emacs](https://github.com/syohex/dot_files/tree/master/emacs)

### 関連する記事

* [Caskをインストールする](http://blog.10rane.com/2014/08/20/install-cask/)
* [Amazon LinuxにMigemoをインストールする](http://blog.10rane.com/2014/08/27/how-to-install-and-setup-migemo-on-amazon-linux/)
* [HelmをCaskでインストールする](http://blog.10rane.com/2014/08/28/how-to-install-and-setup-helm-in-cask/)
