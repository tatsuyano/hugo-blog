+++
date = "2015-03-13T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "heroku", "rails", "ci", "wercker"]
title = "Werckerを使ってHerokuにデプロイする"
slug = "deploying-to-heroku-using-wercker"
+++

Bitbucket(プライベートリポジトリ)に用意したアプリケーション(Rails)を
WerckerでBuildし、HerokuにDeployした時の備忘録です。

## 前提
* Bitbucketにアプリケーションが用意済み
* Werckerにアカウントを持っていて、Bitbacketと連携済み
* Herokuにアカウントを持っていて、Bitbacketと連携済み

## BitbacketのリポジトリをWerckerに登録する

1. プロバイダを選択します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_01.png)

2. リポジトリを選択します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_02.png)

3. リポジトリへの接続方法を選択します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_03.png)

4. `wercker.yml`の内容が自動生成されます。`wercker.yml`は後で新規に作成するので、ここではコピーせずに次に進みます。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_04.png)

5. `Make my app public`に<span id='make_my_app_public'>チェック</span>を入れると、後ほど出てくる`badge`をクリックしたさいに、deploy情報を公開できるようになります(今回はチェックしません)。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_05.png)

これで、WerckerにBitbacketのリポジトリの登録が完了しました。

## wercker.ymlを作成し、アプリケーションをBuildする

WerckerでアプリケーションをBuild、Deployするための設定は、`wercker.yml`に記載します。`wercker.yml`は、アプリケーションのRootディレクトリ直下(Gemfileなどが置いてある階層)に置いてください。

wercker.yml
<pre><code class='language-ruby'>box: wercker/ruby
 services:
     - wercker/postgresql
 build:
     steps:
       - bundle-install
       - rails-database-yml:
           service: postgresql
       - script:
           name: echo ruby information
           code: |
               echo "ruby version $(ruby --version) running!"
               echo "from location $(which ruby)"
               echo -p "gem list: $(gem list)"
       - script:
           name: Set up db
           code: RAILS_ENV=test bundle exec rake db:schema:load
       - script:
           name: Run RSpec
           code: bundle exec rspec
 deploy:
     steps:
         - heroku-deploy:
             install-toolbelt: true
         - script:
             name: Update database
             code: heroku run rake db:migrate --app $APP_NAME
         - script:
             name: Update assets
             code: heroku run rake add_static_assets --app $APP_NAME
</code></pre>
<br>

生成したwercker.ymlを、Bitbacketに`git push`すると、Wercker上に自動でBuildされます。
<pre><code class='language-bash'>$ git add wercker.yml
$ git commit -m 'Add wercker.yml'
$ git push origin master
</pre></code>
<br>

## Deploy先(Heroku)の設定

1. Buildが終わったので、次にDeploy(Heroku)の設定を行います。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_06.png)

2. WerckerにHerokuのAPIキーを設定します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_07.png)

3. `Heroku account page`をクリック先のページ(Heroku)からKeyをコピーします。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_08.png)

4.  `Deploy target name`に適当な名前を設定します。また、`auto deploy ...`のチェックボックスをONにすると、２回目以降のBuild時に、自動でDeployされるようになります。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_09.png)

5. `Create new Heroku app`を押下し、アプリケーション名と、Regionを設定します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_10.png)

6. 先ほど作成したwercker.ymlで使っている変数名を設定します。
今回は`APP_NAME`という変数に、先ほどのアプリケーション名を設定します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_11.png)

7. Deployの設定が終わったら、次にBuildのログのリンクを押下してください。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_12.png)

8. `Deploy to`ボタンに先ほど設定した`Deploy target name(production)`が表示されます。このボタンをクリックするとDeployが開始されます。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_13.png)

## README.mdにbadgeを追加する

GithubのREADMEによくついている例のあれです。あるとなんとないかっこいいのでつけましょう。

<a href='#make_my_app_public'>リポジトリをWerckerに登録した</a>さいにあった`Make my app public`のチェックをONにした状態で、このbadgeをクリックすると、BuildとDeployのログが確認できるようになります。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/deploying-to-heroku-using-wercker_14.png)

こんな簡単に登録できて、しかも無料(今のところ)なんて、Werckerすごくいいですね。以上、お疲れ様でした。

## 参考サイト

* [Githubのプライベートリポジトリでも無料で使えるCI、Werckerを使ってrails newからHerokuのデプロイまでやってみる](http://blog.mah-lab.com/2014/01/08/rails-wercker-heroku-deploy/)
* [bitbucketとwerckerで０円CIをする](http://razokulover.hateblo.jp/entry/2014/05/21/000306)
* [RailsアプリをWerckerを通してHerokuに上げる際に -bash: heroku: command not found とかなった場合の対応](http://qiita.com/tsumekoara/items/62cfa777b108acb5659f)
