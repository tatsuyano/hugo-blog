+++
date = "2013-03-19T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "coffeescript"]
title = "coffee-modeでauto-completeを使えるようにする"
slug = "coffee-mode-used-ac-dict"
+++

まずは coffee-mode.el を el-get で通常通りにインストールします。  

## auto-complete で補完に使うdict(辞書)について
補完をするときに参照している辞書は、.emacs.d/elisp/el-get/auto-complete/dict/ 配下にあります。  
デフォルトでは、coffee-mode 用のdictがないので、js2-mode 用に公開されていた辞書をインストールします。  

```
cd .emacs.d/elisp/el-get/auto-complete/dict/
wget https://raw.github.com/sandai/dotfiles/master/.emacs.d/ac-dict/js2-mode
```

## 設定
coffee-mode でauto-complete を使えるようにし、指定した辞書で補完するようにします。  

52-coffee-mode.el
``` scheme
(require 'coffee-mode)

;; デフォルトだとインデントが８スペースなので、２スペースに変更
;; http://qiita.com/items/a8d96ae95a1838500e37
(defun coffee-custom ()
  "coffee-mode-hook"
  (set (make-local-variable 'tab-width) 2)
  (setq coffee-tab-width 2))
(add-hook 'coffee-mode-hook
  '(lambda() (coffee-custom)))

;; auto-complete
(add-to-list 'ac-modes 'coffee-mode) ;; coffee-modeでACを使えるようにする

;; coffee-modeで、追加したjs2-modeのdict(辞書)で補完するようにする
;; wget https://raw.github.com/sandai/dotfiles/master/.emacs.d/ac-dict/js2-mode
(add-hook 'coffee-mode-hook
  '(lambda ()
    (add-to-list 'ac-dictionary-files "~/.emacs.d/elisp/el-get/auto-complete/dict/js2-mode")
))
```
