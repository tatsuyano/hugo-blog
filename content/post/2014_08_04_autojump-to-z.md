+++
date = "2014-08-04T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["zsh"]
title = "autojumpからzに乗り換えた"
slug = "autojump-to-z"
+++

`autojump`を今まで使っていたが、zshrcを編集していたらエラーを吐くようになったので、
このタイミングで`z`に乗り換えてみた

#### インストール

    $ cd .dotfile
    git clone https://github.com/rupa/z.git .zsh

.zshrc

    _Z_CMD=j
    source ~/.dotfile/.zsh/z.sh
    precmd() {
      _z --add "$(pwd -P)"
    }

キーバインドは`j`に変更。
ホーム直下に`.z`というファイルが作成され、そのファイルに履歴が溜まっていく

* [z](https://github.com/rupa/z)
* [AUTOJUMPよりZ.SHのほうがPYTHONなしで動いて良いよ](http://project-p.jp/halt/?p=1724)
