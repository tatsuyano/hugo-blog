+++
date = "2015-07-28T11:51:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs","js"]
title = "Emacsにflymake-jshintをインストールする"
slug = "how-to-install-and-setup-jshint"
+++

今読んでいる [シングルページWebアプリケーション](http://www.oreilly.co.jp/books/9784873116730/) の
サンプルコードが「JSLint」を使っていたので、インストールしようと思ったが
「JSHint」のほうが良さそうなのでEmacsに「flymake-jshint」をCask経由でインストールした。

### JSHintのインストール
まずはnpmでJSHint本体をインストール。
```
npm install -g jshint
```
<br>
### flymake-jshintのインストール
自分はCaskで管理しているので、Caskファイルに追加しインストール。
flymake-jshintは「js2-mode」に必須のようなので、インストールしていない場合は先にインストールする。
```
# Cask
(depends-on "flymake-jshint")
```
JSHintのパスを通し、js2-modeが起動したら、JSHintがロードするよう設定する。
```
# init_loader/03_javascript.el
(setq exec-path (append exec-path '("~/.anyenv/envs/ndenv/shims/")))
(add-hook 'js2-mode-hook '(lambda ()
                              (require 'flymake-jshint)
                              (flymake-jshint-load)))
```
<br>
### JSHintの実行
正常にインストールが完了すると、タイポなどミスがあると以下のようにポップアップで警告が出るようになる。
うまくいかない場合は`M-x flymake-jshint-load`で手動でJSHintを起動し、エラーを確認すること。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-install-and-setup-jshint_01.png)
<br>

## 参考サイト

* [JSLint から JSHint をフォークした理由(翻訳)](http://blog.node.ws/?p=1379)
* [Emacsでflymake-jshintを利用してリアルタイムでのJavaScript文法チェックができるようにする](http://safx-dev.blogspot.jp/2013/05/emacsflymake-jshintjavascript.html)
