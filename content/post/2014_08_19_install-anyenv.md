+++
date = "2014-08-19T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "ruby", "nodejs", "python"]
title = "anyenvをインストールする"
slug = "install-anyenv"
+++

以前導入したanyenvのインストール方法を忘れないようメモしておきます。

anyenvは ruby、perl、python、nodejs、php(phpenv) のバージョン管理を一元化してくれるツールです。これでhome直下がずいぶんスッキリ。素敵です。

### インストール

ダウンロード

    $ git clone https://github.com/riywo/anyenv ~/.anyenv

$HOME/.zshrc

    $ echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> ~/.zshrc
    $ echo 'eval "$(anyenv init - zsh)"' >> ~/.zshrc

私は`oh-my-zsh`を使っているので、実際は`~/.zshrc`ではなく、`~/.oh-my-zsh/custom/main.zsh`に設定しています。

シェルを再実行し、anyenvの設定を反映させます。  
`source $HOME/.zshrc` でも結構です。

    $ exec $SHELL -l
    $ which anyenv
    ~/.anyenv/bin/anyenv

* [anyenvで開発環境を整える](http://qiita.com/luckypool/items/f1e756e9d3e9786ad9ea)
* [completions/plenv.bash:16: command not found: completeの対処法](http://qiita.com/DQNEO/items/ebf50c39f83970f1127c)

### rubyのインストール

とりあえずanyenv経由でrubyをインストールします。
rubyをインストールするには、事前にOpenSSL系のライブラリが必要なのでインストールしておきます。

また[別記事](http://blog.10rane.com/2014/09/01/set-up-ruby-mode-of-emacs/)で紹介している`robe`を使うには、`read-line`が必要なので、入れておきます。

    $ sudo yum -y install openssl-devel
    $ sudo yum -y install readline-devel

* [CentOSに rbenv 経路で Ruby 2.0.0-p195を入れようとしたら、エラー](http://futurismo.biz/archives/1389)

まずはrbenvはインストールします。

    $ anyenv install rbenv
    $ anyenv versions
    rbenv:
    * system (set by /home/$USER/.anyenv/envs/rbenv/version)

バージョンは2.1.0を入れてみます。

    $ rbenv install 2.1.0
    $ rbenv global 2.1.0

    $ anyenv versions
    rbenv:
    system
    * 2.1.0 (set by /home/$USER/.anyenv/envs/rbenv/version)

*反映されない時は*`source $HOME/.zshrc`*で設定を読み込み直して下さい。*

試しに`bundler`をインストールしてみます。

    $ which gem
    /home/$USER/.anyenv/envs/rbenv/shims/gem
    $ gem install bundler
    $ which bundle
    /home/$USER/.anyenv/envs/rbenv/shims/bundle

### pythonのインストール

    $ sudo yum -y install patch
    $ anyenv install pyenv
    $ pyenv install 3.3.3
    $ pyenv global 3.3.3

### node.jsのインストール

    $ anyenv install ndenv
    $ ndenv install v0.10.26
    $ ndenv global v0.10.26

他の言語も流れは同じです。  
お疲れ様でした。

### 参考サイト

* [anyenvで開発環境を整える](http://qiita.com/luckypool/items/f1e756e9d3e9786ad9ea)
* [completions/plenv.bash:16: command not found: completeの対処法](http://qiita.com/DQNEO/items/ebf50c39f83970f1127c)

### 関連する記事
* [phpenvを導入する](/2014/12/04/how-to-install-and-setup-phpenv/)
