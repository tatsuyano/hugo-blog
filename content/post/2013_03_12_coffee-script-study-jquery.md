+++
date = "2013-03-12T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["coffeescript"]
title = "CoffeeScriptの勉強 その４(クラス、Jquery)"
slug = "coffee-script-study-jquery"
+++

#### クラス

<pre><code class="language-coffeescript">class Animal
  constructor:(name) -> @name = name
    say:(word) -> console.log "#{@name} said: #{word}"

class Dog extends Animal
  constructor:(name) -> super name
    say:(word) -> super "Bowwow, #{word}"

dog = new Dog("Bob")
dog.say("hello")
</code></pre>
<br>

#### 静的なプロパティ

クラスの静的なプロパティを定義するには変数名の頭に@を付けます。

<pre><code class="language-coffeescript">class Dog
  @TYPE_CHIHUAHUA = 1
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">var Dog;
Dog = (function() {
  function Dog() {}
    Dog.TYPE_CHIHUAHUA = 1;
    return Dog;
})();
</code></pre>

クラスの静的なプロパティの使い方というか、そもそもJsのオブジェクト指向、クロージャの実装がしっかりと理解していない。ここは後日勉強する。
<br>

#### 無名関数

setTimeoutなど引数に無名関数を渡したい場合は、次の行頭にカンマを付けます。

<pre><code class="language-coffeescript">setTimeout ->
  console.log("ok")
  ,1000
</code></pre>

コンパイル結果↓

<pre><code class="language-coffeescript">setTimeout(function() {
  return console.log("ok");
},1000);
</code></pre>
<br>

#### コンパイルせずに、直接CoffeeScriptを記述する方法

本家サイトに置いてある、「coffee-script.js」を読み込んで、scriptタグ
「script type="text/coffeescript"」で囲むと、直接CoffeeScriptが記述できる。

``` html
<script type="text/javascript" src="http://coffeescript.org/extras/coffee-script.js"> </script>
<script type="text/coffeescript">
        hogehoge...
</script>
```
<br>

#### CoffeeScriptで、Jqueryを使う

これは、とくに考えずに、そのままCoffeeScriptで記述するだけ。
Jsで書くより、カッコの入れ子がなくなり読みやすくなる。

``` html
<script type="text/coffeescript">
    $('#add-more').click ->
          $('#add-more').hide()
          $('#task-add').fadeIn()
</script>
```
<br>

#### 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [今日から始めるCoffeeScript](http://tech.kayac.com/archive/coffeescript-tutorial.html#coffee_syntax_classes)
* [JavaScriptのクロージャを利用したprivate staticメンバ](http://kudox.jp/java-script/js-private-static)
