TypeScriptのインストール
========================

## はじめに
TypeScriptをはじめるにあたり、開発環境(Nodeのtool)は最小限(npm+TypeScript+Webpack)に留めることとする。  
もちろんWebpack以外のtoolも使って良いが、はじめは自身ローカル環境内で使うこと(便利そうなものは、逐次package.jsonに追加するのでslack)。  
(bower,yeoman,gulp,grant,browserify,babelなどを併用してもいいが、npmとWebpackだけでも事足りそうなので使わない予定。  
このあたりのツールを調べ出すとコードを書く前に日が暮れる)  
  
jQueryをはじめとしたnpmで提供されているライブラリは、npmでインストールすること。  
型定義ファイルもnpmで公開されているものはインストール必須。型定義ファイルの公開の有無は下記サイト判断する事とする。  

* [TypeScript Types Search](http://microsoft.github.io/TypeSearch/)
  
またjQueryのPluginの多くはCommonJS(Node)に未対応なものが多い。その場合はソースをDLし、TypeScriptからimportする形で読み込む。  


## OSとIDE(editor)について
これから先の説明は、Macに[homebrew](https://brew.sh/index_ja.html)がインストールされているものとして進める。  
  
IDE(editor)に関しては[VisualStudioCode(VScode)](https://code.visualstudio.com/)が良さそうだったので、VScode上でのtypescriptの設定方法を記述する。  
WindowsユーザにはVisualStudioCommunityという選択肢もある(mac版はTypeScriptのPluginが提供されていなかったので断念)が、  
Node本体をIDEに内包しているらしいので、細かい所に手が出せない可能性があるかも。  


## Nodejsについて
Nodejsのバージョンはaocca_devのchatのバージョンに合わせ、8系(偶数=長期サポート版)の最新とする。  
ただ今後バージョンが変更することも考慮し、`ndenv`や`nodebrew`などバージョン管理ツール経由でNodejsをインストールすること。  

* [Ndenv](http://brewformulas.org/Ndenv)
* [Homebrewからnodebrewをインストールして、Node.jsをインストールするまで](https://qiita.com/limonene/items/a10c2755dd2784357c43)

### CommonJS、ESModuleどちらでmoduleをimportするか
詳しくは下記リンクを参照。結論としてはTypeScriptからJSにトランスコンパイラするさいはESModuleでコンパイラする。  
もし何か問題がでればCommonJSに設定を切り代えてコンパイラし直す。  

* [ES Modules と Node.js について](http://yosuke-furukawa.hatenablog.com/entry/2016/05/10/111102)


## TypeScriptについて
JSに型を持たすことができる(型定義ファイルというものをコンパイル時に参照しチェックする)AltJS。  
文法はES6(ES2015)+Javaとほぼ同じ。またバージョン2.1からは、targetにes5を設定しトランスコンパイラしても、  
ES2017の`Async/Await`も使える模様なので、積極的に(出来る範囲で)Ecmascriptの新しい文法を使うこと。  

* [TypeScript Handbook](https://www.typescriptlang.org/docs/handbook/basic-types.html)
* [TypeScript Handbook(有志翻訳)](https://qiita.com/murank/items/9c1f633545842c953947)
* [JavaScriptは如何にしてAsync/Awaitを獲得したのか Qiita版](https://qiita.com/gaogao_9/items/5417d01b4641357900c7)
* [Async Await in TypeScript](https://qiita.com/ohgiwk/items/aeb8a726e2b6bfc33a49)

### 型定義ファイルの扱いについて
型定義ファイルの管理はnpmで行う(`tsd`コマンドとかは使わない)。  
~~上記でも書いたが、自分たちが作ったソース以外は型定義ファイルがなくても問題ないので、必要以上に型定義ファイルにとらわれない。~~  
Nodejsで提供されている(npmでインストールする)ライブラリをTypeScriptで使う場合は、型定義ファイルが必須。  
Nodejsインストールしたライブラリで型定義ファイルが存在しない場合は、型定義ファイルを自前で用意するか、  
NodejsでインストールせずにローカルにDLしてTypeScriptからimportする。  

``` typescript
import './jquery-3.2.1';
let $ = require('jquery');

$(() => {
    console.log('型定義ファイルなしでTypesSriptを実行');
});
```

* [特別なツール不要！ TypeScript 2時代の型定義ファイルの取り扱い方](https://qiita.com/tonkotsuboy_com/items/634b0921b6170cf56813)


## Nodejsの環境構築
ここから先はNodejs(v8.9.3以上)、npm(5.5.1以上)がインストールされているものとして進める。
``` shell
$ node -v
v8.9.3

$ npm -v
5.5.1
```

## ソースのディレクトリ構成
最終的に参照されるソースは、トランスコンパイラされたJsのみなので、それ以外のライブラリやTypeScriptのコードはどこに置いてもいいのだが、  
FuelPHPのディレクトリ構成と、コンパイラしたJs以外は、公開領域以外に置くこととし、以下のように(とりあえず)配置。

``` shell
mastersns
.
├── appspec.yml
├── composer.json
├── composer.lock
├── composer.phar
├── fuel
├── mastersns_ts(Nodejsのdocument_root)
├── oil
└── public
    ├── assets
    │   ├── admin
    │   ├── css
    │   ├── data
    │   ├── fonts
    │   ├── img
    │   └── js(コンパイルしたJs)
    ├── favicon.ico
    ├── index.php
    ├── maintenance
    │   └── index.html
    └── web.config
```


### ライブラリのインストール
まずはTypeScriptとTslintをグローバルインストール。
``` shell
$ npm install -g typescript tslint

$ npm list --global --depth=0
/Users/noguchi/.anyenv/envs/ndenv/versions/v8.9.3/lib
├  npm@5.5.1
├  tslint@5.8.0
└  typescript@2.6.2
```

次にpackage.jsonを自動作成し、jQueryのインストール。
``` shell
$ npm init -y
$ npm install --save jquery jquery-ui
```

Webpackとその関連ライブラリのインストール、型定義ファイルのインストール
``` shell
$ npm install --save-dev webpack webpack-dev-server ts-loader @types/node @types/jquery @types/jqueryui

$ npm list --depth=0
mastersns_ts@1.0.0 /Users/noguchi/vagrant/vm/ao/mastersns/mastersns_ts
├  @types/jquery@3.2.17
├  @types/jqueryui@1.12.0
├  @types/node@4.0.35
├  jquery@3.2.1
├  jquery-ui@1.12.1
├  ts-loader@3.2.0
├  webpack@3.10.0
└  webpack-dev-server@2.9.7
```

ライブラリのインストール元が見つからない等のエラーがでた場合は、macの/etc/hostsファイルにIPを追加し回避。

* [ERR! network getaddrinfo ENOTFOUND registry.npmjs.org registry.npmjs.org:443](http://jaga.hatenablog.com/entry/2017/03/19/152739)

Nodejsのライブラリ(モジュール)のインストール先は用途によって分けることができる。  
ざっくり分けると、TypeScriptなどいつも使うライブラリはグローバル(`--global`or`-g`)。  
開発時にしか使わないライブラリ(Webpackなど)は、`--save-dev`。JqueryなどWebアプリ内だけでだけで使うものは`--save`  

|依関係              |オプション     |省略形|意味                                                                       |
|:-------------------|:--------------|:----:|:--------------------------------------------------------------------------|
|dependencies	     |--save         |-S    |jQueryなどのソースコードで利用するライブラリ                               |
|devDependencies     |--save-dev     |-D    |Webpackなどソースコードで利用しないライブラリ                              |
|peerDependencies	 |               |      |パッケージのプラグインなどで依存バージョンを明示するときに使用             |
|optionalDependencies|--save-optional|-O    |環境によって依存するライブラリが変わるもので使用                           |
|bundledDependencies |               |      |既存のパッケージに変更を加えてnpmには無いパッケージなどを明示するときに使用|

------

marrish {
  lang: js,
  ajax: jQuery,
  template: jquery-tmpl,
  validate: jquery-validate,
  etc: jquery-plugin,
};

aocca {
  lang: TypeScript,
  ajax: (axios|superagent|jquery), // Promise & async/await を使う(jqueryの場合は使い方がよくわかっていない)
  template: (vue|jsrender), // jquery-tmpl は開発中止、代案は同作者による jsrender.js(jqueryがnot must)
  validate: (vue|jquery-validate),
  etc: (vue|jquery-plugin),
  bundling: webpack,
  forFuelPHP: laravel-mix, // vuejsを導入するなら
}

------



* [tsconfig.jsonを設置する](https://qiita.com/yuya_presto/items/f625da7b1a4d21c6ce7a#tsconfigjson%E3%82%92%E8%A8%AD%E7%BD%AE%E3%81%99%E3%82%8B)
* [VSCode + Typescript でデバッグを有効にする](https://qiita.com/TsuyoshiUshio@github/items/9879ea04cdd606982a65)
* [サーバー系エンジニアがVue.jsとTypeScriptに入門して10日ほど勉強した内容をまとめてみました。](https://qiita.com/poly_soft/items/39bde910f813fc9ab84a)
* [各種ES6環境構築まとめ(Typescriptもあるよ)](https://qiita.com/kyasbal_1994/items/5b9da881104cf3145bca)
* [Visual Studio CodeでTypeScript開発のノウハウ (ブラウザ編)](https://qiita.com/takao_mofumofu/items/4f8d365846e2f770bd6e)
* [Visual Studio Codeを使ったTypeScript開発 (環境構築)](https://www.sumirelab.com/docs/tech/typescript/visual-studio-code%E3%82%92%E4%BD%BF%E3%81%A3%E3%81%9Ftypescript%E9%96%8B%E7%99%BA-%E7%92%B0%E5%A2%83%E6%A7%8B%E7%AF%89/)


* [TypeScript でフロントエンドを実装する](http://blog.syati.info/post/typescript_webpack/)
* [今時のフロントエンド開発2017 (2. 構築編)](https://qiita.com/Lyude/items/c4250b6be4eeece90348)
* [今時のフロントエンド開発2017 (3. webpack編)](https://qiita.com/Lyude/items/7b8104418a5dd03c7d30)

* [型定義ファイル自作時のおく場所について](https://teratail.com/questions/78174)
* [GitHub - Microsoft/TypeScript](https://github.com/Microsoft/TypeScript)
* [@ConquestArrow](https://qiita.com/ConquestArrow?page=2)

tsc --init

tsconfig.json
{
  "compilerOptions": {
      "module": "commonjs",
      "noImplicitAny": true,
      "removeComments": true,
      "preserveConstEnums": true,
      "sourceMap": true
  },
    "include": [
        "src/ts/*"
    ],
    "exclude": [
        "node_modules"
    ]
}
最初の節はコンパイルオプションです。それぞれの意味は下記の通りです。

オプション	説明
module	　 生成するモジュール関連のコードの対象ソフトウェアを指定
noImplicitAny	暗黙的に利用される"any"型をエラーとする
removeComments	コンパイル時にコメントを除去する
preserveConstEnums	"const enum"を使用する
sourceMap　	".map"ファイルを生成する
"include"の節はコンパイル対象のファイルの指定です。srcフォルダ内のtsフォルダ内のすべてのファイル（＊はワイルドカードです。）が対象です。
"exclude"の節は逆にコンパイル対象外の指定です。npmによって管理されている外部ライブラリを入れるフォルダを対象外としています。


## Nodeのプロジェクト作成 & webpackのインストール

$ mkdir -p study_typescript/src
$ cd study_typescript

プロジェクト管理ファイルpackage.jsonの生成。
* `-y` ... プロジェクト名などを自動で設定しpackage.jsonを生成する

``` shell
$ npm init -y
Wrote to /Users/noguchi/src/github.com/tatsuyano/study_typescript/package.json:

{
  "name": "study_typescript",
  "version": "1.0.0",
  "description": "study_typescript",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "git+https://github.com/tatsuyano/study_typescript.git"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://github.com/tatsuyano/study_typescript/issues"
  },
  "homepage": "https://github.com/tatsuyano/study_typescript#readme"
}
```

``` shell
$ npm install --save-dev webpack node-sass css-loader sass-loader style-loader postcss-loader autoprefixer file-loader url-loader ts-loader
```





$ brew cask install visual-studio-code

## vscodeのpulgin
* Debugger for Chrome
* Emacs Friendly keymap
* TSLint



### create sample code
#### greeter.ts
``` javascript
interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

let user = { firstName: "Jane", lastName: "User" };

document.body.innerHTML = greeter(user);
```

```
$ tsc greeter.ts
greeter.js <- create file!
```

#### greeter.js
``` javascript
function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = { firstName: "Jane", lastName: "User" };
document.body.innerHTML = greeter(user);
```
