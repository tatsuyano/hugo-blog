+++
date = "2013-10-16T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "elpaでyasnippet.elをインストールする"
slug = "install-yasnippet-to-elpa"
+++

今さらですけどysnippet.elをインストールすることにしました。  
でいつものようにel-getでインストールしようとしたんですが、  
昨日scss-modeをインストールしようとした時と同じで、  
githubに接続するところで止まってしまう(インストールが完了しない)。
  
もうel-getやめようかなあ。elpaだけのほうが楽っぽいし。。  
とりあえず、今回はelpaでインストールすることにしました。  

### elpaでのインストール

M-x list-packages でパッケージリストを表示させ、ysnippetの行で[x] & [i]でインストール。

~/.emacs.d/conf/82-yasnippet.el
``` scheme
;; yasnippetを置いているフォルダにパス
(add-to-list 'load-path
             (expand-file-name "~/.emacs.d/elisp/el-get/package/elpa/yasnippet-0.8.0"))

(require 'yasnippet)

(setq yas-snippet-dirs
      '("~/.emacs.d/elisp/el-get/package/elpa/yasnippet-0.8.0/snippets"
        ))
(yas-global-mode 1)
```
### スニペットの登録

試しに、html-modeでスペースのエスケープ文字を登録してみます。

[sp]と入力してtabを押下すると[&nbsp]と出力できるようにする


htmlモードに変更
``` scheme
M-x html-mode
```

snippetのひな形作成
``` scheme
M-x yas/new-snippet
```

``` scheme
# -*- mode: snippet -*-
# name: &nbsp;
# key: sp
# --
&nbsp;
```

html-modeで使いたいので、保存先は以下にします。
```
/.emacs.d/elisp/el-get/package/elpa/yasnippet-0.8.0/snippets/html-mode/sp
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Emacsで「ysnippet.el」を使ってみる](http://rakkyoo.net/?p=1157)
* [Emacs/yasnipettで、やさしいコードスニペットの作り方](http://d.hatena.ne.jp/mizchi/20100806/1281093295)
* [yasnippet 8.0の導入からスニペットの書き方、anything/helm/auto-completeとの連携](http://fukuyama.co/yasnippet)
