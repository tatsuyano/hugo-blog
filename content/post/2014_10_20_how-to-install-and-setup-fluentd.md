+++
date = "2014-10-20T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["middleware", "fluentd"]
title = "fluentdをインストールする"
slug = "how-to-install-and-setup-fluentd"
+++

blogのアクセス数がほんの少し増えてきたので、fluentd(フルーエントディ) をインストールしてみます。
今回、fluentd をインストールする環境は、以下になります。

* AmazonLinux
* nginx
* ruby 2.1.0 (anyenv)

### fluentd を導入すると何ができるか

fluentd はログなどを取得し、取得したデータを整形して elasticsearch + kibana などに渡すことで、データを簡単に可視化することができます。

### fluentdをgem経由でインストールする

まずは[公式](http://docs.fluentd.org/articles/install-by-gem)どおりに`gem`でインストールしてみます。

*注意*  
[こちら](http://eure.jp/blog/fluentd_elasticsearch_kibana)の記事を参考に、[yum経由でインストール](#yum_install)も行いました。

<pre><code class="language-bash">$ gem install fluentd --no-ri --no-rdoc
</code></pre>
<br>

メモリが割り当てられない？というエラーが出ました。

<pre><code class="language-bash">ERROR:  Error installing fluentd:
        ERROR: Failed to build gem native extension.

    $HOME/.anyenv/envs/rbenv/versions/2.1.0/bin/ruby extconf.rb
    Cannot allocate memory - $HOME/.anyenv/envs/rbenv/versions/2.1.0/bin/ruby extconf.rb 2>&1

Gem files will remain installed in $HOME/.anyenv/envs/rbenv/versions/2.1.0/lib/ruby/gems/2.1.0/gems/yajl-ruby-1.2.1 for inspection.
Results logged to $HOME/.anyenv/envs/rbenv/versions/2.1.0/lib/ruby/gems/2.1.0/extensions/x86_64-linux/2.1.0-static/yajl-ruby-1.2.1/gem_make.out
</code></pre>
<br>

試しに`aws-sdk`をインストールしてから、再度インストールしてみたらうまくいきました。

<pre><code class="language-bash">$ gem install aws-sdk
$ gem install fluentd --no-ri --no-rdoc

$ source $HOME/.zshrc
$ fluentd --version
fluentd 0.10.55
</code></pre>
<br>

#### セットアップ

次はセットアップを行います。
指定した`fluent`のホームディレクトリ直下に`fluent.conf`というファイルが生成されます。

<pre><code class="language-bash">$ fluentd --setup app/fluent
Installed app/fluent/fluent.conf.

$ tree app/fluent
app/fluent
├── fluent.conf
└── plugin
</code></pre>
<br>

#### 起動方法

試しに公式どおりに起動してみます。
`-vv`でトレースモード、`&`でバッググラウンドで起動します。
起動後にjsonを`fluent-cat`に渡して、`debug`タグの設定(stdout)どおり出力します。

<pre><code class="language-bash">$ fluentd -c app/fluent/fluent.conf -vv &
[1] 22209

$ echo '{"json":"message"}' | fluent-cat debug.test

2014-10-20 05:48:01 +0000 [trace]: plugin/in_forward.rb:189:initialize: accepted fluent socket from '127.0.0.1:44260': object_id=69925032642500
2014-10-20 05:48:01 +0000 debug.test: {"json":"message"}
2014-10-20 05:48:01 +0000 [trace]: plugin/in_forward.rb:245:on_close: closed fluent socket object_id=69925032642500
</code></pre>
<br>

### Nginxのログをfluentdに流す

`fluent.conf`には、ログの入力方法`<source>`と、出力方法`<match>`を設定することができます。

<a name="conf">app/fluent/fluent.conf</a>

<pre><code class="language-bash"># access_log
&lt;source>
  type tail
  path /var/log/nginx/access.log
  format ltsv
  time_key time_local
  time_format %d/%b/%Y:%H:%M:%S %z
  pos_file /var/tmp/nginx_access_log.pos
  tag nginx.access
 &lt;/source>
 &lt;match nginx.access>
  type elasticsearch
  host localhost
  port 9200
  type_name access_log
  logstash_format true
  logstash_prefix nginx_access
  logstash_dateformat %Y%m
  buffer_type memory
  buffer_chunk_limit 10m
  buffer_queue_limit 10
  flush_interval 1s
  retry_limit 16
  retry_wait 1s
  &lt;/match>
  
 # error_log
 &lt;source>
  type tail
  path /var/log/nginx/error.log
  format /^(? &lt;time>[^ ]+ [^ ]+) \[(? &lt;log_level>.*)\] (? &lt;pid>\d*).(? &lt;tid>[^:]*): (? &lt;message>.*)$/
  pos_file /var/tmp/nginx_error_log.pos
  tag nginx.error
 &lt;/source>
 &lt;match nginx.error>
  type elasticsearch
  host localhost
  port 9200
  type_name error_log
  logstash_format true
  logstash_prefix nginx_error
  logstash_dateformat %Y%m
  buffer_type memory
  buffer_chunk_limit 10m
  buffer_queue_limit 10
  flush_interval 1s
  retry_limit 16
  retry_wait 1s
 &lt;/match>
</code></pre>

今回、fluentd を自分の$HOME以下にインストールしているので、
`path`に設定してあるログのパーミッションに注意してください。

また、`format`をltsvに変更しているので、Nginx のログフォーマットも ltsv に合わせてください。

#### Nginxのログをltsvに変更する

/etc/nginx/nginx.conf

<pre><code class="language-bash">log_format  ltsv  "remote_addr:$remote_addr\t"
                  "remote_user:$remote_user\t"
                  "time_local:$time_local\t"
                  "request:$request\t"
                  "status:$status\t"
                  "body_bytes_sent:$body_bytes_sent\t"
                  "http_refer:$http_referer\t"
                  "http_user_agent:$http_user_agent";

access_log  /var/log/nginx/access.log  ltsv;
</code></pre>
<br>

現在生成されている`access.log`を削除し、Nginx の再起動、及び fluentd を(バックエンドで)起動します。

<pre><code class="language-bash">$ sudo rm /var/log/nginx/access.log
$ sudo /etc/init.d/nginx restart

$ fluentd -c app/fluent/fluent.conf -vv &
</code></pre>

/tmp/nginx.logが更新されていれば成功です。

### <a name="yum_install">yum経由でのインストール

トレジャーデータ社が用意したyumリポジトリを使い、td-agent(fluentd) をインストールします。

<pre><code class="language-bash">$ sudo bash

# cat > /etc/yum.repos.d/treasuredata.repo
[treasuredata]
name=TreasureData
baseurl=http://packages.treasure-data.com/redhat/$basearch
gpgcheck=0

$ sudo yum -y install td-agent
$ sudo chkconfig td-agent on
</code></pre>
<br>

#### fluentd プラグインインストール

anyenvで管理していないrubyをupdateするのは少し気が引けます。。

<pre><code class="language-bash">$ sudo /usr/lib64/fluent/ruby/bin/fluent-gem update
$ sudo /usr/lib64/fluent/ruby/bin/fluent-gem install fluent-plugin-config-expander
$ sudo /usr/lib64/fluent/ruby/bin/fluent-gem install fluent-plugin-elasticsearch
</code></pre>
<br>

#### configファイル

td-agentのconfigファイルは`/etc/td-agent/td-agent.conf`にあるので、
[以前作ったconfigファイル](#conf)で上書きします。

すでに`posファイル`が生成されている場合は、削除してください。

<pre><code class="language-bash">$ sudo cp $HOME/app/fluentd/fluent.conf /etc/td-agent/td-agent.conf
$ sudo rm /var/tmp/nginx_access_log.pos
$ sudo rm /var/tmp/nginx_error_log
</code></pre>
<br>

#### 起動、停止

<pre><code class="language-bash">$ sudo /etc/init.d/td-agent start
$ sudo /etc/init.d/td-agent stop
</code></pre>
<br>

### 参考記事

* [Installing Fluentd Using Ruby Gem](http://docs.fluentd.org/articles/install-by-gem)
* [AWS（Amazon Linux）にfluentdをいれてみた](http://lab.bizreach.co.jp/66/)
* [NginxのアクセスログをLTSV形式にしてfluentdで回収する](http://blog.excale.net/index.php/archives/2682)
* [柔軟なログ収集を可能にする「fluentd」入門](http://knowledge.sakura.ad.jp/tech/1336/)
* [Common Log Formats and How To Parse Them](http://docs.fluentd.org/ja/articles/common-log-formats)
* [今日から始めるfluentd × Elasticsearch × kibana – カジュアルな解析・高速化](http://eure.jp/blog/fluentd_elasticsearch_kibana/)

