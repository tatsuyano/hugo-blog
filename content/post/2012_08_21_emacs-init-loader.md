+++
date = "2012-08-21T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "init-loader.elをインストールする"
slug = "emacs-init-loader"
+++

## インストール

```
M-x auto-install-from-url <RET>
http://coderepos.org/share/browser/lang/elisp/init-loader/init-loader.el
```

## とりあえず.emacs.elをコピーし、00_init.elを作成

```
cp -a .emacs.el .emacs.d/conf/00_init.el
```

## .emacsの設定

今後は、.emacs.d/conf/以下に、設定ごとのファイルを設置していくので、  
.emacsには、読み込み先のみを設定する。  

``` scheme
;; load-path
(add-to-list 'load-path "~/.emacs.d/elisp")

(require 'init-loader)
(init-loader-load "~/.emacs.d/conf")
```

## ファイルを分割する

さきほど生成した00_init.elを設定ごとにファイルを分割していく。

```
tree
.
├── 00_init.el
├── 10_auto_install.el
├── 50_autocomplete.el
├── 50_perl.el
├── 60_flymake.el
├── 80_markdown.el
└── 80_serial_number.pl
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Perl Hacks on Emacs](http://typester.stfuawsc.com/slides/perlcasual2/start.html")
* [emacsでinit-loaderを導入してみた](http://shibayu36.hatenablog.com/entry/20101229/1293624201)
