+++
date = "2014-09-17T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs"]
title = "helm-gtags-modeを導入してソースコードの読解する"
slug = "to-reading-comprehension-of-the-source-code-by-introducing-the-helm-gtags-mode"
+++

`helm-gtags`を導入すると、メソッドの定義先に簡単に移動できたり(メソッドジャンプ)、
メソッドの参照元の一覧を表示できたりと大変便利です。

`helm-gtags`は`GNU GLOBAL(gtag)`というソフトウェアを使って上記の機能を実装しています。
ただ`GNU GLOBAL`は今まではC言語やJava、PHPなど一部言語しか対応していませんでした。

*GNU GLOBAL(gtag)とは、ソースファイルを解析し、インデックスファイルを生成してくれるソフトウェアです*

しかし、バージョン`6.3.2`から`global-pygments-plugin`が取り込まれたことで、Rubyなど多くの言語に対応しました。

今回は、RubyとPHPを`helm-gtags-mode`に対応するようにしました。

### ソフトウェアの依存関係

少しややこしいので整理しておきますと、

1. `helm-gtags(複数言語対応)`は `GNU GLOBAL 6.3.2`が必要
2. `GNU GLOBAL`に取り込まれた`global-pygments-plugin`は、`Pygment`が必要
3. `Pygment`は`Python 2.6以上`が必要

[helm-gtags] <- [GLOBAL] <- [Pygment] <- [Python 2系] という関係性なので、
まずは、Python 2系からインストールします。

*注意*
`Pygment`のコマンドは`pygmentize`で、初めからインストールされている可能性があります。
その場合は、`Python 2系`、`Pygment`のインストール手順はスルーしてください。

    $ which pygmentize
    $ /usr/bin/pygmentize

    $ pygmentize -V
    $ Pygments version 1.4, (c) 2006-2008 by Georg Brandl.

### Python 2系のインストール

`pyenv`を使って2系をインストールします。
`pyenv`のインストールは[こちら](http://blog.10rane.com/2014/08/19/install-anyenv/)を参考にしてください。

    $ pyenv install 2.7.8
    $ pyenv global 2.7.8
    $ pyenv version
    2.7.8

### Pygmentsのインストール

    $ pip install Pygments

    $ $HOME/.anyenv/envs/pyenv/versions/2.7.8/bin/pygmentize -V
    $ Pygments version 1.6, (c) 2006-2013 by Georg Brandl.

### GNU GLOBALのインストール

ダウンロード

    $ sudo bash
    # cd /usr/local/src
    # wget http://tamacom.com/global/global-6.3.2.tar.gz

インストール

    # tar xvzf global-6.3.2.tar.gz
    # cd global-6.3.2
    # ./configure
    # make
    # make install

　

    $ which global
    /usr/local/bin/global

    $ global --version
    global (GNU GLOBAL) 6.3.2

　

    $ which gtags
    /usr/local/bin/gtags

    $ gtags --version
    gtags (GNU GLOBAL) 6.3.2

rootでも使えるようリンクを貼っておきます

    $ sudo ln -s /usr/local/bin/gtags /usr/bin/gtags
    $ sudo ln -s /usr/local/bin/global /usr/bin/global

### global-pygments-pluginの設定

デフォルトのままだと、Rubyには対応していないので、設定ファイルをダウンロードします。

    $ wget https://raw.githubusercontent.com/yoshizow/global-pygments-plugin/master/sample.globalrc
    $ mv sample.globalrc $HOME/.globalrc

設定ファイルでは、拡張子`.tt`がTreetopというパーサ？に設定されていますが、Rubyを解析時にエラーがでたので、
とりあえずコメントアウトします。

$HOME/.globalrc

```
#:langmap=Treetop\:.treetop.tt:\　　　　　#:gtags_parser=Treetop\:/usr/local/lib/gtags/pygments-parser.la:\
```

### gtagsを使ってみる

とりあえず単体で`GNU GLOBAL(gtags)`が使えるようになっているはずなので、テストしてみます。
`GNU GLOBAL`はプロジェクトの直下にインデックスファイル(GPATH、GRTAGS、GTAGS)を生成することで、検索が可能になります。

    $ cd /tmp/gtags_test
    $ cat > hoge.php
    <?php
    class Hoge
    {
        public function hello()
        {
            print "hello\n";
        }
    }

　

    $ gtags --gtagslabel=pygments --debug
    $ tree
    .
    ├── GPATH
    ├── GRTAGS
    ├── GTAGS
    └── hoge.php

　

    $ global -sx hello hoge.php
    hello               4 hoge.php             public function hello()


### helm-gtagsのインストール

$HOME/.emacs.d/Cask

    (depends-on "helm-gtags")

$HOME/.emacs.d/init_loader/02_helm.el

```
(add-hook 'helm-gtags-mode-hook
'(lambda ()
;;入力されたタグの定義元へジャンプ
(local-set-key (kbd "M-t") 'helm-gtags-find-tag)

;;入力タグを参照する場所へジャンプ
(local-set-key (kbd "M-r") 'helm-gtags-find-rtag)  

;;入力したシンボルを参照する場所へジャンプ
(local-set-key (kbd "M-s") 'helm-gtags-find-symbol)

;;タグ一覧からタグを選択し, その定義元にジャンプする
(local-set-key (kbd "M-l") 'helm-gtags-select)

;;ジャンプ前の場所に戻る
(local-set-key (kbd "C-t") 'helm-gtags-pop-stack)))

(add-hook 'php-mode-hook 'helm-gtags-mode)
(add-hook 'ruby-mode-hook 'helm-gtags-mode)
```

#### helm-gtags-modeの使い方

先ほど`php-mode`と`ruby-mode`に`helm-gtags-mode`をホークしたので、Rubyファイルを開くと自動で`helm-gtags-mode`が起動します。

ただインデックスファイルがないと検索できないので、`helm-gtags-mode`のショートカットを実行すると
ミニバッファに`File GTAGS not found. Run 'gtags'? (yes or no)`というメッセージが出てくるので、インデックスファイルを作って下さい。

インデックスファイルが正常に作成されないと、エラーがミニバッファに出るので、その場合は一度Emacsから出て
`$ gtags --gtagslabel=pygments --debug`で確認してください。

すごく便利ですね！お疲れ様でした。

### 参考サイト

* [GNU GLOBALへのPygmentsパーサー取り込みでソースコード読みが信じられないくらいに捗るはず](http://qiita.com/5t111111/items/c14ac68f762ce71a7760)
* [GNU GLOBALとvimで巨大なコードでも快適にコードリーディング](http://blog.matsumoto-r.jp/?p=2369)
* [Pygments を利用して GNU GLOBAL の対応言語を大幅に増やす](http://www.turtlewalk.org/blog/2014/03/09/global-pygments-plugin/)
* [yoshizow/global-pygments-plugin](https://github.com/yoshizow/global-pygments-plugin)
* [EmacsJP:helm-gtags](http://emacs-jp.github.io/packages/helm/helm-gtags.html)
* [PHPで GNU globalを使う](http://d.hatena.ne.jp/syohex/20121025/1351175067)

### 関連する記事

* [anyenvをインストールする](http://blog.10rane.com/2014/08/19/install-anyenv/)
* [HelmをCaskでインストールする](http://blog.10rane.com/2014/08/28/how-to-install-and-setup-helm-in-cask/)
* [Caskをインストールする](http://blog.10rane.com/2014/08/20/install-cask/)
