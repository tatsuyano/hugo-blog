+++
date = "2015-08-13T14:40:00+09:00"
draft = false
image = "/img/taiwan-bg.jpg"
tags = ["misc"]
title = "台湾に旅行に行ってきた"
slug = "i-went-to-travel-to-taiwan"
+++

7月末に3泊4日で台湾旅行に行ってきた。
海外旅行は10年ぶりぐらいだったので、ツアーにするか迷ったけど、もういい大人なので、自由旅行で台北を中心に淡水、九份などを回ってきた。

## 台北

ガイド本などを見ると、台湾は親日で日本語も結構通じるみたいな事が書いてあったが、それは観光地など特定のポイントの話で、
それ意外は普通に日本語は通じなかった(当たり前か)。なのでチケットを買うにも四苦八苦したが、なんとかホテル(王朝大酒店)に到着。

写真では伝わらないかもしれないが、今まで一番大きなホテルでテンションがかなり上がった。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_01.jpg)</center><p>
<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_02.jpg)</center><p>
<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_03.jpg)</center><p>

また平日ということもあり原チャの数が凄かった。日本の感覚で歩いていると事故りそうなので、注意が必要。

### 台北101

初日は雨が降っていたので、遠出はせずに先に台北101で買い物をすることにした。
高さ509メートルという世界第二位の高さは圧巻だった。大きな建物というだけで、十分観光名所として成立するのだなあとなんか関心した。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_04.jpg)</center><p>
<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_05.jpg)</center><p>

展望台から見た夜景。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_06.jpg)</center><br>

### 臨江街観光夜市

台北101の近くの余市。「臭豆腐」という台湾のソウルフードの匂いがなかなかきつい。
旅行前は余市で臭豆腐を食べるつもりだったが、ちょっときつそうだったので断念。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_07.jpg)</center><p>
<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_08.jpg)</center><br>

### 龍山寺

有名なパワースポット？らしいお寺。参拝のルールが解らず、周りのやり方を眺めていたら、親切なおせっかいおばさんがやり方を教えてくれた。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_09.jpg)</center><br>

### 中正紀念堂

とにかく広い。平日で人も少なかったこともあり、なおさら感じたのかもしれない。
広場で遠くを眺めていると、以前行った中国を思い出した。やはり台湾も中国なのだなあと改めて感じた。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_10.jpg)</center><p>

この写真はgoogleフォトが自動で写真を連結してパノラマ化した写真。このクオリティならもう広角カメラは必要ないかもしれない。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_11.jpg" width="100%"><br>

## 淡水

台北からMRT(電車)で約一時間くらい。日本で言うところの横浜っぽいところ。夜景が綺麗ということで18時くらいまで粘ったがなかなか日は落ちず。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_12.jpg)</center><br>

## 故宮博物院

主に陶器や、翡翠などの美術品を中心に展示している博物館。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_13.jpg)</center><br>

## 九份

今回一番楽しみにしていた九份。元々炭鉱の町ということで、山の中の一部分に町がある感じ。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_14.jpg)</center><p>
<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_15.jpg)</center><p>

日が暮れてくると、幻想的な雰囲気になってくる。

<center>![](https://dl.dropboxusercontent.com/u/159938/blog_images/i-went-to-travel-to-taiwan_16.jpg)</center><br>

## 感想

今回は台北を中心にひと通り回れてとても楽しかった。
初めは久々の海外旅行ということもあり、言葉が通じず苦労したが、2日目からは電車移動も覚え、かなり自由な旅行を満喫できた。

ただ英語がもう少しまともに聞き取れれば(話せれば)、もっと楽しかったなあと強く思った。
次回海外に行く時までに、もう少し英語を勉強しようと思う(小並感)。
