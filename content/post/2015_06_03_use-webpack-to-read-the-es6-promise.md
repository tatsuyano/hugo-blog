+++
date = "2015-06-03T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["nodejs", "es6", "webpack", "js"]
title = "webpackを使ってes6-promiseを読み込む"
slug = "use-webpack-to-read-the-es6-promise"
+++

最近勉強中のPromiseですが、現段階(15/06/03)ではIE11に対応していません。
PromiseをIEに対応させるためには、es6-promiseというNode.jsのライブラリをフロントエンドで読み込む必要があります。

しかしNode.jsのライブラリは、そのままではフロントエンドで使えないので、
webpackでフロントエンド向けにBuildする必要があります。

*-webpackは他にも多くの機能がありますが、ここではNode.jsのモジュールをフロントエンドで使うため方法のみにフォーカスします。-*

### webpackを使うための準備

まずは本体のインストール。
<pre><code class="language-bash">$ npm install webpack -g
</code></pre>

次に適当なディレクトリを作成します。
<pre><code class="language-bash">$ mkdir webpack-sample && cd webpack-sample
</code></pre>

今回使うライブラリ(es6-promise)をディレクトリにインストールします。
<pre><code class="language-bash">$ npm install es6-promise
</code></pre>

ライブラリはglobalではなく、ディレクトリ直下(/webpack-sample/node_modules)にインストールしてください。

### JsのBuild方法

次に(Node.jsの)ライブラリを読み込むJs(main.js)をビルドします。

<pre><code class="language-bash">$ webpack main.js bundle.js
Hash: de7b4a8a87c6e52d026f
Version: webpack 1.9.10
Time: 109ms
    Asset     Size  Chunks             Chunk Names
bundle.js  40.1 kB       0  [emitted]  main
   [0] ./main.js 255 bytes {0} [built]
   + 6 hidden modules
</code></pre>
<br>

BuildしたJs(bundle.js)は、main.jsが依存しているライブラリ(es6-promise等)も含んだ一つのファイルに圧縮されます。
このJsのみをHTMLに読み込むだけで、Node.jsが利用できるようになります。

<pre><code class="language-bash">&lt;script&nbsp;src=&quot;bundle.js&quot;&gt;&lt;/script&gt;
</code></pre>
<br>

<b>main.js</b>
<pre><code class="language-javascript">'use strict'
var Promise = require('es6-promise').Promise;

var promise = function(code) {
    return new Promise(function (resolve,reject) {
        resolve('Hello Promise !!');
    });
}

promise().then(function (result) {
    console.log(result);
});
</code></pre>
<br>

### 確認

今回は、node.jsのhttp-serverを使って確認しようと思います。

* [Node.jsのhttp-serverっていうコマンドラインのウェブサーバーが便利](https://firegoby.jp/archives/5706)

<pre><code class="language-bash">$ npm install -g http-server
$ http-server -p 4000
</code></pre>

サーバー起動し、IEのconsoleにメッセージが表示されれば、確認OKです。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/use-webpack-to-read-the-es6-promise_01.jpg)

### 参考サイト

* [webpack](http://webpack.github.io/)
* [es6-promise](https://github.com/jakearchibald/es6-promise)
* [RequireJS等はもう古い。WebPackとは？](http://ameblo.jp/ca-1pixel/entry-11884453208.html)
* [Webpackを使い倒す](http://thujikun.github.io/blog/2014/12/07/webpack/)
* [ReactとStylusをwebpackで使うための開発環境構築](http://www.twopipe.com/front-end/2014/12/22/react-webpack-env.html)

### 関連する記事

* [Promiseを使って、非同期処理を任意のタイミングで実行する](/2015/06/02/use-the-promise-to-perform-the-async-processing-at-anytime/)
