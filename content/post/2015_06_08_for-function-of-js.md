+++
date = "2015-06-08T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["js"]
title = "Jsの関数について"
slug = "for-function-of-js"
+++

Jsには主に「Function文」と「Function式(関数リテラル)」という２つの関数の宣言方法がある。
※本当は「Functionコンストラクタ」という方法もあるが、あまり実用的ではないので省略。

<pre><code class='language-javascript'>// Function文
function func () {
  console.log('statements');
}

// Function式
var func = function func() {
  console.log('expression');
};

//関数の呼び出し方法は同じ
func();
</code></pre>
<br>

違いは評価のタイミング。「Function文」は関数が静的な構造として宣言される。つまり、Jsの実行環境がコードを解析するタイミングで関数が静的に登録され、これにより「関数をどこで定義していても呼び出せる」ようになります。


<pre><code class='language-javascript'>func(); // => 宣言前に読み出してもエラーにならない

function func () {
  console.log('statements');
}
</code></pre>
<br>

逆に「Function式」は、変数に代入された段階で評価される。そのため、宣言後に呼び出さないとエラーになる。

<pre><code class='language-javascript'>func(); // => TypeError: undefined is not a function

var func = function func () {
  console.log('expression');
}
</code></pre>
<br>

## 無名関数

「無名関数」とは、Function式の関数名を省略した形
。Function式を使う場合は、主に無名関数が使われる。

<pre><code class='language-javascript'>// 無名関数
var func = function () {
  console.log('expression');
};

func();
</code></pre>
<br>

## 即時関数

「即時関数」とは、関数を()で囲んだ状態で実行することで、関数宣言と同時にその関数を実行する書き方です。

<pre><code class='language-javascript'>// 即時関数
(function () {
  console.log('Hello');
})();

// ()();の代わりに、().call(this);とも書ける
(function () {
  console.log('Hello');
}).call(this);

// 即時関数に引数を渡す場合
(function (val,val2) {
  console.log(val + ' ' + val2);
})('Hello','World');
</code></pre>

即時関数を使うことで、関数内で宣言した変数のスコープは、関数内でしか使えないようになる。
つまり、ブロックスコープと同じ効果となる。

## まとめ

* 変数宣言は無名関数を使う
* 即時関数を使えば、ブロックスコープと同じ効果が得られる

## 参考サイト

* [現代プログラマの必須知識、JavaScript](http://itpro.nikkeibp.co.jp/atcl/column/14/091700069/091700001/)
* [(function(){})() と function(){}()](http://d.hatena.ne.jp/amachang/20080208/1202448348)

## 関連記事

* [Jsのスコープについて](/2015/06/08/for-the-scope-of-the-js/)
