+++
date = "2014-08-06T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["zsh", "mac"]
title = "oh-my-zshをインストールする"
slug = "install-oh-my-zsh"
+++

今まで設定が面倒で試していなかった[oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)をインストールしてみました。下記の方法で、mac、centosともに対応可能です。

### oh-my-zshをインストールする

すでにzshはインストールされているものとします。  
また、今ある`$HOME/.zshrc`はインストール時に上書きされてしまうので事前に退避してください。  
インストールするとHOME直下に`.oh-my-zsh`というディレクトリが作成され`.zshrc`が上書きされます。

インストール

    curl -L http://install.ohmyz.sh | sh

インストールが終わったので、いったん`sorce .zshrc`して、oh-my-zshを使ってみます。promptが変更されたはずです。

oh-my-zshは、themeとplugin(複数選択可)という単位でzshを管理しています。
初期設定はthemeが`robbyrussell`、pluginが`git`です。

oh-my-zshのディレクトリ構成

    .
    ├── custom      ... 自分が設定したzshrcを入れる
    │   └── plugins ... 自分で作ったpluginを入れる
    ├── lib
    ├── log         ... zsh_historyなどを入れる
    ├── plugins     ... gitなどソフトウェア単位の設定ファイルが入っている
    ├── templates   ... zshrcのテンプレートファイルが入っている
    ├── themes      ... promptなど見た目の設定ファイルが入っている
    └── tools

#### customに自分の設定ファイルを入れる

整理されていませんが、とりあえず`custom`ディレクトリに入れてみます。  
因みに拡張子は`.zsh`で、ファイル名は自由です。

main.zsh

    export LC_ALL=en_US.UTF-8
    export LANG=ja_JP.UTF-8
    export EDITOR=emacs

    bindkey -e                        # emacsライクなキーバインド
    autoload -U compinit              # 強力な補完機能
    compinit -u                       # このあたりを使わないとzsh使ってる意味なし

    setopt autopushd                  # cdの履歴を表示
    setopt pushd_ignore_dups          # 同ディレクトリを履歴に追加しない
    setopt auto_cd                    # 自動的にディレクトリ移動

    ## 履歴
    HISTFILE=~/.oh-my-zsh/log/.zsh_history
    HISTSIZE=10000                    # ファイルサイズ
    SAVEHIST=10000                    # saveする量
    setopt hist_ignore_dups           # 重複を記録しない
    setopt hist_reduce_blanks         # スペース排除
    setopt share_history              # 履歴ファイルを共有
    setopt EXTENDED_HISTORY           # zshの開始終了を記録

    export PATH=/usr/local/bin:$PATH
    export PATH=$PATH:$HOME/Dropbox/dotfile/mybin

    ## zsh-syntax-highlighting
    source $HOME/Dropbox/dotfile/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

    ## anyenv
    export PATH="$HOME/.anyenv/bin:$PATH"
    eval "$(anyenv init - zsh)"

    ## z.sh
    _Z_CMD=j
    source $HOME/Dropbox/dotfile/z/z.sh
    precmd() { _z --add "$(pwd -P)" }

    ## peco(for oh-my-zsh)
    function peco-select-history() {
        local tac
        if which tac > /dev/null; then
            tac="tac"
        else
            tac="tail -r"
        fi
        BUFFER=$(\history -n 1 | \
            eval $tac | \
            peco --query "$LBUFFER")
        CURSOR=$#BUFFER
        zle clear-screen
    }
    zle -N peco-select-history
    bindkey '^r' peco-select-history
    
    ## alias
    alias mysql="mysql --pager='less -S -n -i -F -X'"
    function tmr(){ tmux new -s $1 || tmux attach -d -t $1; }

### 文字化けへの対処法

どうもoh-my-zshがlocateをブランクに設定してしまうようなので、
適当な箇所(私は`/custom/main.zsh`)に、以下の設定を追加しました。

    export LC_ALL=en_US.UTF-8

* [oh-my-zsh の環境で、peco-select-history が動かない](http://qiita.com/uchiko/items/f6b1528d7362c9310da0)


### pecoが動かなくなる問題への対処法

以下のように変更することで対応しました。

変更前

    ## peco
    for f (~/Dropbox/dotfile/peco_sources/*) source "${f}" # load peco sources
    bindkey '^r' peco-select-history

変更後

    ## peco(for oh-my-zsh)
    function peco-select-history() {
        local tac
        if which tac > /dev/null; then
            tac="tac"
        else
            tac="tail -r"
        fi
        BUFFER=$(\history -n 1 | \
            eval $tac | \
            peco --query "$LBUFFER")
        CURSOR=$#BUFFER
        zle clear-screen
    }
    zle -N peco-select-history
    bindkey '^r' peco-select-history

* [oh-my-zshの言語問題](http://qiita.com/huydx@github/items/a045d33ec23350c730af)


### themeの変更

oh-my-zshには複数の[theme](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes)が用意されています。
今回は[agnoster](https://gist.github.com/agnoster/3712874)というthemeを使ってみます。

.zshrc

    ##ZSH_THEME="robbyrussell"
    ZSH_THEME="agnoster"

`agnoster`はデフォルトでは入っていないので、別途インストールします。

    cd .oh-my-zsh/themes/
    wget https://gist.githubusercontent.com/agnoster/3712874/raw/c3107c06c04fb42b0ca27b0a81b15854819969c6/agnoster.zsh-theme

promptの見た目を少しだけ変更しました。

agnoster.zsh-theme

    ##prompt_segment black default "%(!.%{%F{yellow}%}.)$user@%m"
    prompt_segment black default "%(!.%{%F{yellow}%}.)$user"

    #prompt_segment blue black '%~'
    prompt_segment blue black '%1~'

変更したので`source $HOME/.zshrc`を忘れずに実行します。

#### macにpowerlineフォントをインストールする

`agnoster`は、powerline系のフォントを使うことを前提としています。

まずは`/Library/Fonts/`以下に、
[Inconsolata-dz-Powerline.otf](https://gist.github.com/qrush/1595572)ページにある`Inconsolata-dz-Powerline.otf`、`Menlo-Powerline.otf`、`mensch-Powerline.otf`をダウンロードします。

ダウンロード後、iTerm2のフォントを変更し、再起動させます。
ここまで、ひと通りの設定が完了しました。  

お疲れ様でした。

#### 関連する記事
* [Ricky for powerlineをmacにインストールする](http://blog.10rane.com/2014/08/06/install-ricky-powerline-for-mac/)
