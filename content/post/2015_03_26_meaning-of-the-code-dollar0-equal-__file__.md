+++
date = "2015-03-26T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby"]
title = "if $0 == __FILE__のコードの意味"
slug = "meaning-of-the-code-dollar0-equal-__file__"
+++

最近[こちらの記事](http://yugui.jp/articles/885)を読んでいて、
以下のコードの意味が解らなかったので調べてみました。

<pre><code class='language-ruby'>if $0 == __FILE__
  # 何らかの処理
end
</code></pre>
<br>

まず結論から言うと「requireされた時は実行したくないけど、スクリプトファイルを直接実行した時は処理したい」
場合に使います。ややこしいですね。  
主には簡易なテストコードやサンプルコードを書く場合に使われるようです。

ただ、なぜ上記のコードがそのような動きになるのか解らなかったのでもう少し調べてみました。

* `__FILE__` ... 実行中のファイル名(相対パス)を返します。フルパスが必要な場合は File.expand_path(\_\_FILE\_\_) とする必要があります。
* `$0`       ... 直接実行したファイル名(相対パス)を返します。

[こちらのコード](http://qiita.com/naoty_k/items/f84b2a9034a3bb3bfcb2)とほぼ同じですが、

<pre><code class='language-ruby'># target_file.rb
puts "__FILE__ =>  #{__FILE__}"
puts "$0       =>  #{$0}"

if $0 == __FILE__
  puts "Sample codeを実行"
else
  puts "Sample codeは実行されない"
end
</code></pre>

target_file.rb自体を直接実行すると、
`$0`と`__FILE__`に同じ値が入ることになるので、Sample codeが実行されます。

<pre><code class='language-bash'>$ ruby target_file.rb
__FILE__ =>  filename.rb
$0       =>  filename.rb
Sample codeを実行
</code></pre>
<br>

しかし、target_file.rbをライブラリとして使う(requireする)場合などは、`$0` と `__FILE__` の値が異なるので、Sample codeは実行されません。

<pre><code class='language-ruby'># require_file.rb
require_relative 'target_file'
</code></pre>

※ `require_relative`と`require`の違いについては別記事で説明します。

<pre><code class='language-bash'>$ ruby require_file.rb
__FILE__ =>  /tmp/target_file.rb
$0       =>  require_file.rb
Sample codeを実行しない
</code></pre>
<br>

このようにライブラリなどを作る場合、`if $0 == __FILE__`の分岐を入れることで
簡単にサンプルコードを記述することができます。

## 参考サイト
* [__FILE__と$0の違い](http://qiita.com/naoty_k/items/f84b2a9034a3bb3bfcb2)
* [Ruby/スクリプトファイル自身が実行された時のみ実行する方法](http://tobysoft.net/wiki/index.php?Ruby%2F%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A5%D5%A5%A1%A5%A4%A5%EB%BC%AB%BF%C8%A4%AC%BC%C2%B9%D4%A4%B5%A4%EC%A4%BF%BB%FE%A4%CE%A4%DF%BC%C2%B9%D4%A4%B9%A4%EB%CA%FD%CB%A1)
* [Ruby 2.0.0 リファレンスマニュアル > 変数と定数](http://docs.ruby-lang.org/ja/2.0.0/doc/spec=2fvariables.html#pseudo)
