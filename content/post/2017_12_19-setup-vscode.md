
* [VS Code の便利なショートカットキー](https://qiita.com/12345/items/64f4372fbca041e949d0)
* [[Mac版] VisualStudioCode キーボードショートカット](https://qiita.com/naru0504/items/99495c4482cd158ddca8)

* [Visual Studio Code で Emacs キーバインドを使う ](http://tarukosu.hatenablog.com/entry/VisualStudioCodeEmacs)

* [拡張子を言語モードに関連付ける方法](https://pg.kdtk.net/1336)

## typescript
* [Visual Studio Code で TypeScript をはじめよう](https://r2.ag/vscode-typescript/)
* [VSCodeでTypeScriptの開発環境を整える](https://qiita.com/koara-local/items/1db82e3bfb3064c41862)
* [Visual Studio CodeではじめるTypeScript入門](http://www.casleyconsulting.co.jp/blog-engineer/javascript/visual-studio-code%E3%81%A7%E3%81%AF%E3%81%98%E3%82%81%E3%82%8Btypescript%E5%85%A5%E9%96%80/)
* [Visual Studio CodeでTypeScript開発のノウハウ](https://qiita.com/takao_mofumofu/items/4f8d365846e2f770bd6e)

## モジュールシステム
* [JavaScriptのモジュールシステムに関するまとめ](https://qiita.com/kami_zh/items/f093c34906d14ba65ad3)


