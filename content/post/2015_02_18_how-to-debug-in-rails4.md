+++
date = "2015-02-18T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "Rails4でのデバッグ方法"
slug = "how-to-debug-in-rails4"
+++

最近覚えたデバッグ方法を、備忘のためにまとめておきます。

## ログに出力する

ActiveSupport::Logger#debugメソッドを使って、rails serverの標準出力にデバッグコードを出力する。

<pre><code class="language-ruby">logger.debug('Hello world')
</pre></code>
<br>

### SQLを出力する

ActiveRecord::Relation#to_sqlメソッドでSQLを出力する。

<pre><code class="language-ruby">User.all.to_sql #=> SELECT "users".* FROM "users"
</pre></code>
<br>

## ブレークポイントを設定する

コード内に`binding.pry`というコードを書くと、そこまで実行中のプログラムが中断され、
以降をコンソール上からデバッグできます。

Gemfile
<pre><code class="language-ruby">gem 'pry-rails',  group: [:development, :test]
gem 'pry-byebug', group: [:development, :test]
</pre></code>
<br>

<pre><code class="language-ruby">$ bundle install
$ rails server
</pre></code>
<br>

中断したい箇所に`binding.pry`を記述。

<pre><code class="language-ruby">def index
  @users = User.all

  binding.pry # <- This!

  logger.debug('------ 1 -------')
  logger.debug('------ 2 -------')
  logger.debug('------ 3 -------')
  logger.debug('------ 4 -------')
end
</pre></code>
<br>

ブレークポイントの移動は`next`,`step`,`exit`で行う。

<pre><code class="language-ruby">     7: def index
     8:   @users = User.all
     9:
    10:   binding.pry
    11:
 => 12:   logger.debug('------ 1 -------')
    13:   logger.debug('------ 2 -------')
    14:   logger.debug('------ 3 -------')
    15:   logger.debug('------ 4 -------')
    16: end
</pre></code>
<br>

### おまけ1 出力結果を見やすく整形する

* pコマンド

<pre><code class="language-ruby">p Array.new(10) { {:hoge => :fuga} }
#=> [{:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga}, {:hoge=>:fuga},
{:hoge=>:fuga}, {:hoge=>:fuga}]
</pre></code>
<br>

* ppコマンド

<pre><code class="language-ruby">require 'pp'
pp Array.new(10) { {:hoge => :fuga} }
#=> [{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga},
{:hoge=>:fuga}]
</pre></code>
<br>

### おまけ2 オブジェクトの情報を出力する

* Object#inspectメソッドでオブジェクトを文字列として出力する。

<pre><code class="language-ruby">User.inspect
</pre></code>
<br>

* オブジェクトのメソッドを出力する

<pre><code class="language-ruby">User.methods
</pre></code>
<br>

* オブジェクト(クラス)の継承関係を出力する

<pre><code class="language-ruby">User.ancestors
</pre></code>
<br>

## 参考サイト& 書籍

* [パーフェクトRuby on Rails](http://www.amazon.co.jp/dp/B00P0UR1RU/)
* [Ruby で debug する7つの方法](http://secondlife.hatenablog.jp/entry/20061010/1160453355)
* [Rubyでのデバッグテクニック](http://www.matzmtok.com/blog/?p=119)
