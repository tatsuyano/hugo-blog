+++
date = "2013-02-04T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "mysql", "mac"]
title = "MAMP環境でのCakePHPのmysql.sockの指定方法"
slug = "mamp-cakephp-mysql"
+++

## worning

MacにMAMPをインストールし、CakePHPの「bakeコンソール」でModelクラスを作成したさいに、mysql.sockの指定箇所が違うよと警告された  

```
Warning Error: PDO::__construct(): [2002] No such file or directory (trying to connect via unix:///var/mysql/mysql.sock) in [/Applications/MAMP/htdocs/bake/lib/Cake/Model/Datasource/Database/Mysql.php, line 149]

Error: Database connection "SQLSTATE[HY000] [2002] No such file or directory" is missing, or could not be created.
#0 /Applications/MAMP/htdocs/bake/lib/Cake/Model/Datasource/DboSource.php(260): Mysql->connect()
```

MAMPを使っているので、mysql.sockは「/Applications/MAMP/tmp/mysql/mysql.sock」作成されている。  
たぶん解決方法としては３つあって、  
1.sockファイルをworiningに書いてある「/var/mysql/mysql.sock」に生成するようMysql側の設定をいじる。  
2.「/var/mysql/mysql.sock」に「/Applications/MAMP/tmp/mysql/mysql.sock」のリンクを貼ってしまう。  
3.たぶん「app/Config/database.php」に現在生成されているmysql.sockの場所を指定する。  

どう考えても「３」が正しいやり方だと思うけど、なかなかそれっぽい情報が引っかからなかったがやっとあった。  

[Cakephp console (bake) on Mac OS with Mamp : database connection error](http://stackoverflow.com/questions/1688705/cakephp-console-bake-on-mac-os-with-mamp-database-connection-error)

<pre><code class="language-php">
public $default = array(
    'datasource' => 'Database/Mysql',
    'persistent' => false,
    'host' => 'localhost',
    'login' => 'USERNAME',
    'password' => 'PASSWORD',
    'database' => 'DATABASE',
    'encoding' => 'utf8',
    'unix_socket' => '/Applications/MAMP/tmp/mysql/mysql.sock' # <- this
    );
</code></pre>

2.x系から、socketの指定がデフォルトと違う場合に上記のように設定する模様。
