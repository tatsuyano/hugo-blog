+++
date = "2013-02-10T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["git"]
title = "ファイルを指定したコミットの状態に戻す(git checkout)"
slug = "git-checkout"
+++

指定したファイルの、過去のコミット一覧を表示する

```
git log --oneline Controller/TasksController.php
a8eb2ff task add,edit,delete
995bfc9 To move the program
4b5e3dd check score today
187cf30 add Tasks
```

現在の状態と、過去のコミットを比較する

```
git diff 995bfc9 Controller/TasksController.php
diff --git a/Controller/TasksController.php b/Controller/TasksController.php
index e6ab27a..9dc9b45 100644
--- a/Controller/TasksController.php
+++ b/Controller/TasksController.php
@@ -6,6 +6,7 @@ App::uses('AppController', 'Controller');
  * @property Task $Task
  */
  class TasksController extends AppController {
+    public $uses = array('Task','User');

/**
 * index method
@@ -37,12 +38,19 @@ class TasksController extends AppController {
     *
     * @return void
     */
-    public function add() {
+    public function add($user_id = null) {
+      $this->User->id = $user_id;
+      if (!$this->User->exists()){
```

戻したいコミットを確認したらcheckout

```
git checkout 995bfc9 Controller/TasksController.php
```
