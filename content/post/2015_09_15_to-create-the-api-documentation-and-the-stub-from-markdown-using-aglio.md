+++
date = "2015-09-15T15:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["nodejs","js","gulp"]
title = "aglioを使ってmarkdownからAPIドキュメントとStubを作成する"
slug = "to-create-the-api-documentation-and-the-stub-from-markdown-using-aglio"
+++

仕事で簡単なAPIを実装する必要があったので、簡単にAPIドキュメントを公開したり、ドキュメントからStubを自動で用意する方法を調べてみた。

で、[こちらの記事](http://dackdive.hateblo.jp/entry/2015/08/07/181723)にやりたいことがまんま載っていた(ありがとうございます！)ので、
記事を参考に[自分用のテンプレート](https://github.com/tatsuyano/aglio-template)を用意した。

<br>
### Blueprint

Blueprintの記述は下記をページを参照。

* [API Blueprint Tutorial](https://github.com/apiaryio/api-blueprint/blob/master/Tutorial.md)
* [API Blueprint examples](https://github.com/apiaryio/api-blueprint/tree/master/examples)

<br>
### api-mockの起動をgulpに設定する

以下のように設定し、`$ gulp mock`

    var gulp    = require('gulp'),
        aglio   = require('gulp-aglio'),
        ApiMock = require('api-mock');
    
    var TEMPLATE_FILES = ['apidocs/*.md'],
    LAYOUT_FILE    = 'apidocs/layout.md',
    PUBLISHED_DIR  = 'published';
    
    gulp.task('api-mock', function () {
      var mockServer = new ApiMock({
        blueprintPath: PUBLISHED_DIR + '/index.md',
        options: {
          port: 3000
        }
      });
      mockServer.run();
    });
    
    gulp.task('mock', ['api-mock']);

<br>
### Vagrantで使う場合、ポートを開ける

vagrant上からaglioとapi-mockを使うには、デフォルトだと`8088`と`3000`を開ける必要があるので、`Vagrant`ファイルに以下の設定を加える。

    config.vm.network "forwarded_port", guest: 3000, host: 3000
    config.vm.network "forwarded_port", guest: 8088, host: 8088

* APIドキュメントを閲覧　 => `http://localhost:8088`
* StubでAPIのテストをする => `http://localhost:3000/questions`

<br>
### POSTでJSONのテストをする

GETの場合は、ブラウザからそのままAPIをそのまま叩けばいいが、POSTの場合は curl を使う。

    curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST \
    -d '{"name": "hoge" }' \
    http://localhost:3000/user

もしくはChromeアプリの[Advanced REST client](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo/related)などを使ってテストする。


<br>
### 参考サイト

* [APIドキュメント作成ツールならApiary.ioよりaglioの方が良かった](http://dackdive.hateblo.jp/entry/2015/08/07/181723)
* [GitHub: aglio-template](https://github.com/tatsuyano/aglio-template)
* [API Blueprint Tutorial](https://github.com/apiaryio/api-blueprint/blob/master/Tutorial.md)
* [API Blueprint examples](https://github.com/apiaryio/api-blueprint/tree/master/examples)
* [curl を使って JSONデータをPOSTする](http://takuya71.hatenablog.com/entry/2012/11/10/143415)
* [Advanced REST client](https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo/related)
