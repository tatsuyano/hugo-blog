+++
date = "2014-07-28T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "PHP5.4から error_reportingのE_ALLにE_RISTRICTが加わった"
slug = "e_ristrict-is-added-to-the-e_all"
+++

fuelphp案件でcomposer経由でxml_rpc2をインストールしたら、以下のようなエラーが出た

    Uncaught exception Fuel\Core\PhpErrorException: Non-static method PEAR::loadExtension() should not be called statically

最初はソースの依存関係が原因かなあと思ったが、調べてみるとそうではなく
PHP5.4から `error_reportingのE_ALL`に`E_RISTRICT`が加わったことで、エラーと判定されるようになった

* [php5.4からはerror_reportingのE_ALLにE_RISTRICTが含まれる](http://life.co-hey.com/2013/04/php5-4%E3%81%8B%E3%82%89%E3%81%AFerror_reporting%E3%81%AEe_all%E3%81%ABe_ristrict%E3%81%8C%E5%90%AB%E3%81%BE%E3%82%8C%E3%82%8B/)
* [PHPでHTML上にエラー表示/非表示する php.ini 設定](http://blog.thingslabo.com/archives/000184.html)

#### 対象箇所でのみerror_reportingを止めることにした

production環境以外で、かつ一部分でのみ`error_reporting`を止めたかったので、対象箇所の前後で一時的に解除した

    $before_level = error_reporting(0); // error_report解除

    $options = array('prefix' => 'package.');
    $client = \XML_RPC2_Client::create('http://pear.php.net/xmlrpc.php', $options);
    $result = $client->info('XML_RPC2');
    print_r($result);

    error_reporting($before_level); // error_reportを元に戻す

* [error_reporting](http://php.net/manual/ja/function.error-reporting.php)
* [定義済み定数](http://www.php.net/manual/ja/errorfunc.constants.php)
