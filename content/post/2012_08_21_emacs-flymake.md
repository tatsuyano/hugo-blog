+++
date = "2012-08-21T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "perl"]
title = "flymakeを設定し、perlのシンタックスチェックをする"
slug = "emacs-flymake"
+++

## はじめに

perlのシンタックスチェックをflymakeで行うには、事前に「set-perl5lib.el」をインストールする必要があります。  

## set-perl5lib.elのインストール

```
cd .emacs.d/elisp/
wget http://svn.coderepos.org/share/lang/elisp/set-perl5lib/set-perl5lib.el
```

## .emacsの設定

``` scheme
;; flymake for perl
(require 'flymake)
(require 'set-perl5lib) ;; http://svn.coderepos.org/share/lang/elisp/set-perl5lib/set-perl5lib.el

(defvar flymake-perl-err-line-patterns '(("\\(.*\\) at \\([^ \n]+\\) line \\([0-9]+\\)[,.\n]" 2 3 nil 1)))
(defconst flymake-allowed-perl-file-name-masks '(("\\.pl$" flymake-perl-init)
                                                ("\\.pm$" flymake-perl-init)
                                                ("\\.t$" flymake-perl-init)
                                                ))

(defun flymake-perl-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         (local-file (file-relative-name
                      temp-file
                      (file-name-directory buffer-file-name))))
  (list "perl" (list "-wc" local-file))))

(defun flymake-perl-load ()
  (interactive)
  (set-perl5lib)
  (defadvice flymake-post-syntax-check (before flymake-force-check-was-interrupted)
    (setq flymake-check-was-interrupted t))
  (ad-activate 'flymake-post-syntax-check)
  (setq flymake-allowed-file-name-masks (append flymake-allowed-file-name-masks flymake-allowed-perl-file-name-masks))
  (setq flymake-err-line-patterns flymake-perl-err-line-patterns)
  (flymake-mode t))

(add-hook 'cperl-mode-hook '(lambda () (flymake-perl-load)))

(defun next-flymake-error ()
  (interactive)
  (flymake-goto-next-error)
  (let ((err (get-char-property (point) 'help-echo)))
    (when err
      (message err))))
(global-set-key "\M-e" 'next-flymake-error)
```

## 実行画面

![](https://dl.dropboxusercontent.com/u/159938/blog_images/flymark001.png)

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Perl Hacks on Emacs](http://typester.stfuawsc.com/slides/perlcasual2/start.html")
* [Perl開発環境としてのEmacsの設定](http://tech.lampetty.net/tech/index.php/archives/384")
