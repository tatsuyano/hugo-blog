+++
date = "2015-04-08T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "rails_configで定数を管理する"
slug = "manage-constant-in-rails_config"
+++

環境ごとに定数を管理したい時は、rails-config を使うのが便利。

## インストール

Gemfileに下記を追加し`bundle install`

<pre><code class="language-ruby"># Gemfile
gem 'rails_config'
</code></pre>

`bundle install`後、rails_config の初期設定を実行

<pre><code class="language-bash">$ rails g rails_config:install
      create  config/initializers/rails_config.rb
      create  config/settings.yml
      create  config/settings.local.yml
      create  config/settings
      create  config/settings/development.yml
      create  config/settings/production.yml
      create  config/settings/test.yml
      append  .gitignore
</code></pre>
<br>

## 環境ごとに読み込まれるファイル

<table border=1>
  <tr>
    <th>環境(env)</th>
    <th>ファイル名</th>
  </tr>
  <tr>
    <td>共通</td>
    <td>config/settings.yml</td>
  </tr>
  <tr>
    <td>(ローカルの)開発環境</td>
    <td>config/settings.local.yml</td>
  </tr>
  <tr>
    <td>開発環境</td>
    <td>config/settings/development.yml</td>
  </tr>
  <tr>
    <td>テスト環境</td>
    <td>config/settings/test.yml</td>
  </tr>
  <tr>
    <td>本番環境</td>
    <td>config/settings/production.yml</td>
  </tr>
</table>

開発環境で同じ定数を読み込む場合、優先度は下記の順番でなります。

1. `config/settings.local.yml`
2. `config/settings/development.yml`
3. `config/settings.yml`

ちなみにこの`config/settings.local.yml`は、gitignore の対象ファイルです。  
※ `rails g rails_config:install`実行時に追加されます。

<pre><code class="language-ruby">config/settings.local.yml
config/settings/*.local.yml
config/environments/*.local.yml
</code></pre>
<br>

## 定数の出力

例えば`config/settings.local.yml`を、以下のように設定した場合

<pre><code class="language-ruby">site:
  url: 'http://hoge.jp'
  desc: 'hogehoge'
</code></pre>

View側の出力は以下のようになります。

<pre><code class="language-ruby"><%= Settings.site.desc %> # hogehoge
</code></pre>
ちなみにプリフィックスの`Settings`は、`config/initializers/rails_config.rb`で変更できます。


## 参考サイト

* [Railsで定数を環境ごとに管理するrails_config](http://qiita.com/yumiyon/items/32c6afb5e2e5b7ff369e)
* [Railsで定数を一元管理する](http://blog.hello-world.jp.net/ruby/2268/)
* [railsconfig/rails_config](https://github.com/railsconfig/rails_config)
