+++
date = "2014-10-06T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "fuelphpアプリのgit管理"
slug = "git-management-of-fuelphp"
+++

fuelphpをcreate(clone)した状態だと、push先が`fuel`を向いているので、アプリをgit管理するために`git init`し直す必要がある。

まずはfuelphpをインストールするための`oil`コマンドをインストールし、アプリのひな形を作成

<pre><code class="language-bash">$ curl get.fuelphp.com/oil | sh

$ cd Sites/
$ oil create sample
</code></pre>

必要のないファイルを削除

<pre><code class="language-bash">$ rm -rf .git .gitmodules *.md docs fuel/core fuel/packages
</code></pre>

`fuel/core`、`fuel/packages`を`git submodule`で新たにインストール。

<pre><code class="language-bash">$ cd sample
$ git init
$ git submodule add git://github.com/fuel/core.git fuel/core
$ git submodule add git://github.com/fuel/oil.git fuel/packages/oil
$ git submodule add git://github.com/fuel/auth.git fuel/packages/auth
$ git submodule add git://github.com/fuel/parser.git fuel/packages/parser
$ git submodule add git://github.com/fuel/orm.git fuel/packages/orm
$ git submodule add git://github.com/fuel/email.git fuel/packages/email
</code></pre>

push先(GitHubなど)、remote/originを設定する

<pre><code class="language-bash">$ git remote add origin git@github.com:tatsuyano/sample.git
</code></pre>
