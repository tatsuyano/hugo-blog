+++
date = "2014-09-25T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["linux"]
title = "jqをインストールする"
slug = "how-to-install-and-setup-jq"
+++

[こちらのサイト](http://shibayu36.hatenablog.com/entry/2014/09/22/211719)を参考に、`jq`をインストールしてみました。  
思っていた以上に必要なライブラリがあって、ちょっと時間がかかりました。

#### libtoolとflexのインストール

<pre><code class="language-bash">$ sudo yum -y install libtool flex
</code></pre>

#### bisonのインストール

バージョン3.0以上が必要です。  
`configure: error: You need bison version 3.0 or greater.`  
初めは`yum`でインストールしましたが、バージョンが2.4.1だったので、手動で`make`しました。

<pre><code class="language-bash">$ sudo bash
# cd /usr/local/src
# wget http://ftp.gnu.org/gnu/bison/bison-3.0.2.tar.gz
# tar xvzf bison-3.0.2.tar.gz
# cd bison-3.0.2
# ./configure
# make
# make install
# ln -s /usr/local/bin/bison /usr/bin/bison
</code></pre>

#### bisonにyaccコマンドをaliasする

<pre><code class="language-bash">$ alias yacc='bison'
/usr/bin/bison
</code></pre>

* [yacc command not found and y.tab.c error 127 after installing bison](http://stackoverflow.com/questions/16420756/make-file-yacc-command-not-found-and-y-tab-c-error-127-after-installing-biso)

#### 鬼車のインストール

`oniguruma-5.9.5`のインストール。`ruby`に標準に入っているものではなく、単体版が必要な模様。

<pre><code class="language-bash">$ sudo bash
# cd /tmp/
# wget http://www.geocities.jp/kosako3/oniguruma/archive/onig-5.9.5.tar.gz
# tar xvzf onig-5.9.5.tar.gz
# cd onig-5.9.5
# ./configure --prefix=/usr
# make
# make install
# ldconfig
</code></pre>

* [CentOS 5.5に正規表現ライブラリ 鬼車 4.7.1 をインストールしてみる。](http://d.hatena.ne.jp/IT7C/20100828/1283007564)

### jqのインストール

やっと`jq`のインストールです。ソースが`git`にあるので今回も`ghq`で取得します。

<pre><code class="language-bash">$ ghq get https://github.com/stedolan/jq.git
$ sudo bash
# autoreconf -i
# ./configure
# make
# make install
</code></pre>

環境によっては、他にも必要なライブラリがあると思いますので、[本家サイト](http://stedolan.github.io/jq/download/)を確認してください。


*To build it from a git clone, you’ll need to install a few packages first:*  
*Flex*  
*Bison*  
*GCC*  
*Make*  
*Autotools*

インストールが正常に終わったら、下記のコマンドで違いを確認してみてください。  
お疲れ様でした。

<pre><code class="language-bash">$ curl 'https://api.github.com/repos/stedolan/jq/commits?per_page=5'
</code></pre>

pretty化

<pre><code class="language-bash">$ curl 'https://api.github.com/repos/stedolan/jq/commits?per_page=5' | jq '.'
</code></pre>

### 参考サイト

* [curlとjqで簡単にAPIの調査をする](http://shibayu36.hatenablog.com/entry/2014/09/22/211719)
* [yacc command not found and y.tab.c error 127 after installing bison](http://stackoverflow.com/questions/16420756/make-file-yacc-command-not-found-and-y-tab-c-error-127-after-installing-biso)
* [CentOS 5.5に正規表現ライブラリ 鬼車 4.7.1 をインストールしてみる。](http://d.hatena.ne.jp/IT7C/20100828/1283007564)
* [jq:Download jq](http://stedolan.github.io/jq/download/)
* [jq:Tutorial](http://stedolan.github.io/jq/tutorial/)
