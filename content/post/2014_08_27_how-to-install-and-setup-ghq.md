+++
date = "2014-08-27T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["go"]
title = "ghqをインストールする"
slug = "how-to-install-and-setup-ghq"
+++

[ghq](http://motemen.hatenablog.com/entry/2014/06/01/introducing-ghq)というローカルリポジトリを一元的に
管理するツールをインストールしてみました。こちらのツールを使うと`golang`と同じようなディレクトリ構成で
リポジトリを管理できるようになります。

また今回は[こちらの記事](http://blog.kentarok.org/entry/2014/06/03/135300)を参考に(というかまんま同じに)、
リポジトリ(ソース)を管理するようにしてみました。

### golangのディレクトリ構成

まずは`GOPATH`を`$HOME`に設定し、以下のようなディレクトリ構成にします。

.zshrc

    ## golang
    export GOPATH=$HOME
    export PATH=$PATH:$GOPATH/bin

`$HOME`配下の構成

    .
    ├── bin
    ├── pkg
    └── src
        └── github.com
                ├── clone
                ├── codegangsta
                ├── mattn
                └── peco

これで`golang`のリポジトリ(ソース)は、`src`配下に作成されます。
また今回は`ghq`を使ってインストール(clone)する`golang`以外のリポジトリも`src`配下にインストールするよう設定します。

### ghqのインストール

    $ go get github.com/motemen/ghq

`$HOME/.gitconfig`に`ghq`を使ってインストールするディレクトリ先を指定します。

    [ghq]
        root = ~/src

これで`ghq`コマンドを使って、インストールできるようになります。

    $ ghq get https://github.com/zsh-users/zsh-syntax-highlighting.git

`src`ディレクトリ配下にインストールされます。

    src
    └── github.com
            └── zsh-users
                └── zsh-syntax-highlighting

### ghqでインストールしたリポジトリの一覧を表示し、移動するスクリプト

    # peco-src
    # (http://blog.kentarok.org/entry/2014/06/03/135300)
    function peco-src () {
        local selected_dir=$(ghq list --full-path | peco --query "$LBUFFER")
        if [ -n "$selected_dir" ]; then
            BUFFER="cd ${selected_dir}"
            zle accept-line
        fi
        zle clear-screen
    }
    zle -N peco-src
    bindkey '^S' peco-src

### まとめ

今までの自己流から、ずいぶん分かりやすくなりました。  
当面この方法で管理をしていこうと思います。

お疲れ様でした。


### 参考サイト

* [リモートリポジトリのローカルクローンをシンプルに管理する](http://motemen.hatenablog.com/entry/2014/06/01/introducing-ghq)
* [ghqを使ったローカルリポジトリの統一的・効率的な管理について](http://blog.kentarok.org/entry/2014/06/03/135300)

### 関連する記事

* [流行りのpecoをcentosにインストールしました](http://blog.10rane.com/2014/07/02/installed-the-peco-to-centos/)
* [goパッケージ(peco)のバージョンを上げる](http://blog.10rane.com/2014/08/14/updating-the-package-go/)
* [pecoで接続先hostを選ぶスクリプトを書いた](http://blog.10rane.com/2014/08/21/select-host-for-peco/)
