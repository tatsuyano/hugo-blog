+++
date = "2017-09-27T13:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["swift","xcode","iphonex"]
title = "Xcode8でつくっていたアプリをiPhoneXでシュミレートすると画面の上下が描画されない"
slug = "screen_shifts_with_iphonex"
+++

iPhoneXでの動きを確かめるために、Xcode8,Swift3.2で作っていたアプリをXcode9,Swift4.0にConvertし、iPhoneXでシュミレートしてみた。
結果、本来描画されるべき領域(画面の上部と下部)が描画されなかった。

はじめはiOS11から導入されたSafeAreaの影響かなと思ったが違った。

## Safe Area
* [iOS 11 の Safe Area は Auto Layout だけでなくコードベースでも取れる](http://qiita.com/lovee/items/21131fb441a1a86b45e3)
* [Top/Bottom Layout GuideをSafe Area対応する](https://qiita.com/eimei4coding/items/5a0794cf7cd13c82b2b5)
* [Human Interface Guidelines iPhone X](https://developer.apple.com/ios/human-interface-guidelines/overview/iphone-x/)

結論としては、iPhoneX用のLaunchImageが設定されていないために、アプリがiPhoneXに対応していないとみなされ、iPhoneXでは描画されるべき領域に
描画されなかったのが原因だった。

* [iPhone Xのセーフエリアやマージン幅について](http://qiita.com/usagimaru/items/761e9a5f3d78b1939df8)
* [iPhone XのSafariで表示される謎の空白を消す方法](http://qiita.com/hin0deya_p0n1/items/2b8cc28f176e202976b7)
