+++
date = "2012-09-04T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["mysql", "perl"]
title = "cpanmでDBD::mysqlをインストールしようとすると、mysql.hがないと怒られる"
slug = "cpanm-dbd-mysql"
+++

## インストール

``` perl
cpanm DBD::mysql
```

しようするとエラーが。。build.logを確認してみると  

```
emacs .cpanm/build.log
...
dbdimp.h:24:49: error: mysql.h: そのようなファイルやディレクトリはありません
```

とのこと。ググったところ、「mysql.h」は「mysql-devel」に入っているらしいので、mysql-develをインストールし、再度 cpanm DBD::mysqlを実行。  

```
sudo yum -y install mysql-devel
...
cpanm DBD::mysql
```

今度は  

```
emacs .cpanm/build.log
...
make: *** [test_dynamic] エラー 255
```

エラーの原因がよくわからなかったので、再度で検索したところ、下記のサイトの方法で対応できました。  
[Mac OS XにDBD::mysqlをインストール](http://shibayu36.hatenablog.com/entry/20100605/1275754485)

```
cd .cpanm/latest-build/DBD-mysql*
perl Makefile.PL --testuser='hoge' --testpassword='piyo'
make
make test
make install
```

mysqlの接続でしけていたんですね。勉強になりました。  

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [Mac OS XにDBD::mysqlをインストール](http://shibayu36.hatenablog.com/entry/20100605/1275754485")
* [Rails3の bundle install で mysql.h が見つからないというエラー](http://d.hatena.ne.jp/eisenbach/20111019/1319021855")
