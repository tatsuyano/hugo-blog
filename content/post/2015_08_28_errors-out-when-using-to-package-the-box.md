+++
date = "2015-08-28T19:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["middleware","vagrant"]
title = "package化したboxを使うときによく出るエラー"
slug = "errors-out-when-using-to-package-the-box"
+++

`vagrant package`コマンドでパッケージ化したboxで仮想環境を作成(`vagrant up`)するさいによく出るエラーをまとめました。

## An error occurred while downloading the remote file(モートファイルのダウンロード中にエラーが発生しました)
```
An error occurred while downloading the remote file. The error
message, if any, is reproduced below. Please fix this error and try
again.

Couldn't open file xxxxxxxxxxxx
```

*解決方法*<br>
パッケージ(コピー)元のboxと同じ設定のbox(パッケージ化したboxで生成した仮想環境)がバッティングしている？
パッケージ元の仮想環境を`vagrant destroy`して削除する。
<br><br>

## Authentication failure. Retrying...(認証の失敗。再試行)
```
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: Warning: Connection timeout. Retrying...
    default: Warning: Authentication failure. Retrying...
    default: Warning: Authentication failure. Retrying...
    default: Warning: Authentication failure. Retrying...
```

*解決方法*<br>
`vagrant up`したさいに生成される秘密鍵と、パッケージしたbox(ゲストOS)に設定してある公開鍵がマッチしないためにエラーになる。
`vagrant ssh-config`で表示される秘密鍵から公開鍵を生成し、公開鍵を設定する。

1.秘密鍵を確認
```
$ vagrant ssh-config
...
  IdentityFile xxxx/vagrant_private_key
```

2.公開鍵を生成
```
ssh-keygen -yf xxxx/vagrant_private_key > public_key
```

3.ゲストOSに生成した公開鍵を設定する
```
$ vagrant ssh

vagrant at localhost in ~
$ vi /home/vagrant/.ssh/authorized_keys # 生成した公開鍵を設定する
```

### 参考サイト
* [Vagrant で作った VM にやってはいけない2つのこと](http://qiita.com/megane42/items/1d8ae7444d8c1b10bbd7)
<br><br>


## The following SSH command responded with a non-zero exit status(SSHコマンドがゼロ以外の終了ステータスで応答しました)

```
The following SSH command responded with a non-zero exit status.
Vagrant assumes that this means the command failed!

ARPCHECK=no /sbin/ifup eth1 2> /dev/null

Stdout from the command:

Device eth1 does not seem to be present, delaying initialization.


Stderr from the command:
```

*解決方法*</br>
MACアドレスとのマッピングを無効にし、再起動`vagrant reload`する。

```
$ vagrant ssh

vagrant at localhost in ~
$ sudo ln -s -f /dev/null /etc/udev/rules.d/70-persistent-net.rules
$ exit

$ vagrant reload
```

### 参考サイト
* [macで作られたvagrant boxをもらったときに出るネットワークエラーの対処法](http://qiita.com/tarr1124/items/8276e609c0f7cdec79e7)
* [Vagrantのネットワークが起動しないときは](http://blog.monochromegane.com/blog/2013/04/06/vagrant-network-error/)
