+++
date = "2017-09-20T13:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["push","swift","php"]
title = "iOSのpush設定、APNS_PHP用のpemファイル作成"
slug = "push_notification_of_ios_and_pem_file_of_apnsphp"
+++

## Pushの仕組み
* [プッシュ通知に必要な証明書の作り方2017](http://qiita.com/natsumo/items/d5cc1d0be427ca3af1cb)
* [APNs プッシュ通知、デバイストークンの取得](https://i-app-tec.com/ios/apns-device-token.html)
* [APNs プッシュ通知、リモート通知の取得](https://i-app-tec.com/ios/apns-remote.html)

## 証明書の種類、生成方法
* [Push通知の証明書、p12 -> pem変換について:疎通テスト方法](http://ryokwkm2.hatenadiary.jp/entry/2016/08/10/095840)
* [RSA鍵、証明書のファイルフォーマットについて](http://qiita.com/kunichiko/items/12cbccaadcbf41c72735)
* [iOSでPush通知機能の実装に必要な2つの証明書を作成する方法まとめました](http://blog.kmusiclife.com/p/xcode-push-cer/)

p12 ... Certificatesから出力できるファイル。Certificatesは証明書のみ（証明書と、必要な秘密鍵が書かれているのみ。実際の鍵は入っていない。）で、
p12ファイルは秘密鍵を含めた証明書なので管理は厳重に。Admin権限をもつことと等価。
プッシュサービス（Repro, Parse）などのMBaaSを利用する際にはPush通知用のこのファイルを書き出してサービス側に登録したりすることもある。

openssl pkcs12 -clcerts -nodes -in marry3_stg_pw_marry.p12 -out marry3_stg_pw_marry.pem
