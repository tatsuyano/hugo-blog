+++
date = "2012-12-21T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["nginx"]
title = "nginxのファイルのアップロードサイズを変更する　413 Request Entity Too Large"
slug = "nginx-request-entity-too-large"
+++

サイズの大きなファイルをアップロードしようとするとタイトルのエラーが出るので、その対処方法。

## nginx.conf

まずはnginx.confファイルに「client_max_body_size 20M;」を追加。  
私はリバースプロキシを利用しているので、2箇所に記述。  

``` scheme
server {
   listen       80;
      server_name blog.10rane.com;

   client_max_body_size 20M; # 413 Request Entity Too Large
   .....

server {
   listen       8080;
      server_name blog.10rane.com;

   client_max_body_size 20M; # 413 Request Entity Too Large
   .....
```

## php.ini

php.iniは3箇所の変更。  

``` php
# emacs /etc/php.ini
upload_max_filesize = 20M
post_max_size = 20M
memory_limit = 128M
```

## 再起動
nginx、fast-cgi両方を再起動。  

```
/etc/init.d/nginx restart
/etc/init.d/php-fastcgi restart
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！  

- [nginx error – 413 Request Entity Too Large と出る場合は最大ファイルサイズを変更する](http://weble.org/2012/05/28/nginx-error-413-request-entity-too-large-%E3%81%A8%E5%87%BA%E3%82%8B%E5%A0%B4%E5%90%88%E3%81%AF%E6%9C%80%E5%A4%A7%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%82%B5%E3%82%A4%E3%82%BA%E3%82%92)
- [FileをUploadするためのnginxとphp.iniの設定](http://se-suganuma.blogspot.jp/2012/04/fileuploadnginxphpini.html)
