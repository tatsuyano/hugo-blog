+++
date = "2015-03-27T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby"]
title = "Rubyの標準ライブラリの構成について"
slug = "standard-library-of-ruby"
+++

`$LOAD_PATH`について調べていた時に、Rubyの標準ライブラリの
ディレクトリ構成の意味をわかっていない事にきづいたので、調べてみました。

<pre><code class="language-bash">$ ruby -e 'puts $:'
$HOME/.anyenv/envs/rbenv/rbenv.d/exec/gem-rehash
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby/2.2.0/x86_64-linux
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby/2.2.0/x86_64-linux
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/2.2.0/x86_64-linux
</code></pre>
<br>

大きくは4つの階層に分かれていて、以下のような構成になっています。

<pre><code class="language-bash">$ cd $HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby

$ tree -d -L 1
.
├── 2.2.0       # 標準ライブラリ
├── gems        # gemファイルの置き場
├── site_ruby   # ユーザのライブラリ置き場
└── vendor_ruby # ベンダー用のライブラリ
</code></pre>
<br>

また、`gems`ディレクトリ意外のディレクトリには、それぞれ環境依存のソースを置くディレクトリがあります。
※ 自分の環境はCentOSなので、`x86_64-linux`という名前のディレクトリになっています。

<pre><code class="language-bash">$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/2.2.0/x86_64-linux # soファイルなど、環境に依存したソース

$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/site_ruby/2.2.0/x86_64-linux # 環境依存

$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby/2.2.0
$HOME/.anyenv/envs/rbenv/versions/2.2.0/lib/ruby/vendor_ruby/2.2.0/x86_64-linux # 環境依存
</code></pre>
<br>

## 参考サイト

*
[Rubyのライブラリの標準ディレクトリ構成を考えて、自作ライブラリを何処に置くべきか思案する](http://takuya-1st.hatenablog.jp/entry/2012100\7/1349587308)
