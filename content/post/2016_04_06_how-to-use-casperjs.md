+++
date = "2016-04-06T19:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["js","nodejs","casperjs"]
title = "CasperJsの使い方メモ"
slug = "how-to-use-casperjs"
+++

### 日本語のページのキャプチャを取る

日本語のページのキャプチャを撮るには、CasperJsを実行するサーバーに日本語フォントが入っている必要がある。

``` shell
sudo yum -y groupinstall "Japanese Support"
```

### CasperJsを一時停止させる

3秒プログラムを停止させてからキャプチャを撮る

``` javascript
casper.then(function() {
  this.wait(3000, function() {
    console.log('3秒後');
    this.capture('capture/test.png');
  });
});
```

### CasperJsからJSONをPOSTする

``` javascript
casper.start();
casper.open('http://hoge.com/api/hoge', {
  method: 'post',
  headers: {
    'Accept-Language': 'ja'
  },
  data: {
    uuid   : 'UUID',
    secret : 'SECRET'
  }
});

// this.getPageContent()で結果を取得できる
casper.then(function(response) {
  var json = JSON.parse(this.getPageContent());
});
```

### CasperJsにメソッドを定義する

``` javascript
casper.then(function() {
  this.createMarkdown('title','describe');
});

casper.createMarkdown = function(title,describe){
  template = fs.read('result/api_template.md', 'utf8');
  template = template.replace(/{{title}}/g    ,title);
  template = template.replace(/{{describe}}/g ,describe);
  fs.write('result/'+title+'.md', template);
};
```

### ファイルアップロード(type="file")を操作する

``` javascript
 casper.then(function() {
   // 画像はフルパスで、evaluate()の外で実行する
   this.page.uploadFile('input[type="file"]', '/vagrant/sample_image.jpg');

   this.evaluate(function(number, title) {
     $('[name="coupon[number]"]').val(number);
     $('[name="coupon[title]"]').val(title);

     document.querySelector('form').submit();

   }, '999', 'title_test');
 });
```
