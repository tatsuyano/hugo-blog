+++
date = "2013-03-26T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "ruby"]
title = "emacsでのRuby環境(Rsenseでオムニ補完)"
slug = "emacs-rsense"
+++

今更ですが、最近yugaiさんの「初めてのRuby」を読み始めています。すごく読みやすくて楽しいです。  
そこでemacsのRuby環境もちゃんと用意することにしました。  

## Rsenseとは

Rsenseは、rubyコードのオムニ補完(文脈を解析して行う補完)をしてくれるツールです。  
emacsでのrubyのコード補完は、他にもauto-complete(単体)、auto-complete-ruby、rcodetools などあります。  
※ Rsense は Java Runtime Environment (JRE) 1.5以上が必要です。JREはOSXに標準でインストールされています。  

## Rsense のインストール

本家サイトから最新版をwgetします。  
※ emacs には el-get.le を事前にインストールしておいてください。  

```
wget http://cx4a.org/pub/rsense/rsense-0.3.tar.bz2
tar xvzf rsense-0.3.tar.bz2

mkdir .emacs.d/opt
mv rsense-0.3 .emacs.d/opt/
```

## .rsenseの作成

次に Rsense に Rubyの環境変数を指定するための ~/.rsense を作成します。

```
cd $HOME/.emacs.d/opt/rsense-0.3/
chmod +x ./bin/rsense
bin/rsense version
>> RSense 0.3 # <- 今回インストールしたRsenseのバージョン

# $HOME 直下に ~/.rsenseファイルを作成、環境変数が設定されている
ruby ./etc/config.rb > ~/.rsense
```

## Java Runtime が正しく動くか確認

生成した ~/.rsense の環境変数が正しく設定されていないと Java Runtime が動きません。  
下記のコマンドで、まずJava Runtimeが正常に動くか確認してください。  

```
cd $HOME/.emacs.d/opt/rsense-0.3/
bin/rsense server # <- 環境変数がおかしいと、この段階でエラーが出るのでパスを修正する

java -cp lib/jruby.jar:lib/antlr-runtime-3.2.jar:lib/rsense.jar org.cx4a.rsense.Main version
RSense 0.3 # <- 正常値
```

本家サイトに[トラブルシューティング](http://cx4a.org/software/rsense/manual.ja.html#.E3.83.88.E3.83.A9.E3.83.96.E3.83.AB.E3.82.B7.E3.83.A5.E3.83.BC.E3.83.86.E3.82.A3.E3.83.B3.E3.82.B0")があるのでうまく動かない場合は、確認にしてみてください。  

## .emacs に Rsense の設定を追加

.emacs.d/conf/54-ruby.el
``` scheme
(setq rsense-home (expand-file-name "~/.emacs.d/opt/rsense-0.3"))
(add-to-list 'load-path (concat rsense-home "/etc"))
(require 'rsense)
(add-hook 'ruby-mode-hook
  (lambda ()
    (add-to-list 'ac-sources 'ac-source-rsense-method)
    (add-to-list 'ac-sources 'ac-source-rsense-constant)))
```

Rsenseが正しく動いているか確認
```
M-x rsense-version # => Rsense 3.0
```

![](https://dl.dropboxusercontent.com/u/159938/blog_images/emacs_rsense_001.png)

## リファレンスの追加

リファレンスを追加することで、オムニ補完中にリファレンスを表示することができる。  

```
cd $HOME/.emacs.d/opt/rsense-0.3/doc
wget ruby-refm-1.9.3-dynamic-snapshot.tar.gz
tar xvzf ruby-refm-1.9.3-dynamic-snapshot.tar.gz
```

### .emacs に設定追加

.emacs.d/conf/54-ruby.el
``` scheme
(setq rsense-rurema-home (concat rsense-home "/doc/ruby-refm-1.9.3-dynamic-snapshot"))
(setq rsense-rurema-refe "refe-1_9_3")
```

![](https://dl.dropboxusercontent.com/u/159938/blog_images/emacs_rsense_002.png)

## 参考サイト

* [Rsenseを動かしてみた](http://kayakaya.net/d/20100321.html)
* [本家サイト トラブルシューティング](http://cx4a.org/software/rsense/manual.ja.html#.E3.83.88.E3.83.A9.E3.83.96.E3.83.AB.E3.82.B7.E3.83.A5.E3.83.BC.E3.83.86.E3.82.A3.E3.83.B3.E3.82.B0)
* [Emacs で ruby の自動入力補完とリファレンスの表示](http://tcnksm.sakura.ne.jp/blog/2012/05/07/)
* [本家サイト Rubyリファレンスマニュアル](http://cx4a.org/software/rsense/manual.ja.html#Ruby.E3.83.AA.E3.83.95.E3.82.A1.E3.83.AC.E3.83.B3.E3.82.B9.E3.83.9E.E3.83.8B.E3.83.A5.E3.82.A2.E3.83.AB)


## ruby-block、ruby-electric のインストール

ruby-block は、end に対応する行をハイライトしてくれるパッケージです。  
ruby-electric は、括弧などを自動挿入してくれます。  
  
こちらは el-get に初めから recipe があるので、M-x el-get-list-package でインストールします。  
  
.emacs.d/conf/54-ruby.el
``` scheme
(require 'ruby-block)
(ruby-block-mode t)
(setq ruby-block-highlight-toggle t)

(require 'ruby-electric)
(add-hook 'ruby-mode-hook '(lambda () (ruby-electric-mode t)))
(setq ruby-electric-expand-delimiters-list nil)
```

## 参考サイト

* [emacsのruby環境を整えています](http://shibayu36.hatenablog.com/entry/2013/03/18/192651)

## inf-ruby のインストール

irb を emacs 上から利用することができます。M-x run-ruby (C-c C-s)  
el-get でインストールすると error がでて、Rsense が動かなくなったので、自分で recipe を用意します。  
  
.emacs.d/conf/10-el-get.el
``` scheme
(setq el-get-dir "~/.emacs.d/elisp/el-get/")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
    (url-retrieve-synchronously
    "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

(setq el-get-sources
  '(
  (:name inf-ruby
    :type http
    :url "https://raw.github.com/ruby/ruby/trunk/misc/inf-ruby.el"
    :description "[My Recipes] Inferior Ruby Mode")
    ))
(el-get 'sync)
```

.emacs.d/conf/54-ruby.el
``` scheme
(autoload 'run-ruby "inf-ruby" "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby" "Set local key defs for inf-ruby in ruby-mode")
(add-hook 'ruby-mode-hook '(lambda () (inf-ruby-keys)))
```

## 参考サイト

* [Emacs から irb を使いやすくするために inf-ruby.el の設定を追加した](http://d.hatena.ne.jp/a666666/20090703/1246609986)


## rcodetools のインストール

コード補完や xmp(画面出力の結果をコメントにしてソースに自動で記載してくれる) など、便利な機能が追加されます。  
自分は補完は Rsense で行なっているので、xmp のみ使っています。  

```
gem install rcodetools
cp -a .rbenv/versions/2.0.0-p0/lib/ruby/gems/2.0.0/gems/rcodetools-0.8.5.0/rcodetools.el .emacs.d/elisp/
```

.emacs.d/conf/54-ruby.el
``` scheme
(require 'rcodetools)
(setq rct-find-tag-if-available nil)
(defun ruby-mode-hook-rcodetools ()
;;  (define-key ruby-mode-map "\M-\C-i" 'rct-complete-symbol)
  (define-key ruby-mode-map "\C-c\C-t" 'ruby-toggle-buffer)
  (define-key ruby-mode-map "\C-c\C-d" 'xmp)
  (define-key ruby-mode-map "\C-c\C-f" 'rct-ri))
  (add-hook 'ruby-mode-hook 'ruby-mode-hook-rcodetools)
```

' # =>' とコメントして、C-cC-d すると、  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/emacs_rsense_003.png)

## 出力結果をコメントしてくれる。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/emacs_rsense_004.png)

## 参考サイト

* [rcodetoolsが素晴らしすぎる件](http://d.hatena.ne.jp/authorNari/20090523/1243051306)


## 最終的な.emacsd
※ el-get の recipe は省略してます。  

.emacs.d/conf/54-ruby.el

``` scheme
;; --------------------------------------------------
;; ruby-mode
;; http://shibayu36.hatenablog.com/entry/2013/03/18/192651
;; --------------------------------------------------
(autoload 'ruby-mode "ruby-mode"
  "Mode for editing ruby source files" t)
  (add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Capfile$" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))

;; --------------------------------------------------
;; Rsense
;; [description] オムニ補完
;; http://kayakaya.net/d/20100321.html
;; http://cx4a.org/software/rsense/manual.ja.html#.E3.83.88.E3.83.A9.E3.83.96.E3.83.AB.E3.82.B7.E3.83.A5.E3.83.BC.E3.83.86.E3.82.A3.E3.83.B3.E3.82.B0
;; http://tcnksm.sakura.ne.jp/blog/2012/05/07/
;; --------------------------------------------------
(setq rsense-home (expand-file-name "~/.emacs.d/opt/rsense-0.3"))
(add-to-list 'load-path (concat rsense-home "/etc"))
(require 'rsense)
(add-hook 'ruby-mode-hook
  (lambda ()
    (add-to-list 'ac-sources 'ac-source-rsense-method)
    (add-to-list 'ac-sources 'ac-source-rsense-constant)))

;; http://cx4a.org/software/rsense/manual.ja.html#Ruby.E3.83.AA.E3.83.95.E3.82.A1.E3.83.AC.E3.83.B3.E3.82.B9.E3.83.9E.E3.83.8B.E3.83.A5.E3.82.A2.E3.83.AB
;; cd $HOME/.emacs.d/opt/rsense-0.3/doc && wget ruby-refm-1.9.3-dynamic-snapshot.tar.gz
(setq rsense-rurema-home (concat rsense-home "/doc/ruby-refm-1.9.3-dynamic-snapshot"))
(setq rsense-rurema-refe "refe-1_9_3")

;; --------------------------------------------------
;; ruby-block
;; [description] endに対応する行のハイライト
;; --------------------------------------------------
(require 'ruby-block)
(ruby-block-mode t)
(setq ruby-block-highlight-toggle t)

;; --------------------------------------------------
;; ruby-electric
;; [description] 括弧などを自動挿入
;; --------------------------------------------------
(require 'ruby-electric)
(add-hook 'ruby-mode-hook '(lambda () (ruby-electric-mode t)))
(setq ruby-electric-expand-delimiters-list nil)

;; --------------------------------------------------
;; rcodetools
;; http://d.hatena.ne.jp/authorNari/20090523/1243051306
;; gem install rcodetools
;; cp -a .rbenv/versions/2.0.0-p0/lib/ruby/gems/2.0.0/gems/rcodetools-0.8.5.0/rcodetools.el .emacs.d/elisp/
;; --------------------------------------------------
(require 'rcodetools)
(setq rct-find-tag-if-available nil)
(defun ruby-mode-hook-rcodetools ()
;;  (define-key ruby-mode-map "\M-\C-i" 'rct-complete-symbol)
  (define-key ruby-mode-map "\C-c\C-t" 'ruby-toggle-buffer)
    (define-key ruby-mode-map "\C-c\C-d" 'xmp)
      (define-key ruby-mode-map "\C-c\C-f" 'rct-ri))
      (add-hook 'ruby-mode-hook 'ruby-mode-hook-rcodetools)

;; --------------------------------------------------
;; inf-ruby
;; [description] irbをemacsから利用する。M-x run-ruby (C-c C-s)
;; el-getでインストールするとerrorがでて、rsenseが動かなくなったので、自分でrecipeを用意すること
;; http://d.hatena.ne.jp/a666666/20090703/1246609986
;; --------------------------------------------------
(autoload 'run-ruby "inf-ruby" "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby" "Set local key defs for inf-ruby in ruby-mode")
(add-hook 'ruby-mode-hook '(lambda () (inf-ruby-keys)))
```
