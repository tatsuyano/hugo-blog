+++
date = "2013-04-15T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["running"]
title = "掛川・新茶マラソン"
slug = "kakegawa-shincha-marathon"
+++

昨日、第8回掛川・新茶マラソンに参加してきました。  
まずレース結果ですが、5時間オーバーの5:02:38(ネット)で、惨敗でした。。  
それでは簡単にですがレビューしたいと思います。  

## 会場までのアクセス

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_001.jpg)

今回行きは、6:33分発の東海道新幹線で掛川駅まで行き、掛川駅からシャトルバスで、会場の「つま恋スポーツ＆アミューズメント」に行きました。  
時間は予定どおり、スタートの1時間前に着いたのですが、駐車場から会場までが遠く、スタートの9:30に間に合わずになんと9:40分スタートになってしまいました。。  
余裕をもって調整したつもりでしたが、これはちょっと予想外でした。

## レースコンディション

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_002.png)

天候は曇り時々晴れで、程よい風が吹いていて、とても走りやすい状態でした。  
また体調調節もうまくいき、今までのフルマラソンの中で一番いいコンディションでした。  

## コース

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_003.png)

コースは思っていたよりフラットで、走りやすかったです。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_004.png)

また給水場の数も多く、とても良かったです。

### 27km地点くらいからペースダウン

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_005.png)

前半、予定通りのペースでいけ、これは4時間30分を切れるかもしれないと考えていたのですが、27km地点で少し立ちくらみをし、ペースが落ちてしまいました。  
実は今回、レース1ヶ月前ぐらいから膝に痛みがあり、あまり練習ができずスタミナが切れてしまいました。  

## 総評

レースコンディション、体調調整とはじめてうまくいった大会でしたが、そもそもの練習不足という、もったいない大会になってしまいました。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/kakegawa_006.jpg)
