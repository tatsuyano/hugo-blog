+++
date = "2013-04-05T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["mac"]
title = "iterm2のカラースキームを変更する"
slug = "item2-color-scheme"
+++

最近は background を白系の透過で使っていたが、なんとなくまた黒系の透過に戻してみた。  
毎回自分で設定するのは面倒なので、今回はこちらの方のカラースキームを使わしてもらうことにした。  
  
[iTerm 2 で使えるカラースキーム、Japanesque を作った](http://this.aereal.org/entry/2013/01/02/222304)  

## カラースキームのダウンロード

```
$ mkdir Dropbox/src/itermcolors
$ cd Dropbox/src/itermcolors
$ wget https://raw.github.com/aereal/dotfiles/master/colors/Japanesque/Japanesque.itermcolors
```

## カラースキームの設定

### 新しい Profile を作成する

Preferences > Profiles > General > +  
Preferences > Profiles > General > Name  
Preferences > Profiles > General > Other Actions > Set as Default  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/iterm2_color_scheme_001.png)

### ダウンロードしたカラースキームを import する
  
Import を行うと、Load Presets に新たに Japanesque が追加される。  
  
Preferences > Profiles > Colors > Import  
Preferences > Profiles > Colors > Japanesque  

![](https://dl.dropboxusercontent.com/u/159938/blog_images/iterm2_color_scheme_002.png)

## 参考サイト
以下のサイトを参考にさせていただきました。ありがとうございます！  

* [iTerm2のカラースキームをSolarizedにする](http://d.hatena.ne.jp/nomnel/20111120/1321799289)
* [iTerm 2 で使えるカラースキーム、Japanesque を作った](http://this.aereal.org/entry/2013/01/02/222304)
