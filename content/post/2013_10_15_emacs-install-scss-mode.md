+++
date = "2013-10-15T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "css"]
title = "auto-installでscss-modeをインストールする"
slug = "emacs-install-scss-mode"
+++

el-getでうまくインストールできなかったので、今回はauto-installを使いました。

```
M-x auto-install-from-url RET https://raw.github.com/antonj/scss-mode/master/scss-mode.el
```

touch ~/.emacs.d/conf/55-scss.el
``` scheme
;; scss-mode
;; https://github.com/antonj/scss-mode
(autoload 'scss-mode "scss-mode")
(add-to-list 'auto-mode-alist '("\\.\\(scss\\|css\\|sass\\)\\'" . scss-mode))
(setq scss-compile-at-save nil)

;; auto-comple
(add-to-list 'ac-modes 'scss-mode)
(add-hook 'scss-mode-hook 'ac-css-mode-setup)
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [emacsでscssを扱うscss-modeを入れる](http://blog.sanojimaru.com/post/20621077969/emacs-scss-scss-mode)
* [Emacsにscss-modeをいれた。](http://blog.be-open.net/emacs/emacs-scss-mode/)
