+++
date = "2016-01-06T10:40:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["apple","swift"]
title = "SwiftからObjective-Cを呼ぶ方法"
slug = "how-to-call-objective-c-from-swift"
+++

Swiftで開発していて、Objective-Cのライブラリを使いたいケースがよくあります。
試しに、[LUKeychainAccess](https://github.com/TheLevelUp/LUKeychainAccess)というKeychain ServicesのラッパークラスををSwiftから呼び出したいと思います。
*今回はLUKeychainAccessのインストールは、CocoaPodなどを使わず手動で行います*

### 実装の流れ
実装の流れは以下のようになります。

1. Swiftのプロジェクトを作成し、Objective-Cのライブラリを追加
2. ブリッジファイルを作成し、Objective-Cのライブラリのをimport
3. SwiftのからObjective-Cのコードを呼び出す

### Swiftのプロジェクトを作成し、Objective-Cのライブラリを追加
まずは、いつもどおりSwiftのプロジェクトを作成します。
テンプレートは「SingleViewApplication」、プロジェクト名は「LUKeychainFromSwift」で作成します。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_001.png" width="100%">

次にダウンロードしたLUKeychainAccessのヘッダーファイルなどが入っているディレクトリ(LUKeychainAccess/LUKeychainAccess)をD&Dで、プロジェクトにコピーします。
今回はプロジェクト内にライブラリを追加したいので、`Copy items if needed`と`Create groups`にチェックを入れてください。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_002.png" width="100%">

ライブラリを追加すると、自動で`Build Phases > Compile Sources` に`LUKeychainAccess.m`と`LUKeychainServices.m`が追加されます。
追加されていない場合は手動で追加してください。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_003.png" width="100%">

### ブリッジファイルを作成し、Objective-Cのライブラリをimport
ブリッジファイルを作成して、呼び出したいライブラリ(ヘッダーファイル)を指定(import)します。

`File > New > File... > Header File` からヘッダーファイルを選択し、ブリッジファイル「LUKeychainFromSwift-Bridging-Header.h」を作成します。
作成したブリッジファイルにimport先のライブラリ(`LUKeychainAccess.h`)を指定します。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_004.png" width="100%">

※ 今回は自前でObjective-Cのクラスを実装しないので、手動でブリッジファイルを用意しましたが、
新規にObjective-Cのファイルを作成することで、自動でブリッジファイルを作成することが可能です。くわしくは下記サイトを確認してください。

* [Bridging Headerのファイル作成と設定を簡単に行う手順](http://easyramble.com/setup-bridging-header-easily.html)

### Build Setting に作成したブリッジファイルを指定する
次に`Build Setting > Swift Compiler - Code Generation > Objective-C Bridgeing Header`に作成したブリッジファイル名を設定します。
以上で設定は完了です。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_005.png" width="100%">

### SwiftのからObjective-Cのコードを参照する
設定が正常に完了していれば、コード補完が機能されます。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_006.png" width="100%">

ためしに実行してみると、Keychain Servicesに保存した値が引っ張ってこれることが確認できます。

<img src="https://dl.dropboxusercontent.com/u/159938/blog_images/how-to-call-objective-c-from-swift_007.png" width="100%">

### 参考サイト
* [Objective-CプログラムをSwiftから呼び出す方法](http://crunchtimer.jp/blog/technology/swift/3312/)
* [Bridging Headerのファイル作成と設定を簡単に行う手順](http://easyramble.com/setup-bridging-header-easily.html)
* [How to call Objective C code from Swift](http://stackoverflow.com/questions/24002369/how-to-call-objective-c-code-from-swift/24005242#24005242)
* [Undefined symbols for architecture armv7:〜という警告が出たときの対策方法](http://blog.ch3cooh.jp/entry/20130204/1359942207)
