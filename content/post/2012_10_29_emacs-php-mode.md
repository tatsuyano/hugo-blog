+++
date = "2012-10-29T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php", "emacs"]
title = "php-mode.elをインストールする"
slug = "emacs-php-mode"
+++

## インストール

```
M-x auto-install-from-url <RET>
http://php-mode.svn.sourceforge.net/svnroot/php-mode/tags/php-mode-1.5.0/php-mode.el
```

## .emacsの設定、50_php.elを作成する

``` scheme
cat >> .emacs.d/conf/50_php.el
;; php-mode
(require 'php-mode)
(setq php-mode-force-pear t)
(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [PHP開発環境 on Emacs](http://blog.fusic.co.jp/archives/94)
