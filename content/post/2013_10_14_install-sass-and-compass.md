+++
date = "2013-10-14T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["css"]
title = "sassとcompassをインストールする"
slug = "install-sass-and-compass"
+++

最近PGでもCSSがいじれて当たり前いう空気をヒシヒシと感じ、  
せっかくなんでsassを勉強することにしました。  
  
### 用語の整理
* less ... CSS拡張メタ言語。コンパイルして使う less -> css
* scss ... CSS拡張メタ言語。コンパイルして使う scss -> css
* sass ... scssのバージョン違い。広義の意味ではこれもscss。ファイルの拡張子も.scss
* compass ... cssのFW、というかツールに近い。

### どれを使うか

文法がシンプルでかつoctopressやrailsで採用されているので、sassを使うことにしました。  
sassを記述するのにcompassを使うと便利、というか標準みたいなのでcompassも使います。

### sassのインストール
```
gem install sass

gem list sass
sass (3.2.12)

gem which sass
/home/hoge/.rbenv/versions/1.9.3-p125/lib/ruby/gems/1.9.1/gems/sass-3.2.12/lib/sass.rb
```

### compassのインストール
```
gem install compass

gem list compass
compass (0.12.2)

gem which compass
/home/noguchi/.rbenv/versions/1.9.3-p125/lib/ruby/gems/1.9.1/gems/compass-0.12.2/lib/compass.rb
```

### パスを通す
パスが通っていなかったので、パスの通っている  
$HOME/.rbenv/shims/以下に同じ名前のスクリプトを置きました。
```
cp -a $HOME/.rbenv/shims/rake $HOME/.rbenv/shims/sass
cp -a $HOME/.rbenv/shims/rake $HOME/.rbenv/shims/compass
```

### プロジェクトの作成
```
compass create test --sass-dir "sass" --css-dir "css" --javascripts-dir "js" --images-dir "img"
tree test
test
├── config.rb
├── css
│   ├── ie.css
│   ├── print.css
│   └── screen.css
└── sass
    ├── ie.scss
        ├── print.scss
            └── screen.scss
```

### 既存のプロジェクトにCompassプロジェクトを追加する
```
compass init <ディレクトリ名>
```

### Sassファイルのコンパイル
```
compass compile
```
### ファイル変更時、自動でコンパイルする
```
compass watch
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [CompassとSassの基礎の覚書](http://parashuto.com/rriver/development/compass-sass-basics-installation-and-tools)
* [Compassで本番用と開発用で設定を振り分けてみる](http://howtohp.com/css/compass-environment_setting.html)
* [Sass/Compassの社内運用に関するありがたいスライドから学んだことのまとめ](http://parashuto.com/rriver/development/utilizing-sass-compass-in-an-organization)
