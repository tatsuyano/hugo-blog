+++
date = "2013-05-09T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog"]
title = "octopressをインストールする"
slug = "octopress-install"
+++

![](https://dl.dropboxusercontent.com/u/159938/blog_images/octopress_001.png)

## Octopressとは

github上にブログを構築できるruby製のフレームワーク。

## インストール

複数PCで更新したいので、Dropbox上にインストール。手順は[公式サイト](http://octopress.org/docs/setup/)と同じです。  
事前にrbenvかRVMでruby1.9.3以上をインストールしておいてください。  

```
cd $HOME/Dropbox/
git clone git://github.com/imathis/octopress.git octopress
cd octopress
gem install bundler
rbenv rehash
bundle install
```

### rake インストール時にエラー
rakeをインストールしたさいにOctopressで使うバージョンが違うというエラーがでた。  

```
rake install
rake aborted!
You have already activated rake 10.0.4, but your Gemfile requires rake 0.9.2.2. Using bundle exec may solve this.
```

gemのバージョンは、Gemfileに設定されているので、rakeのバージョンを 10.0 から 0.9に変更し、再度 rake install を行う

octopress/Gemfile

``` ruby
source "http://rubygems.org"

group :development do
  gem 'rake', '~> 0.9' # 10.0 -> 0.9
  gem 'jekyll', '~> 0.12'
```

### インストールの確認

```
rake preview
```
とコマンドを打ち、ブラウザからlocalhost:4000にアクセスしてください。
正常にインストールされていれば、記事が0件のOctopressが表示されるはずです。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/octopress_002.png)

## 使い方、最低限のコマンド

* 記事の生成

octopress/source/_posts/以下にmarkdown形式のファイルが生成される
```
rake new_post ["hoge"]
```

* 記事の確認

ブラウザからlocalhost:4000で、生成した記事を確認できる
```
rake preview
```

* 記事の再構築

記事を変更すると自動で更新されるが、まれに更新されない場合がある。その場合は、手動で更新(再構築)する。
```
rake generate
```

### rake preview実行後、logに regeneration: 1 files changed が流れ続ける
[Octopressでpreviewしていたらregeneration: 1 files changedが終わらない場合の対処方法](http://blog.satooshi.jp/blog/2013/04/09/octopress-preview-never-stop-auto-regeneration)に解決方法がかいてありました。
これもgemのバージョン違いが原因のようです。  
  
octopress/Gemfile

``` ruby
source "http://rubygems.org"

group :development do
...
  gem 'directory_watcher', '~> 1.4.1' # 1.5.1 -> 1.4.1
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [公式サイト](http://octopress.org/)
* [GitHub pages + Octopressの導入](http://rcmdnk.github.io/blog/2013/03/07/setup-octopress/)
* [Octopressはじめました](http://www.miukoba.net/blog/2013/01/05/start-octopress/)
* [GithubとOctopressでモダンな技術系ブログを作ってみる](http://blog.glidenote.com/blog/2011/11/07/install-octopress-on-github)
* [Octopressでpreviewしていたらregeneration: 1 files changedが終わらない場合の対処方法](http://blog.satooshi.jp/blog/2013/04/09/octopress-preview-never-stop-auto-regeneration)
