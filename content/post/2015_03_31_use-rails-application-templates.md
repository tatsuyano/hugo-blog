+++
date = "2015-03-31T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails"]
title = "アプリケーションテンプレートを使って、プロジェクトのひな形を作る"
slug = "use-rails-application-templates"
+++

Railsアプリを作っていると、ある程度決まったRuby Gemを使うようになってきたり、
同じ設定(config周りとか)を繰り返すようになってきます。

そこでRailsのアプリケーションテンプレート機能を使うと、
そういった決まった作業をコード化することができるようになります。

今回は、主にデータベースをPostgreSQL、テストFWをRSpec、CSSFWにTwitterBootStrapの初期設定を行い、最後に`git commit -m 'Initial commit'`するまでのアプリケーションテンプレートを作ってみたいと思います。

## 作業の流れ

1. templateファイルを作成
2. templateファイルをGitHubなどにpush
3. templateファイルを使って、Railsアプリを作ってみる

## template.rb

コードを実際見てもらえばわかるように、templateファイルといっても特別なものではなく、
Gemfileと同じように、必要なGemを指定し、実際の作業を明文化したファイルになります。

※ 最新のソースは[こちら](https://github.com/tatsuyano/rails-template)

<pre><code class="language-ruby"># -*- coding: utf-8 -*-

@app_name = app_name

gem 'rails', '4.2.0'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'annotate' # modelクラスにスキーマ情報の注釈をつける
gem 'bootstrap-sass' # bootstrap

gem_group :development, :test do
  gem 'spring'
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'rspec-rails'
  gem 'guard-rspec'                        # railsでguardを使うためのGem
  gem 'spring-commands-rspec' , '~> 1.0.2' # springでキャッシュした状態でguardを使うためのGem
  gem "factory_girl_rails" , "~> 4.4.1"    # テストデータの作成
end

gem_group :test do
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'shoulda-matchers', require: false # rspecで使うmatcher
  gem "faker" , "~> 1.4.3"              # 名前やメールアドレス、その他のプレースホルダをファクトリに提供
  gem "database_cleaner" , "~> 1.3.0"   # まっさらな状態で各specが実行できるように、テストデータベースのデータを掃除
#  gem "capybara" , "~> 2.4.3"           # ユーザとWebアプリケーションのやりとりをプログラム上でシミュレートできる
#  gem "launchy" , "~> 2.4.2"            # 好きなタイミングでデフォルトのwebブラウザを開く
#  gem "selenium-webdriver" , "~> 2.43.0"# ブラウザ上でJavaScriptを利用する機能をCapybaraでテストできる
end

gem_group :production do
  gem 'rails_12factor'
end

# rspec initalize setting
run 'bundle install'
run 'rm -rf test'
generate 'rspec:install'

# guard initalize setting
run 'bundle exec spring binstub rspec'
run 'bundle exec guard init rspec'

# rm unused files
run "rm README.rdoc"

# database
run 'rm config/database.yml'

database_yml = <<-FILE
default: &default
  adapter: postgresql
  encoding: unicode
  pool: 5
development:
  <<: *default
  database: #{@app_name}
  username: postgres
  password: postgres
test:
  <<: *default
  database: #{@app_name}_test
  username: postgres
  password: postgres
FILE
  
File.open("config/database.yml","w") do |file|
  file.puts database_yml
end

run 'bundle exec rake db:create'
run 'rails g annotate:install'

# config/application
environment "config.time_zone = 'Tokyo'"
environment "config.active_record.default_timezone = :local"

# bootstrap
run "rm app/assets/stylesheets/application.css"
run "rm app/assets/javascripts/application.js"

File.open("app/assets/stylesheets/application.scss","w") do |file|
  file.puts <<-SCSS
@import "bootstrap-sprockets";
@import "bootstrap";
SCSS
end

File.open("app/assets/javascripts/application.js","w") do |file|
  file.puts <<-JS
//= require jquery
//= require bootstrap-sprockets
JS
end

# git initalize setting
after_bundle do
  git :init
  git add: '.'
  git commit: %Q{ -m 'Initial commit' }
end
</code></pre>
<br>

## 確認

まずはテンプレートにエラーがないかに、Githubにpushする前に直接指定し`rails new`してみます。

<pre><code class="language-bash">$ rails new app_name -m template.rb
</code></pre>

エラーがでなければ、Githubにpushして、今後はURLを指定して`rails new`してください。

<pre><code class="language-bash">$ rails new app_name -m
https://raw.githubusercontent.com/tatsuyano/rails-template/master/template.rb
</code></pre>
<br>

テンプレートを一度作るまでは面倒ですが、作業がコード化できるなど、メリットが大きいので作ることをおすすめします。

## 参考サイト

* [Rails Templateでプロジェクト作成を楽にしよう](http://nanapi.co.jp/blog/2015/01/27/rails-template/)
* [RailsのApplication templateを使って開発の初速をあげよう！](http://qiita.com/tachiba/items/26b2e9dc271bd8e6907d)
* [Rails のアプリケーションテンプレート](http://railsguides.jp/rails_application_templates.html)
* [Rails 4.2 + Bootstrap の Application Template
1コマンドでモダンRailsが！](http://morizyun.github.io/blog/rails4-application-templates-heroku/)
* [tatsuyano/rails-template](https://github.com/tatsuyano/rails-template)
