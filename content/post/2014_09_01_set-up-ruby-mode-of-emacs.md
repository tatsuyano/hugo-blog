+++
date = "2014-09-01T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "ruby", "rspec"]
title = "emacsのruby環境をセットアップする"
slug = "set-up-ruby-mode-of-emacs"
+++

最近`cask`に切り替えたので、ruby環境も一から作り直したいと思います。
オムニ補完には、今までは`Rsense`を使っていましたが、今回は`robe`というパッケージと使ってみたいと思います。  
*対象のEmacsのバージョンは24.3.1になります*

### 今回インストールするパッケージ

* ruby-mode
* ruby-end
* ruby-block
* inf-ruby
* robe
* auto-complete
* rvenv 　　　　　　# 2015/02/23追記
* smart-newline　# 2015/02/23追記
* rspec-mode 　　# 2015/02/23追記

### ruby-mode,ruby-end,ruby-blockをインストール

まずは基本的なところから。
Emacs24だと`ruby-electric`がうまく動かなかったので、
今回はインストールしませんでしたが、`ruby-end`だけで事足りました。

~/.emacs/Cask

    ;; Ruby
    (depends-on "ruby-mode")
    (depends-on "ruby-end")
    (depends-on "ruby-block")

~/.emacs.d/init_loader/03_ruby.el

    ;; --------------------------------------------------
    ;; ruby-mode
    ;; http://shibayu36.hatenablog.com/entry/2013/03/18/192651
    ;; --------------------------------------------------
    (autoload 'ruby-mode "ruby-mode"
      "Mode for editing ruby source files" t)
    (add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
    (add-to-list 'auto-mode-alist '("Capfile$" . ruby-mode))
    (add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
    (add-to-list 'interpreter-mode-alist '("ruby" . ruby-mode)) ;; shebangがrubyの場合、ruby-modeを開く

    ;; ruby-modeのインデントを改良する
    (setq ruby-deep-indent-paren-style nil)
    (defadvice ruby-indent-line (after unindent-closing-paren activate)
      (let ((column (current-column))
            indent offset)
        (save-excursion
          (back-to-indentation)
          (let ((state (syntax-ppss)))
            (setq offset (- column (current-column)))
            (when (and (eq (char-after) ?\))
                       (not (zerop (car state))))
              (goto-char (cadr state))
              (setq indent (current-indentation)))))
        (when indent
          (indent-line-to indent)
          (when (> offset 0) (forward-char offset)))))

    ;; --------------------------------------------------
    ;; ruby-end
    ;; endや括弧などを自動挿入する
    ;; http://blog.livedoor.jp/ooboofo3/archives/53748087.html
    ;; --------------------------------------------------
    (require 'ruby-end)
    (add-hook 'ruby-mode-hook
      '(lambda ()
        (abbrev-mode 1)
        (electric-pair-mode t)
        (electric-indent-mode t)
        (electric-layout-mode t)))

    ;; --------------------------------------------------
    ;; ruby-block
    ;; endにカーソルを合わせると、そのendに対応する行をハイライトする
    ;; --------------------------------------------------
    (require 'ruby-block)
    (ruby-block-mode t)
    (setq ruby-block-highlight-toggle t)

### auto-completeのインストール

`cask`でインストールすると、辞書が格納されるディレクトリ(dict)のパスが
`~/.emacs.d/.cask/24.3.1/elpa/auto-complete-20140824.1658/dict"`になりました。

~/.emacs/Cask

    ;; Auto-complete
    (depends-on "auto-complete")
    (depends-on "fuzzy")
    (depends-on "popup")

~/.emacs.d/init_loader/02_auto-complete.el

    (require 'auto-complete-config)
    (add-to-list 'ac-dictionary-directories "~/.emacs.d/.cask/24.3.1/elpa/auto-complete-20140824.1658/dict")
    (ac-config-default)
    (setq ac-use-menu-map t)

### robeのインストール

ここからが本題のrobeのインストールです。
`robe`を使うには、`pry`という`Gem`が必要になりますが、
`pry`を使うためには、`read-line`ライブラリをインストール時にオプション指定したRubyが必要になります。

今回すでに、`read-line`が入っていない状態でRubyをインストールしていたので、
再度インストールし直したいと思います。

#### pryのインストール

    $ gem install pry
    $ source $HOME/.zshrc

    $ which pry
    $HOME/.anyenv/envs/rbenv/shims/pry

`read-line`ライブラリを読み込めないため、`pry`が実行できない

     $ pry
     Sorry, you can't use Pry without Readline or a compatible library.
     Possible solutions:
      * Rebuild Ruby with Readline support using `--with-readline`
      * Use the rb-readline gem, which is a pure-Ruby port of Readline
      * Use the pry-coolline gem, a pure-ruby alternative to Readline

* [Pry起動時にエラーが出る場合の対処法](http://qiita.com/Tamadon/items/347baf2ba89ccde89624)
* [Rubyistよ、irbを捨ててPryを使おう](http://labs.timedia.co.jp/2011/12/rubyist-should-use-pry.html)

#### readlineのインストールとRubyの再インストール

    $ sudo yum -y install readline-devel
    $ rbenv uninstall 2.1.0
    $ rbenv install 2.1.0

*注意*  
Ruby本体の再インストールをしたため、`pry`や`bundle`なども削除されていますので、
再度インストールしてください。

    $ gem install pry pry-doc bundler
    $ source $HOME/.zshrc

#### emacsの設定

`robe`は内部で`inf-ruby`を使っているので、`robe`をインストールすると、
`inf-ruby`も自動でインストールされます。

~/.emacs/Cask

    (depends-on "robe")

~/.emacs.d/init_loader/03_ruby.el

    ;; --------------------------------------------------
    ;; robe
    ;; http://codeout.hatenablog.com/entry/2014/02/04/210237
    ;; --------------------------------------------------
    (add-hook 'ruby-mode-hook 'robe-mode)
    (autoload 'robe-mode "robe" "Code navigation, documentation lookup and completion for Ruby" t nil)
    (autoload 'ac-robe-setup "ac-robe" "auto-complete robe" nil nil)
    (add-hook 'robe-mode-hook 'ac-robe-setup)

#### robeの起動方法

`robe`を利用するには、

1. `M-x inf-ruby`を実行し、裏で`irb(pry)`を起動させ、
2. `M-x robe-start`で初めて`robe`が使えるようになります。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/set-up-ruby-mode-of-emacs_01.jpg)

#### Gemfileを用意してirb(pry)を裏で自動に起動させる

編集したいファイルと同じディレクトリに`pry`のための`Gemfile`を用意しておけば、
`M-x robe-start`だけで`robe`をつかうことができます。

    $ bundle init

Gemfile

    source "https://rubygems.org"

    gem "pry"
    gem "pry-doc",       ">= 0.6.0"
    gem "method_source", ">= 0.8.2"

* [robe:Dependencies](https://github.com/dgutov/robe#dependencies)

### rbenvのパスを通す
*2015/02/23 追記*

Emacsにrbenvのパスを通してくれます。

~/.emacs.d/initloader/03_ruby.el
<pre><code class='language-ruby'> (require 'rbenv)
 (global-rbenv-mode)
 (setq rbenv-installation-dir "~/.anyenv/envs/rbenv")
</code></pre>

* [モダンなEmacsを求めて (6) Rubyについて](http://block-given.hatenablog.com/entry/2014/11/12/005657)

### smart-newlineを導入する
*2015/02/23 追記*

改行をよしなにやってくれます。素敵です。

~/.emacs.d/initloader/03_ruby.el
<pre><code class='language-ruby'> (add-hook 'ruby-mode-hook
   (lambda ()
   (smart-newline-mode t)))
</code></pre>

* [smart-newline.elという拡張](http://ainame.hateblo.jp/entry/2013/12/08/162032)

### rspec-modeを導入する
*2015/02/23 追記*

テストとその対象のコードを簡単に切り替えてくれたり、カーソルのある行だけのテストを実行してくれる。
キーバインドにカンマが入っていることに注意。

~/.emacs.d/initloader/03ruby.el
<pre><code class='language-ruby'> ;; C-c , v RSpec実行
 ;; C-c , s カ-ソルが当たっているサンプルを実行
 ;; C-c , t Specとソースを切り替える
 (require 'rspec-mode)
 (custom-set-variables '(rspec-use-rake-flag nil))
</code></pre>

* [EmacsでRSpec環境をめちゃガチャパワーアップしたまとめ](http://futurismo.biz/archives/2266)

お疲れ様でした。

### 参考サイト

* [emacsのruby環境を整えています](http://shibayu36.hatenablog.com/entry/2013/03/18/192651)
* [Emacs24 で ruby-electric的なruby-modeを実現するには](http://blog.livedoor.jp/ooboofo3/archives/53748087.html)
* [auto-complete + rsense の代わりに auto-complete + robe をつかう](http://codeout.hatenablog.com/entry/2014/02/04/210237)
* [Pry起動時にエラーが出る場合の対処法](http://qiita.com/Tamadon/items/347baf2ba89ccde89624)
* [Rubyistよ、irbを捨ててPryを使おう](http://labs.timedia.co.jp/2011/12/rubyist-should-use-pry.html)
* [robe:Dependencies](https://github.com/dgutov/robe#dependencies)

### 関連する記事

* [anyenvをインストールする](http://blog.10rane.com/2014/08/19/install-anyenv/)
* [init-loaderをCaskでインストールする](http://blog.10rane.com/2014/08/28/how-to-install-and-setup-initloader-in-cask/)
