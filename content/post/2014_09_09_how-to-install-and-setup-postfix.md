+++
date = "2014-09-09T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["middleware"]
title = "Postfixをインストールする"
slug = "how-to-install-and-setup-postfix"
+++

仕事ではじめて Postfix を使ったので、インストール方法などを残しておきます。

### Postfixのインストール

まず Postfix のインストール前に Sendmail がインストールされている場合は止めておきます。

```
$ sudo chkconfig sendmail off
$ sudo /etc/init.d/sendmail stop

$ sudo yum -y install postfix
$ sudo chkconfig --add postfix
$ sudo chkconfig on postfix
$ sudo /etc/init.d/postfix start
```

メールサーバー(MTA)を postfix に切り替え
```
$ sudo update-alternatives --set mta /usr/sbin/sendmail.postfix
```
<br>

### postfix の設定

設定は主に`/etc/postfix/`配下の3ファイル

#### /etc/postfix/main.cf (設定全般)
```
# mydomainを設定しない場合、メールのホスト名としても使われる(と思われる)
myhostname = hogehoge.net

# メールをローカルで受信するドメイン名を設定
mydestination = $myhostname

# 全てのメールを受け取る
inet_interfaces = all

# ipv6、ipv4 両方許可する
inet_protocols = all

# コメントアウト
#mynetworks_style = host

# LAN内のネットワークを指定
mynetworks = 127.0.0.0/8

# Telnet等でアクセスした際にpostfixのバージョン情報の表示
smtpd_banner = $myhostname ESMTP SERVER...

# 許可
allow_mail_to_commands = alias,forward,include
allow_mail_to_files = alias,forward,include
allow_min_user = yes

# 別のドメイン mail.hogehoge.net でも受信する
virtual_maps = hash:/etc/postfix/virtual

# 受信したメールを正規表現(regexp)で指定ユーザに振り分ける
alias_maps = hash:/etc/aliases,regexp:/etc/postfix/aliases.reg
```
<p>
#### /etc/postfix/aliases.reg (メールを受信するユーザの設定)
```
/^support-[0|1](@hogehoge.net)?$/ support
/^support(@hogehoge.net)?$/ support
```
メールアドレスにパラメータを含めたい場合、`aliases.reg`に正規表現を使って、受信したメールを指定のユーザ(メールアドレス)に送ることができる。  
例では `support-1@hogehoge.net`宛のメールを`support@hogehoge.net`と同じユーザ(メールアドレス)に送る。 

<p>
#### /etc/postfix/virtual (ドメインの設定)
```
@mail.hogehoge.net  support
```
サブドメインなど、別ドメインも指定したい場合に設定する。  
例では、`@mail.hogehoge.net`というドメインに来たアドレスも`support`ユーザに配送されるようにしている。
<p>

また`virtual`の設定を反映させるには、`postmap`コマンドで`virtual.db`を作成し、postfix を再起動する。

```
$ sudo postmap /etc/postfix/virtual
$ sudo /etc/init.d/postfix reload
```
<br>

### 参考サイト

* [Postfix の設定 - 基本](http://www.postfix-jp.info/trans-2.0/jhtml/basic.html)
* [Postfixでのドメイン名やホスト名の設定について](http://www.uetyi.mydns.jp/wordpress/postfix-setting/entry-476.html)
* [Postfixによるメールサーバーの構築](http://linux.kororo.jp/cont/server/postfix.php)
* [postfix の alias を正規表現で指定したいときの注意点](http://blog.honestyworks.jp/blog/archives/193)
* [Postfix で複数ドメインの管理](http://linux.kororo.jp/cont/server/postfix_virtual.php)
