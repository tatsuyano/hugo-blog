+++
date = "2013-05-01T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["mac"]
title = "tmuxでもpbcopy、pbpasteを使えるように設定する"
slug = "pbcopy-pbpaste"
+++

最近しりましたpbcopyとpbpasteをtmuxで使う方法です。

## pbcopyとpbpasteの使い方

```
ls -la | pbcopy    # 標準出力 -> クリップボード
pbpaste > hoge.txt # クリップボード -> 標準出力
```

## pbcopyとpbpasteをtmuxで使うための設定
tmuxで、pbcopyとpbpasteが正しく動かない。

```
brew install reattach-to-user-namespace
```

.tmux.conf
```
set -g default-command 'reattach-to-user-namespace -l zsh'
```

## tmuxにpbcopyのキーバインドを追加

[Mac の tmux でクリップボードを使う。](http://koseki.hatenablog.com/entry/20110816/TmuxCopy)のサイトを参考にpbcopyのキーバインドを追加しました。

.tmux.conf
```
# buffer copy
bind C-q run-shell 'reattach-to-user-namespace tmux-pbcopy'
```

emacs $HOME/Dropbox/dotfile/mybin/tmux-pbcopy
``` scheme
#! /bin/sh

tmux save-buffer /tmp/.tmux_to_pbcopy
cat /tmp/.tmux_to_pbcopy | pbcopy

# 以下はメッセージを表示するだけ。無くてもOK。
HEAD=`head -1 /tmp/.tmux_to_pbcopy`
TAIL=`tail -1 /tmp/.tmux_to_pbcopy`
tmux display-message "Copy:  $HEAD  -  $TAIL"
```

## 使い方

* C-z C-[ でコピーモードに入る。
* C-SPCで選択開始
* C-wでバッファにコピー
* C-z C-qでバッファの内容をクリップボードにコピー

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [ターミナルで使える「pbcopy」「pbpaste」って知ってました？](http://www.ideaxidea.com/archives/2011/04/macosx_terminal_tips.html)
* [Mac の tmux でクリップボードを使う。](http://koseki.hatenablog.com/entry/20110816/TmuxCopy)
* [homebrew で最低限これだけはいれておけってやつ](http://blog.livedoor.jp/xaicron/archives/54458405.html)
