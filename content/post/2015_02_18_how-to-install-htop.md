+++
date = "2015-02-18T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["linux"]
title = "htopコマンドをインストール"
slug = "how-to-install-htop"
+++

centos6とamazon-linuxにhtopをインストールしたときのメモです。

## yumでインストール

<pre><code class="language-bash"># wget http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
# rpm -Uhv rpmforge-release*.rf.x86_64.rpm
# yum --enablerepo=epel install -y htop
</code></pre>
<br>

## 参考サイト

* [top の代わりに htop を使ってみた。](http://loumo.jp/wp/archive/20121217084642/)
* [Install htop in CentOS](http://www.dreamcreative.net/2013/07/install-htop-in-centos/)
* [htopを使ってサーバのプロセスの状態を詳しく見る方法](http://www.submit.ne.jp/379)
* [htopのCPU付加とメモリ使用量の色の意味](http://rishida.hatenablog.com/entry/2013/09/21/054234)
