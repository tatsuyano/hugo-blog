+++
date = "2017-09-18T20:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["blog"]
title = "hugoを再インストールした"
slug = "re-installed-hugo"
+++

久々にblogに記事を書こうと新しいmacにhugoをインストールしたが、まったく動かなかっていたので自分用にメモ。  

ちなみに構成は、Hugo本体をbitbucketのリポジトリに非公開で管理し、生成した記事(HTML)をgithub pagesで管理している。  

## Install Hugo
今回はhomebrewでインストール。
``` shell
$ brew install hugo
$ hugo version
Hugo Static Site Generator v0.27.1 darwin/amd64 BuildDate: 2017-09-16T19:35:01+09:00
```
* [Quick Start](https://gohugo.io/getting-started/quick-start/)

## Add submodule
まずgithub pageでブログを公開するために、github上に生成したHTMLを管理するリポジトリを`submodule`として管理している。

そもそもsubmoduleの仕組みだが、`git submodule add`すると、リポジトリの直下に`.gitmodules`、`.git/config`、`.git/modules`の３箇所に記載、ファイルが追加される。

しかし今回のように、すでにある(add moduleした)リポジトリをcloneした場合`.git/modules`中にファイルが存在しない。
なので一旦`git rm public`でsubmoduleを削除し(すると上記３ファイルの記載が消える)、再度`git submodule add`する。
``` shell
$ git rm public
$ git submodule add git@github.com:tatsuyano/tatsuyano.github.io.git

##.gitsubmodules
[submodule "public"]
        path = public
        url = git@github.com:tatsuyano/tatsuyano.github.io.git

## .git/config
[submodule "public"]
    url = git@github.com:tatsuyano/tatsuyano.github.io.git

## git/modules
$ ll .git/modules/
drwxr-xr-x  13 noguchi  staff   442B Sep 18 19:50 public //<-追加される
drwxr-xr-x   4 noguchi  staff   136B Sep 18 20:20 themes
```
* [Git のさまざまなツール - サブモジュール](https://git-scm.com/book/ja/v1/Git-%E3%81%AE%E3%81%95%E3%81%BE%E3%81%96%E3%81%BE%E3%81%AA%E3%83%84%E3%83%BC%E3%83%AB-%E3%82%B5%E3%83%96%E3%83%A2%E3%82%B8%E3%83%A5%E3%83%BC%E3%83%AB)

## version 0.15から静的ファイル(public)が自動生成されなくなった
そのためまず`hugo server --renderToDisk=true`でpublic配下にHTMLファイルを生成してから、deploy(githubにpush)する必要がでてきた。
``` shell
$ hugo version
Hugo Static Site Generator v0.27.1 darwin/amd64 BuildDate: 2017-09-16T19:35:01+09:00

$ hugo server --renderToDisk=true
```
* [Hugo の public フォルダへの生成が v0.15 から変更に](http://lab.geo.jp/blog/other/hugo-public-v015.html)

## ローカルで確認してからdeploy
`hugo server`でローカル( http://localhost:1313 )で確認し終わったら、いつもどおり`deploy.sh`でgithubにpushし、github pagesでblogを公開する。
``` shell
$ sh .deploy.sh
```
