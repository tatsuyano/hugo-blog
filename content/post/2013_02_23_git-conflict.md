+++
date = "2013-02-23T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["git"]
title = "conflictしたファイルを修正しpushする"
slug = "git-conflict"
+++

pullした後に、commitしたファイルdummy.txtがconflictした場合

```
git pull
Auto-merging dummy.txt # <- dummy.txtがconflictして、auto-mergeされた
CONFLICT (content): Merge conflict in dummy.txt
Automatic merge failed; fix conflicts and then commit the result.
```

conflictしたファイルを修正する

```
emacs dummy.txt
<<<<<<< HEAD
local
=======
remote
>>>>>>> 58da51ee64bf7f793167ddb40d035815e39566b4
```

修正後、git commit -aで再度commitしpush

```
git commit -a -m 'merge dummy'
[master 102b6c6] merge dummy

git push
```
git commit -a は git add -u した後すぐにgit commitしたのと同義


conflictしているファイルの表示

```
git ls-files -u
```
