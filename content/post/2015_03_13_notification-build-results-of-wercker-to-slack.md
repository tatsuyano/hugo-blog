+++
date = "2015-03-13T02:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["ruby", "rails", "ci", "wercker", "slack"]
title = "WerckerのBuild結果をSlackに通知する"
slug = "notification-build-results-of-wercker-to-slack"
+++

Werckerに登録したアプリケーションのBuild結果をSlackのwebHookという機能を使うことで、Slackに通知することができるようになります。

## SlackにWebHookを追加する

1. サイドメニューの  Configure Integrations を押下してください。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_01.png)

2. `Incoming WebHooks`を追加してください。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_02.png)

3. どのチャンネルに追加するか選択してください。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_03.png)

4. URLをコピーし、`Save Settings`を押下します。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_04.png)

## WerckerにSlackのTOKENを設定する

先ほどコピーしたURLの一番後ろについているパラーメータがSLACK_TOKENになります。

`https://hooks.slack.com/services/aaaaa/bbbbb/[SLACK-TOKEN]`

`SLACK_TOKEN`は`Protected`にチェックした状態でSAVEしてください。`Protected`をチェックすると、ログ上で値が非表示になります。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_05.png)

## wercker.ymlに`after-steps`を追加

Build後(build: after-steps)に、Slackに通知するようwercker.ymlを設定します。`subdomain`はSlackのURL`http://hoge.slack.com`の`hoge`の部分にあたります。
<pre><code class='language-ruby'>box: wercker/ruby
 services:
     - wercker/postgresql
 build:
     steps:
       - bundle-install
       - rails-database-yml:
           service: postgresql
       - script:
           name: echo ruby information
           code: |
               echo "ruby version $(ruby --version) running!"
               echo "from location $(which ruby)"
               echo -p "gem list: $(gem list)"
       - script:
           name: Set up db
           code: RAILS_ENV=test bundle exec rake db:schema:load
       - script:
           name: Run RSpec
           code: bundle exec rspec
     # ここから追加 ------------------------------
     after-steps:
       - sherzberg/slack-notify:
           subdomain: hoge
           token: $SLACK_TOKEN
           channel: "#general"
           username: wercker
           icon_url: https://avatars3.githubusercontent.com/u/1695193?s=140
     # ここまで------------------------------------
 deploy:
     steps:
         - heroku-deploy:
             install-toolbelt: true
         - script:
             name: Update database
             code: heroku run rake db:migrate --app $APP_NAME
         - script:
             name: Update assets
             code: heroku run rake add_static_assets --app $APP_NAME
</code></pre>
<br>

wercker.yml を更新したので`git push`してください。
正しく設定できれていれば、下記のようなメッセージがSlackに通知されます。
![](https://dl.dropboxusercontent.com/u/159938/blog_images/notification-build-results-of-wercker-to-slack_06.png)

## 参考サイト

* [Wercker|ビルド結果をSlackに通知する](http://qiita.com/tbpgr/items/7705995c9f679d97a382)
* [wercker-step-slack-notify:github](https://github.com/sherzberg/wercker-step-slack-notify)
