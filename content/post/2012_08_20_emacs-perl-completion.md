+++
date = "2012-08-20T04:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "perl"]
title = "perl-completion.elをインストールする"
slug = "emacs-perl-completion"
+++

## はじめに
  
すでにauto-intall.elがインストールされている前提で話をすすめます。  
auto-installのインストール手順は[こちら](/2012/08/20/emacs-auto-install/)をご覧ください。  

## インストール手順

emacsを起動し、

```
M-x auto-install-batch <RET>
Extension name: perl-completionn <RET>
```

auto-isntall-batchを実行、次にインストールしたいパッケージ名(perl-completion)を入力します。  
  
すると自動で複数ソースのダウンロードが始まります。  
ダウンロードし終わると「モード行」に  

```
Type C-c C-c to continue; Type C-c C-d for view diff; Type C-c C-q to quit.
```

というメッセージが出てくるので、「C-c C-c」と入力し、ダウンロードしたソースを順番にコンパイルしていきます。  
すべてコンパイルし終えると、以下のようなメッセージが表示され、インストールが完了します。  

```
Installation is completed.
```

## .emacsの設定

cperl-mode時に、auto-completion(とauto-complete)を有効にするように設定します。  

``` scheme
;; perl-completion
(add-hook  'cperl-mode-hook (lambda ()
                              (require 'auto-complete)
                              (require 'perl-completion)
                              (add-to-list 'ac-sources 'ac-source-perl-completion)
                              (perl-completion-mode t)))
```

## 実行画面

![](https://dl.dropboxusercontent.com/u/159938/blog_images/perl-completion001.png)
