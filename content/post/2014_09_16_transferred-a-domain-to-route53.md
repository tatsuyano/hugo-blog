+++
date = "2014-09-16T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["aws"]
title = "Route53にドメインを移管する"
slug = "transferred-a-domain-to-route53"
+++

Route53のDNSを使うようにしたので、続けてドメインも移管したいと思います。

### AuthCodeの取得

value-domainから`AuthCode`を取得します。

まずは`AuthCode`を取得する前に、whois代行を利用している場合は
先に自分の情報に変更します。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_1.jpg)

whois情報を自分の情報に変更後、一度saveをし、
再度同じページを開くと、画面下側に`AuthCode`が表示されています。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_2.jpg)

### ドメインの移管

ドメインの移管時にドメインが利用している`NS`を入力するので、事前に確認しておきます。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_3.jpg)

次にドメインがRoute53で移管できるか確認します。
ちなみに`.com`ドメインの移管料は$12.00でした。

*移管をすると、ドメインの利用期間が1年伸びます*

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_4.jpg)

whois情報を登録します。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_5.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_6.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_7.jpg)

「Thank you for transferring your domain to Route 53」とメッセージがでれば完了です。

![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_8.jpg)
![](https://dl.dropboxusercontent.com/u/159938/blog_images/route53_domain_9.jpg)

登録完了後、数日後に確認メールが来るのでaccept(許可)してください。  
ちなみに移管が完了するまでに、7日かかりました。。

以上で完了です。お疲れ様でした。

### 参考サイト
* [Amazon Route 53にドメインを移管してみた](http://yoshidashingo.hatenablog.com/entry/2014/08/01/132312)
* [Value Domainからお名前.comへドメインを移管してみた](http://server-domain.info/reportd/transfer-value-domain-onamae-com.html)

### 関連する記事
* [Route53のDNSを使う](http://blog.10rane.com/2014/09/11/using-the-dns-of-route53/)
* [aws はじめました。](http://blog.10rane.com/2014/07/21/start-aws/)
