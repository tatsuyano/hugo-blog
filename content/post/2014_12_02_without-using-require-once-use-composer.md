+++
date = "2014-12-02T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "require_onceを使わず、Composerを使う"
slug = "without-using-require-once-use-composer"
+++

毎回`require_once`を書くのがだるかったので、ローカルのパッケージを自動で読み込む方法を調べてみたら、
PHP5.3以上ならComposerが使えることを知ったので試してみた。

ただ前提として読み込まれる側のソース(パッケージ)は、Gitなど
バージョンコントロールシステム(vcs)で管理されている必要がある。

### Composer のインストール

まずはComposerのインストールから。
今回はすでにpathの通っている`/usr/local/bin`にインストールする。

<pre><code class="language-bash">$ sudo bash
# cd /usr/local/bin
# curl -sS https://getcomposer.org/installer | php

$ which composer.phar
/usr/local/bin/composer.phar
</code></pre>
<br>

### ローカルパッケージの構成

読み込まれる(require_onceされる)側の構成は以下のようにした。

<pre><code class="language-bash">composer-test
│
├── composer.json
└── src
    └── Tatsuyano
        ├── ParentClass
        │   └── ChildClass.php
        └── ParentClass.php
</code></pre>

まずは作成したパッケージ(`composer-test`)をかならず`git commit`する。  
というのも、たとえ読み込む側、読み込まれる側、両方が同じローカル環境内にあっても
読み込む側は、Git 経由でソースを読み込む(インストールする)ので、
`commit`した内容しか反映されない。

#### composer.json

<pre><code class="language-bash">{
    "name": "composer-test/tatsuyano",
    "description": "Composerを使ってのファイルのロードテスト",
    "license": "MIT",
    "autoload": {
        "psr-0": {
            "Tatsuyano\\": "src/"
        }
    }
}
</code></pre>
<br>

#### ParentClass.php

<pre><code class="language-bash">namespace Tatsuyano;

class ParentClass
{
    public static function name()
    {
        return "ParentClass";
    }

    public static function nameOfChild()
    {
        return ParentClass\ChildClass::name();
    }

    public static function child($class_name = 'ChildClass')
    {
        # namespaceを切ったクラスを生成する場合、'\'で区切る必要があるが
        #クラスを動的に生成する場合は、sprintfで指定しないと
        #シンタックスエラーになる
        $class = sprintf("Tatsuyano\ParentClass\%s",$class_name);
        return new $class();
    }
}
</code></pre>

あえて`use`は使わずに namespace をすべて指定している。
結局`use`を使いまくると`require_once`を使いまくるのと同じになってしまうのと、
動的にクラスを生成したい場合、実装時にクラスを明示(`use`)できないため。

#### ChildClass.php

<pre><code class="language-bash">namespace Tatsuyano\ParentClass;

class ChildClass
{
    public static function name()
    {
        return "ChildClass";
    }

    public function say()
    {
        return "hello";
    }
}
</code></pre>
<br>

### パッケージの読み込み

読み込む側の構成は以下とする。

<pre><code class="language-bash">.
├── composer.json
├── test.php #-> composer-testパッケージを利用する側のソース
└── vendor #-> Composerでパッケージをインストールしたあとに生成される
    ├── autoload.php
    └── composer
    └── composer-test #-> インストールされたパッケージ
</code></pre>

パッケージを読み込む(インストール)には
<pre><code class="language-bash">$ composer.pear update
</code></pre>

ちなみに、`vendor`ディレクトリ以下にインストールしたソースを直接編集したあとに
再度`$ composer.pear update`しても、編集したソースは更新(上書き)されない。

#### composer.json

`repositories/url`ディレクティブには、
読み込まれる側の`composer.json`が入っているディレクトリを指定する。

<pre><code class="language-bash">{
    "repositories": [
        {
            "type": "vcs",
            "url": "/home/noguchi/src/git/composer-test/"
        }
    ],
    "require": {
        "composer-test/tatsuyano" : "dev-master"
    }
}
</code></pre>

また、BitBucket の非公開リポジトリを設定することもできる。  
`"url": "git@bitbucket.org:tatsuyano/composer-test.git"`

#### test.php(読み込む側のプログラム)

インストールされたパッケージを使うには`include_once "vendor/autoload.php"`で。  
まとめてソースを`include`することができる。

<pre><code class="language-bash">include_once "vendor/autoload.php";

use Tatsuyano\ParentClass;

print ParentClass::name() . "\n";
print ParentClass::nameOfChild() . "\n";

$child = ParentClass::child();
print $child->say() . "\n";
</code></pre>
<br>

### 参考サイト

* [PHPで、spl_autoload_registerを使って、require_once地獄を脱出しよう](http://qiita.com/misogi@github/items/8d02f2eac9a91b4e6215)
* [Composerでローカルのパッケージを取り込む](http://kore1server.com/182/Composer%E3%81%A7%E3%83%AD%E3%83%BC%E3%82%AB%E3%83%AB%E3%81%AE%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E3%82%92%E5%8F%96%E3%82%8A%E8%BE%BC%E3%82%80)
