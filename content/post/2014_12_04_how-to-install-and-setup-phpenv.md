+++
date = "2014-12-04T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["php"]
title = "phpenvを導入する"
slug = "how-to-install-and-setup-phpenv"
+++

phpもanyenvで管理できるようなので入れてみた。

anyenv本体のインストールは[こちら](/2014/08/19/install-anyenv/)を参照してください。

### phpenvのインストール

<pre><code class="language-bash">$ anyenv install phpenv
$ source $HOME/.zshrc
</code></pre>

phpenv には2種類あるらしく、anyenv でインストールされるのは [phpenv/phpenv](https://github.com/phpenv/phpenv)。
この phpenv/phpenv だと、anyenv だとうごかなかったので、[CHH/phpenv](https://github.com/CHH/phpenv) に差し替える必要がある。

### phpenvの差し替え

<pre><code class="language-bash">$ cd ~/.anyenv/envs/phpenv
$ git clone https://github.com/CHH/php-build.git plugins/php-build
$ rm libexec/phpenv-install
</code></pre><br>

### PHPのインストール

結構必要なライブラリが多い。面倒なので、参考サイトに記載のあったライブラリを全部いれた。

<pre><code class="language-bash">$ sudo yum install -y re2c libxml2-devel openssl-devel libcurl-devel libjpeg-turbo-devel libpng-devel libmcrypt-devel readline-devel libtidy-devel libxslt-devel
$ phpenv install 5.5.17
$ phpenv global 5.5.17
$ source $HOME/.zshrc
$ which php
/home/noguchi/.anyenv/envs/phpenv/shims/php
</code></pre>

php.ini も anyenv 以下に作成される
`/home/noguchi/.anyenv/envs/phpenv/versions/5.5.17/etc/php.ini`

### 参考サイト

* [anyenv + phpenv + php-build で複数のバージョンの PHP を切り替える](http://y-uti.hatenablog.jp/entry/2014/10/13/232540)
* [anyenvのphpenvを入れ替える方法](http://www.nishimiyahara.net/2014/06/25/071123)

### 関連する記事

* [anyenvをインストールする](/2014/08/19/install-anyenv/)
