+++
date = "2012-10-01T01:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["running"]
title = "GARMIN FORERUNNER 110"
slug = "garmin-forrunner-110"
+++

<img src='https://dl.dropboxusercontent.com/u/159938/blog_images/garmin001.png' width='40%' />

買ってしまいました「Garmin Fore Runner 110」。  
Amazonでハートレートモニター付属の並行輸入品、18,900円で購入。  
  
いやあ素晴らしいですねｗ  
今までiphoneのGPSアプリを使っていたのですが、iphoneを持って走るのが億劫になり購入してしまいました。  
では簡単にですがレビューします。  

## Garmin Fore Runner 110　でできること

* 1km(指定した距離)ごとにお知らせ音がなること
* 1km(指定した距離)ごとにペースが表示されること
* GPS機能で走った道のりやカロリーが確認できること
* 心拍数が(ハートレートモニターを使って)計測できること

## できないこと

* 信号などで立ち止まった時に、自動で計測が止まらないこと
* 距離などをバイブレーションで知らせる機能がないこと

## すばらしい点

* 操作方法がシンプル。ボタンは４つなのですが、機能毎に集約されていて使いやすい
* 画面が大きい。またバックライトが見やすい
* 見た目のわりに軽い(227g)
* Garmin Connect(データを管理するサイト)がすごく使いやすい

## 不満な点

* 時計とUSBを接続する方法がクリップ式になっていて、時計の画面に毎回クリップで挟んださいのゴムの後が残る

## 注意点

* 時計の画面が「Saving Activity」というメッセージが表示されてフリーズする

 -> ボタン４つを長押しして、初期化(ただし計測した走行データはきえなかった)する。
 
* ChromeでGarmin Connectに接続するとPluginがないと警告が出る

 -> Safariで開く
 
* Garminが認識されない

 ->「Garmin Communicator Plugin」と「Garmin ANT Agent」をインストールし、safariで開く
