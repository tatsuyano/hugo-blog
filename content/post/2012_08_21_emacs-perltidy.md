+++
date = "2012-08-21T03:00:00+09:00"
draft = false
image = "/img/about-bg.jpg"
tags = ["emacs", "perl"]
title = "perltidyをインストールし、emacsでも使えるように設定する"
slug = "emacs-perltidy"
+++

## はじめに

perltidyとはperlのソースを指定したフォーマットに整形してくれるツールです。  

## インストール

私の環境はCentOSなので、<del datetime="2012-09-04T06:43:23+00:00">yumでインストールします。</del>  
macとCentOS両方同じ方法でインストールしたいので、Perlモジュールのperltidyを使用することにします。  

```
cpanm Perl::Tidy
```

## フォーマットを指定する

ホームディレクトリに「.perltidyrc」というファイルを生成し、フォーマットを指定します。  
以下の設定は、書籍「Perl Best Practices」のものらしいです。。  

```
cat > .perltidyrc
-l=78   # Max line width is 78 cols
-i=4    # Indent level is 4 cols
-ci=4   # Continuation indent is 4 cols
-st     # Output to STDOUT
-se     # Errors to STDERR
-vt=2   # Maximal vertical tightness
-cti=0  # No extra indentation for closing brackets
-pt=1   # Medium parenthesis tightness
-bt=1   # Medium brace tightness
-sbt=1  # Medium square brace tightness
-bbt=1  # Medium block brace tightness
-nsfs   # No space before semicolons
-nolq   # Don't outdent long quoted strings
-wbb="% + - * / x != == >= <= =~ !~ < > | & >= < = **= += *= &= <<= &&= -= /= |= >>= ||= .= %= ^= x="
        # Break before all operators
```

## 使用例

コマンドとして単体で使うには以下のように使ってください。  

```
perltidy hoge.pl
```

## .emacsの設定

``` scheme
;; perl tidy
;; sudo yum -y install perltidy

;; リージョン内のperlソースを整形する。
(defun perltidy-region ()
  "Run perltidy on the current region."
      (interactive)
          (save-excursion
                (shell-command-on-region (point) (mark) "perltidy -q" nil t)))
                (global-set-key "\C-ct" 'perltidy-region)

;; ソースすべてを整形する。
(defun perltidy-defun ()
  "Run perltidy on the current defun."
    (interactive)
      (save-excursion (mark-defun) (perltidy-region)))
      (global-set-key "\C-c\C-t" 'perltidy-defun)
```

## 参考サイト

以下のサイトを参考にさせていただきました。ありがとうございます！

* [UbuntuでEmacsからPerlの整形ツールperltidyを使う設定](http://d.hatena.ne.jp/supermassiveblackhole/20100723/1279874404")
